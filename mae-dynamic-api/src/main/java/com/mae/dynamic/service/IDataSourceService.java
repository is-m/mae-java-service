package com.mae.dynamic.service;

import com.mae.core.ReturnMessage;
import com.mae.core.ReturnPagedCollection;
import com.mae.core.ReturnValue;
import com.mae.dynamic.DataSourcePageQuery;
import com.mae.dynamic.datasource.entity.DataSource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/api/data-sources")
public interface IDataSourceService {
    @PostMapping
    ReturnValue<DataSource> createDataSource(@Valid @RequestBody DataSource dataSource);

    @DeleteMapping("/{id}")
    ReturnMessage deleteDataSource(@PathVariable String id);

    @PutMapping
    ReturnValue<DataSource> updateDataSource(DataSource dataSource);

    @GetMapping("/{id}")
    ReturnValue<DataSource> findOne(String id);

    @GetMapping
    ReturnPagedCollection<DataSource> findPageList(DataSourcePageQuery query);

    /**
     * 测试连接
     *
     * @param dataSource dataSourceVO
     * @return ReturnMessage
     */
    @PostMapping("/connect")
    ReturnMessage testConnection(@RequestBody DataSource dataSource);

}
