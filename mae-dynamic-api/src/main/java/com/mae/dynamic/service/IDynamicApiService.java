package com.mae.dynamic.service;

import com.mae.core.ReturnValue;
import com.mae.dynamic.DynamicApiVO;

import java.io.Serializable;
import java.util.Map;

public interface IDynamicApiService {
    /**
     * 创建动态API
     *
     * @param dynamicApiVO 动态API
     * @return dynamic
     */
    ReturnValue<DynamicApiVO> createDynamicApi(DynamicApiVO dynamicApiVO);

    /**
     * 运行动态API
     *
     * @param dynamicApiId 动态 API ID
     * @param input 入参
     * @return 出参
     */
    Serializable run(String dynamicApiId, Map<String, Object> input);
}
