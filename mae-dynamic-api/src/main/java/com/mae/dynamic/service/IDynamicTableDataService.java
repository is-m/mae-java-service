package com.mae.dynamic.service;

import com.mae.core.ReturnPagedCollection;
import com.mae.core.ReturnValue;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/api/dynamic/tables/{table}/data")
public interface IDynamicTableDataService {
    @PostMapping
    int insert(@PathVariable String table, @RequestBody List<Map<String, Object>> objects);

    @PutMapping
    int update(@PathVariable String table, @PathVariable String id, @RequestBody Map<String, Object> properties);

    @DeleteMapping("/{id}")
    int delete(@PathVariable String table, @PathVariable String id);

    @GetMapping("/{id}")
    ReturnValue<Map<String,Object>> findOne(@PathVariable String table, @PathVariable String id);

    /**
     * 分页查询，query>page=1, query>size=20, query>name key,
     * @return
     */
    @GetMapping
    ReturnPagedCollection<Map<String,Object>> findPageList(@PathVariable String table, Map<String, Object> query);
}
