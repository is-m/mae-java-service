package com.mae.dynamic.service;

import com.mae.core.ReturnValue;
import com.mae.dynamic.DynamicSqlVO;

import java.io.Serializable;
import java.util.Map;

public interface IDynamicSqlService {
    /**
     * 创建动态SQL
     *
     * @param dynamicSqlVO
     * @return
     */
    ReturnValue<DynamicSqlVO> createDynamicSql(DynamicSqlVO dynamicSqlVO);

    Serializable run(String dynamicSqlId, Map<String, Object> input);
}
