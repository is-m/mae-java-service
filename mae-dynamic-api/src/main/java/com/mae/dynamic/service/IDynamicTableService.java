package com.mae.dynamic.service;

import com.mae.core.ReturnMessage;
import com.mae.core.ReturnValue;
import com.mae.dynamic.DynamicTableVO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@RequestMapping("/dynamic/tables")
public interface IDynamicTableService {

    @PostMapping
    ReturnValue<DynamicTableVO> createDynamicTable(DynamicTableVO dynamicTableVO);

    /**
     * 即便存在列丢失，不做列的删除
     * @param dynamicTableVO
     * @return
     */
    @PutMapping
    ReturnValue<DynamicTableVO> updateDynamicTable(DynamicTableVO dynamicTableVO);

    ReturnMessage deleteTableColumn();
}
