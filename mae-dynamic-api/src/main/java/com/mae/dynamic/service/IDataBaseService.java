package com.mae.dynamic.service;

import com.mae.core.ReturnCollection;
import com.mae.core.ReturnValue;
import com.mae.dynamic.DataBaseVO;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/databases")
public interface IDataBaseService {

    @GetMapping("/connect-by-source/{dataSourceId}")
    ReturnCollection<DataBaseVO> connectByDataSourceId(@PathVariable String dataSourceId);

    @PostMapping("/execute-sql/{dataSourceId}")
    ReturnValue<String> executeSql(@PathVariable String dataSourceId, @RequestBody String sql);
}
