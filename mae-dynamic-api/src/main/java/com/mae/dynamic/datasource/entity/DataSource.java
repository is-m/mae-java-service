package com.mae.dynamic.datasource.entity;

import com.mae.core.entity.TenantEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Schema(name = "DataSourceVO", description = "数据源")
@Getter
@Setter
public class DataSource extends TenantEntity {
    @NotBlank
    @Size(max=100)
    @Schema(name = "name", description = "数据源名称", example = "default")
    private String name;
    @Size(max=1000)
    @Schema(name = "remark", description = "数据源说明", example = "default")
    private String remark;
    @NotBlank
    @Size(max=10)
    private String databaseType;
    /*@Size(max=100)
    @Schema(name = "jdbcDriver", description = "数据源驱动类", example = "com.mysql.cj.jdbc.Driver")
    private String jdbcDriver;*/
    /*@Size(max=500)
    @Schema(name = "jdbcUrl", description = "jdbc连接串", example = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai")
    private String jdbcUrl;*/
    @NotBlank
    @Size(max=255)
    @Schema(name = "host", description = "服务器", example = "localhost")
    private String host;
    @NotNull
    @Max(65535)
    @Schema(name = "port", description = "端口", example = "3306")
    private Integer port;
    @NotBlank
    @Schema(name = "database", description = "数据库", example = "test")
    private String database;
    @NotBlank
    @Schema(name = "username", description = "用戶名", example = "default")
    private String username;
    @Size(max=128)
    @Schema(name = "password", description = "密码", example = "default")
    private String password;
}
