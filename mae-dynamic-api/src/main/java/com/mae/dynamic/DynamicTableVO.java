package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DynamicTableVO {
    /**
     * 表名字
     */
    private String name;

    /**
     * 表说明
     */
    private String remark;

    /**
     * 列，注意 默认具备id列
     */
    private List<DynamicTableColumnVO> columns;

    /**
     * 支持创建
     */
    private boolean supportCreate;

    /**
     * 支持更新
     */
    private boolean supportUpdate;

    /**
     * 支持删除
     */
    private boolean supportDelete;

    /**
     * 包含审计字段，
     */
    private boolean includeAuditField;
}
