package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TableDataVO {
    private TableDefineVO tableDefineVO;
    private List<Object> data;

    public TableDataVO(){
        tableDefineVO = new TableDefineVO();
        data = new ArrayList<>();
    }
}
