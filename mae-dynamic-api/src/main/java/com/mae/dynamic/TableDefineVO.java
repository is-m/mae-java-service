package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TableDefineVO {
    /**
     * 表名
     */
    private String name;
    /**
     * 编码
     */
    private String encode;

    /**
     * 列
     */
    private List<TableColumnDefineVO> columns;

    /**
     * 索引
     */
    private List<TableIndexDefineVO> indexes;
}
