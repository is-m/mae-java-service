package com.mae.dynamic.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class JdbcConnection {
    private String driver;
    private String jdbcUrl;

    /**
     * 获取格式化后的jdbcUrl
     *
     * @param host     主机
     * @param port     端口
     * @param database 数据库名称
     * @return String
     */
    public String formatJdbcUrl(String host, int port, String database) {
        return String.format(jdbcUrl, host, port, database);
    }
}
