package com.mae.dynamic.utils;

import com.mae.core.utils.Assert;
import com.mae.core.utils.Mae;
import com.mae.dynamic.DataBaseVO;
import com.mae.dynamic.datasource.entity.DataSource;
import com.mae.dynamic.service.IDataSourceService;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.*;

public class DataSourceUtils {
    private final static Map<String, JdbcConnection> connectionMap;

    static {
        connectionMap = new HashMap<>();
        connectionMap.put("mysql", new JdbcConnection("com.mysql.cj.jdbc.Driver", "jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai"));
        connectionMap.put("pgsql", new JdbcConnection("org.postgresql.Driver", "jdbc:postgresql://%s:%s/%s"));
    }

    /**
     * 获取JDBC 连接参数
     *
     * @param databaseType 数据库类型
     * @return JdbcConnection
     */
    public static JdbcConnection getJdbcConnection(String databaseType) {
        Assert.isTrue(connectionMap.containsKey(databaseType), "不支持的数据库类型");
        return connectionMap.get(databaseType);
    }

    /**
     * 获取JDBC连接
     *
     * @param datasourceId
     * @return
     */
    public static Connection openConnection(String datasourceId) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        IDataSourceService dataSourceService = Mae.context().getBean(IDataSourceService.class);
        DataSource dataSource = dataSourceService.findOne(datasourceId).getContent();
        Assert.notNull(dataSource, "数据源不存在");
        JdbcConnection jdbcConnection = DataSourceUtils.getJdbcConnection(dataSource.getDatabaseType());
        String jdbcUrl = jdbcConnection.formatJdbcUrl(dataSource.getHost(), dataSource.getPort(), dataSource.getDatabase());
        List<DataBaseVO> databases = new ArrayList<>();

        // 根据数据源查找所有数据库
        Driver driver = (Driver) Class.forName(jdbcConnection.getDriver()).newInstance();
        Properties properties = new Properties();
        properties.put("user", dataSource.getUsername());
        properties.put("password", dataSource.getPassword());
        return driver.connect(jdbcUrl, properties);
    }
}
