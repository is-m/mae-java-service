package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventDefineVO {
    private String name;
}
