package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DataSourceManagementVO {
    private String id;
    private String host;
    private String user;
    private List<DataBaseVO> databases;

}
