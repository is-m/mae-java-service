package com.mae.dynamic.dao;

import com.mae.core.PagedResult;
import com.mae.dynamic.DataSourcePageQuery;
import com.mae.dynamic.datasource.entity.DataSource;
import org.apache.ibatis.annotations.Param;

public interface DataSourceDao {

    int createDataSource(DataSource dataSource);

    DataSource findOne(String id);

    PagedResult<DataSource> findPageList(DataSourcePageQuery query);

    int countByTenantIdAndNameAndId(@Param("tenantId") String tenantId,@Param("name") String name,@Param("id") String dataSourceId);
}
