package com.mae.dynamic;

import com.mae.core.PageQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataSourcePageQuery extends PageQuery {
    private String keyword;
}
