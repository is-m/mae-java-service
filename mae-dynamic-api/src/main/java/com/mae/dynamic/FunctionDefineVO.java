package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FunctionDefineVO {
    private String name;
}
