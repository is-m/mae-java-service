package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TableColumnDefineVO {
    private String name;
    private String remark;
    private String type;
    private Integer length;
    private String encode;
    private boolean autoIncrement;
    private int incrementValue;
}
