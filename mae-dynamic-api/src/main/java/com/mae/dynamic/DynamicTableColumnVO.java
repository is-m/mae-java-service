package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DynamicTableColumnVO {
    private String name;
    private String remark;
    private String type;
    private Object defaultValue;

    /**
     * 详情返回
     */
    private boolean supportDetailGet;

    /**
     * 列表返回
     */
    private boolean supportListGet;

    /**
     * 分页返回
     */
    private boolean supportPageListGet;

    /**
     * 允许在insert时设置
     */
    private boolean supportCreate;

    /**
     * 允许在update时设置
     */
    private boolean supportUpdate;
}
