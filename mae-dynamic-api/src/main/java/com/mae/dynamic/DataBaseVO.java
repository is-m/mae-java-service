package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DataBaseVO {
    /**
     * 数据库名称
     */
    private String name;
    /**
     * 编码
     */
    private String charset;
    /**
     * 存储引擎(mysql inno,isam)
     */
    private String engine;

    /**
     * 表
     */
    private List<TableDefineVO> tables;
    /**
     * 视图
     */
    private List<ViewDefineVO> views;
    /**
     * 存储过程
     */
    private List<ProcedureDefineVO> procedures;
    /**
     * 函数
     */
    private List<FunctionDefineVO> functions;
    /**
     * 触发器
     */
    private List<Object> triggers;
    /**
     * 事件
     */
    private List<EventDefineVO> events;
}
