package com.mae.dynamic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DynamicSqlVO {
    private String id;
    private String name;
    private String sql;
}
