package com.mae.async.service;

import java.io.Serializable;

public interface IMessageSender {

    void submit(String topic, Serializable body);

}
