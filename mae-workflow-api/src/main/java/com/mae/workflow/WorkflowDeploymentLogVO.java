package com.mae.workflow;

import com.mae.core.entity.TenantEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowDeploymentLogVO extends TenantEntity {
    private String workflowModelId;
    private String keycode;
    private String deploymentId;
    private String definitionId;
    private String model;
    private int version;
}
