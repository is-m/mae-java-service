package com.mae.workflow.service;

import com.mae.core.ReturnPagedCollection;
import com.mae.workflow.dto.todo.MyApplicationDTO;
import com.mae.workflow.dto.todo.MyApplicationPageQueryDTO;
import com.mae.workflow.dto.todo.MyApplyPageQueryDTO;
import com.mae.workflow.dto.todo.MyTodoPageQueryDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Tag(name = "MyApplicationQueryApi", description = "我的任务")
@RequestMapping("/api/my-application")
public interface IMyApplicationQueryService {
    /**
     * 我的申请列表(申请人为我的)
     *
     * @param myApplyPageQueryDTO
     */
    @GetMapping("/apply-list")
    ReturnPagedCollection<MyApplicationDTO> getMyApplyApplications(MyApplyPageQueryDTO myApplyPageQueryDTO);

    /**
     * 我的待办列表(当前处理人为我的流程列表)
     * <p>
     * 注意使用时，这里是待办列表，基于运行时任务来的，这里的任务ID，任务名称，为当前待办task，
     * <p>
     * 界面应该有限显示任务相关内容
     *
     * @param myTodoPageQueryDTO
     */
    @GetMapping("/todo-list")
    ReturnPagedCollection<MyApplicationDTO> getMyTodoApplications(MyTodoPageQueryDTO myTodoPageQueryDTO);

    /**
     * 我参与的流程列表(我处理过的流程列表)
     *
     * @param myApplicationPageQueryDTO
     */
    @GetMapping("/list")
    ReturnPagedCollection<MyApplicationDTO> getMyApplications(MyApplicationPageQueryDTO myApplicationPageQueryDTO);
}
