package com.mae.workflow.service.internal;

import com.mae.workflow.dto.model.ProcessDefinitionDTO;

public interface IProcessDefinitionQueryService {
    ProcessDefinitionDTO findById(String processDefinitionId);
}
