package com.mae.workflow.service;

import com.mae.core.*;
import com.mae.workflow.ProcessStartDTO;
import com.mae.workflow.dto.process.ProcessCommentDTO;
import com.mae.workflow.dto.process.ProcessCommentDifferenceDTO;
import com.mae.workflow.dto.process.ProcessInstanceDTO;
import com.mae.workflow.dto.process.SimpleProcessInstanceDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

/**
 * 流程实例（跟当前登录用户有权限检查）
 */
@Tag(name = "ProcessInstanceApi", description = "流程实例服务")
@RequestMapping("/api/workflows")
public interface IProcessInstanceService {
    /**
     * 根据部署ID启动（依赖流程版本信息，需要先根据key调用部署接口查询最新的部署ID，适合界面调用）
     *
     * @param deploymentId
     * @param startDTO
     */
    @Operation(summary = "启动流程（根据部署ID）", description = "根据部署ID启动流程，注意：部署ID必须是当前正在使用的部署ID才能启动")
    @PostMapping("/start/{deploymentId}/dep")
    ReturnValue<ProcessInstanceDTO> startByDeployment(
            @PathVariable String deploymentId,
            @RequestBody ProcessStartDTO startDTO);

    /**
     * 根据流程key启动流程（无视流程版本, 每次自动获取最新的部署ID启动，适合接口固定调用）
     *
     * @param keycode
     * @param startDTO
     */
    @Operation(summary = "启动流程（根据流程标识符）", description = "根据流程流程标识符启动流程，按当前流程标识符对应最新的部署版本启动")
    @PostMapping("/start/{keycode}/key")
    ReturnValue<ProcessInstanceDTO> startByProcessKey(
            @PathVariable String keycode,
            @RequestBody ProcessStartDTO startDTO);

    /**
     * 暂停
     *
     * @param procInstId
     */
    @Operation(summary = "暂停流程", description = "活动中的流程可以被暂停，只能暂停申请人为自己的")
    @PostMapping("/{procInstId}/suspend")
    ReturnMessage suspend(@PathVariable String procInstId);

    /**
     * 激活
     *
     * @param procInstId
     */
    @Operation(summary = "激活流程", description = "暂停状态的流程才能被激活，只能激活申请人为自己的")
    @PostMapping("/{procInstId}/resume")
    ReturnMessage resume(@PathVariable String procInstId);

    /**
     * 终止（只能终止流程配置了可以终止的流程，已经完成的不能终止）
     *
     * @param procInstId
     */
    @Operation(summary = "终止流程", description = "流程模型配置了允许终止才能进行终止，只能终止申请人为自己的")
    @PostMapping("/{procInstId}/stop")
    ReturnMessage stop(@PathVariable String procInstId);

    @Operation(summary = "获取流程审批日志", description = "建议使用500条pageSize进行分页显示")
    @GetMapping("/{procInstId}/comments")
    ReturnPagedCollection<ProcessCommentDTO> getPagedComments(
            @PathVariable String procInstId,
            PageQuery pageQuery);

    @Operation(summary = "获取流程审批日志中表单差异", description = "建议使用500条pageSize进行分页显示")
    @GetMapping("/{procInstId}/comments/{commentId}/differences")
    ReturnPagedCollection<ProcessCommentDifferenceDTO> getPagedCommentDifferences(
            @PathVariable String commentId,
            PageQuery pageQuery);
}
