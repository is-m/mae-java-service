package com.mae.workflow.service.internal;

/**
 * ID生成服务
 */
public interface IdGeneratorService {
    /**
     * 获取下一个ID
     *
     * @param procDefinitionId 流程定义ID
     * @return 下一个ID
     */
    String next(String procDefinitionId);
}
