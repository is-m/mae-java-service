package com.mae.workflow.service;

public interface IWorkflowDefinitionService {
    /**
     * 获取部署信息
     *
     * @param keyCode
     */
    void getDeployment(String keyCode);

    /**
     * 获取起始表单（主表单）
     *
     * @param deploymentId
     */
    void getStartForm(String deploymentId);
}
