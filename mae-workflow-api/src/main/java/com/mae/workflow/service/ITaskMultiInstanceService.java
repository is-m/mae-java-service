package com.mae.workflow.service;

/**
 * TaskMultiInstance
 */
public interface ITaskMultiInstanceService {
    /**
     * 加签
     *
     * @param procInstId 流程实例ID
     * @param taskDefKey 任务定义KEY(当前活动中的节点必须包含这个key，否则加签失败)
     */
    void claim(String procInstId, String taskDefKey);

    /**
     * 减签
     *
     * @param taskId 任务ID
     */
    void unClaim(String taskId);
}
