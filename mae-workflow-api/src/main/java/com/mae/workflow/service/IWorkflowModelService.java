package com.mae.workflow.service;

import com.mae.core.ReturnMessage;
import com.mae.workflow.dto.model.WorkflowModelXmlCreateDTO;
import com.mae.workflow.dto.model.WorkflowModelXmlUpdateDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;


@Valid
@Tag(name = "WorkflowModelApi", description = "工作流模型服务")
@RequestMapping("/api/workflow-models")
public interface IWorkflowModelService {
    /**
     * 创建XML模型
     *
     * @param keycode
     * @param xmlCreateDTO
     * @return
     */
    @PostMapping("/{keycode}/xml-create")
    ReturnMessage createModelForXml(
            @Schema(title = "模型标识符", example = "demo") @PathVariable String keycode,
            @Valid @RequestBody WorkflowModelXmlCreateDTO xmlCreateDTO);

    /**
     * 修改Xml模型
     *
     * @param modelVO vo
     */
    @PutMapping("/{keycode}/xml-update")
    ReturnMessage updateModelForXml(
            @Schema(title = "模型标识符", example = "demo") @PathVariable String keycode,
            @Valid @RequestBody WorkflowModelXmlUpdateDTO modelVO);

    /**
     * 模型启用
     *
     * @param keycode
     * @return
     */
    @PostMapping("/{keycode}/install")
    ReturnMessage installModel(
            @Schema(title = "模型标识符", example = "demo") @PathVariable String keycode);

    /**
     * 停用
     *
     * @param keycode
     * @return
     */
    @PostMapping("/{keycode}/uninstall")
    ReturnMessage uninstallModel(
            @Schema(title = "模型标识符", example = "demo") @PathVariable String keycode);

    /**
     * 刪除
     *
     * @param keycode
     * @return
     */
    @DeleteMapping("/{keycode}")
    ReturnMessage deleteModel(
            @Schema(title = "模型标识符", example = "demo") @PathVariable String keycode);

    /**
     * 上传/导入模型
     *
     * @return
     */
    ReturnMessage uploadBpmn20Model();

    /**
     * 下载/导出模型
     *
     * @return
     */
    ReturnMessage downloadBpmn20Model();
}
