package com.mae.workflow.service;

import com.mae.core.ReturnMessage;
import com.mae.core.ReturnPagedCollection;
import com.mae.workflow.dto.process.SimpleProcessInstanceDTO;
import com.mae.workflow.dto.task.TaskCompleteDTO;
import com.mae.workflow.dto.task.TaskTransferDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

/**
 * 流程实例管理服务 (无视当前登录用户)
 */
@Tag(name = "ProcessInstanceManagementApi", description = "流程实例管理服务(管理)")
@RequestMapping("/api/workflows")
public interface IProcessInstanceManagementService {
    /**
     * 查询所有工作流
     *
     * @return
     */
    @GetMapping
    ReturnPagedCollection<SimpleProcessInstanceDTO> findProcessInstancePageList();

    /**
     * 暂停
     *
     * @param procInstId
     */
    @Operation(summary = "暂停流程", description = "活动中的流程可以被暂停")
    @PostMapping("/{procInstId}/mgt-suspend")
    ReturnMessage suspend(@PathVariable String procInstId);

    /**
     * 激活
     *
     * @param procInstId
     */
    @Operation(summary = "激活流程", description = "暂停状态的流程才能被激活")
    @PostMapping("/{procInstId}/mgt-resume")
    ReturnMessage resume(@PathVariable String procInstId);

    /**
     * 终止（只能终止流程配置了可以终止的流程，已经完成的不能终止）
     *
     * @param procInstId
     */
    @Operation(summary = "终止流程", description = "流程模型配置了允许终止才能进行终止")
    @PostMapping("/{procInstId}/mgt-stop")
    ReturnMessage stop(@PathVariable String procInstId);

    @Operation(summary = "审批", description = "审批")
    @PostMapping("/{procInstId}/tasks/{taskId}/mgt-complete")
    ReturnMessage complete(
            @PathVariable String procInstId,
            @PathVariable String taskId,
            @RequestBody TaskCompleteDTO taskCompleteDTO);

    @Operation(summary = "转派", description = "转派")
    @PostMapping("/{procInstId}/tasks/{taskId}/mgt-transfer")
    ReturnMessage transfer(
            @PathVariable String procInstId,
            @PathVariable String taskId,
            @RequestBody TaskTransferDTO taskTransferDTO);

}
