package com.mae.workflow.service.internal;

import com.mae.core.ReturnMessage;
import com.mae.workflow.dto.task.TaskCompleteDTO;
import com.mae.workflow.dto.task.TaskSaveDTO;
import com.mae.workflow.dto.task.TaskTransferDTO;

public interface IProcessInstanceInternalService {

    ReturnMessage suspend(String procInstId, boolean checkPermission);

    ReturnMessage resume(String procInstId, boolean checkPermission);

    ReturnMessage stop(String procInstId, boolean checkPermission);

    ReturnMessage save(String procInstId, String taskId, TaskSaveDTO taskSaveDTO, boolean checkPermission);

    ReturnMessage complete(String procInstId, String taskId, TaskCompleteDTO taskCompleteDTO, boolean checkPermission);

    ReturnMessage transfer(String procInstId, String taskId, TaskTransferDTO taskTransferDTO, boolean checkPermission);
}
