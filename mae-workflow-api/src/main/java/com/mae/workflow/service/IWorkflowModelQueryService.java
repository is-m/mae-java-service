package com.mae.workflow.service;

import com.mae.core.ReturnPagedCollection;
import com.mae.core.ReturnValue;
import com.mae.workflow.WorkflowModelPageQuery;
import com.mae.workflow.WorkflowModelVO;
import com.mae.workflow.dto.model.WorkflowModelXmlDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Tag(name = "WorkflowModelApi", description = "工作流模型服务")
@RequestMapping("/api/workflow-models")
public interface IWorkflowModelQueryService {

    /**
     * 获取xml模型详情
     *
     * @param keycode
     * @return
     */
    @GetMapping("/{keycode}/xml-get")
    ReturnValue<WorkflowModelXmlDTO> findXmlDetail(
            @Schema(title = "模型标识符", example = "demo") @PathVariable String keycode);

    /**
     * 分页简单查询，不返回model属性
     *
     * @return
     */
    @GetMapping
    ReturnPagedCollection<WorkflowModelVO> findSimplePagedList(WorkflowModelPageQuery query);
}
