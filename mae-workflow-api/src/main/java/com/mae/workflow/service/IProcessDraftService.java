package com.mae.workflow.service;

import com.mae.core.ReturnMessage;
import com.mae.workflow.dto.draft.DraftPageQueryDTO;
import com.mae.workflow.dto.draft.ProcessDraftCreateDTO;
import com.mae.workflow.dto.draft.ProcessDraftUpdateDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 流程草稿服务
 */
@Tag(name = "ProcessInstanceApi", description = "流程实例服务")
@RequestMapping("/api/workflows-drafts")
public interface IProcessDraftService {
    /**
     * 保存草稿
     *
     * @param processDraftCreateDTO 创建草稿
     */
    @PostMapping
    ReturnMessage createDraft(
            @PathVariable String procInstId,
            @RequestBody ProcessDraftCreateDTO processDraftCreateDTO);

    /**
     * 更新草稿
     *
     * @param processDraftUpdateDTO 更新草稿
     */
    @PutMapping
    void updateDraft(
            @PathVariable String procInstId,
            @RequestBody ProcessDraftUpdateDTO processDraftUpdateDTO);

    /**
     * 删除草稿
     *
     * @param draftId 删除草稿
     */
    @DeleteMapping
    void deleteDraft(@PathVariable String draftId);

    /**
     * 分页查询草稿（不包含表单信息）
     *
     * @param draftPageQueryDTO
     */
    void findDraftPageList(DraftPageQueryDTO draftPageQueryDTO);

    /**
     * 获取草稿详情（包含表单信息）
     *
     * @param id
     */
    @GetMapping("/{id}")
    void findDraft(@PathVariable String id);
}
