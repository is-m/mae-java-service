package com.mae.workflow.service.internal;

import com.mae.workflow.persistence.po.WorkflowDeploymentLogPO;
import com.mae.workflow.persistence.po.WorkflowModelPO;

public interface IWorkflowModelDeployInternalService {

    WorkflowDeploymentLogPO deploy(WorkflowModelPO modelPO);

    void uninstall(String deploymentId);
}
