package com.mae.workflow.service;

import com.mae.core.ReturnMessage;
import com.mae.workflow.dto.task.TaskCompleteDTO;
import com.mae.workflow.dto.task.TaskSaveDTO;
import com.mae.workflow.dto.task.TaskTransferDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Tag(name = "ProcessInstanceApi", description = "流程实例服务")
@RequestMapping("/api/workflows")
public interface IProcessInstanceTaskService {
    @Operation(summary = "保存任务表单", description = "可修改的主表单")
    @PostMapping("/{procInstId}/tasks/{taskId}/save")
    ReturnMessage save(
            @PathVariable String procInstId,
            @PathVariable String taskId,
            @RequestBody TaskSaveDTO taskSaveDTO);

    @Operation(summary = "审批", description = "可修改的主表单")
    @PostMapping("/{procInstId}/tasks/{taskId}/complete")
    ReturnMessage complete(
            @PathVariable String procInstId,
            @PathVariable String taskId,
            @RequestBody TaskCompleteDTO completeDTO);

    @Operation(summary = "转给他人处理", description = "转给他人处理")
    @PostMapping("/{procInstId}/tasks/{taskId}/transfer")
    ReturnMessage transfer(
            @PathVariable String procInstId,
            @PathVariable String taskId,
            @RequestBody TaskTransferDTO transferDTO);
}
