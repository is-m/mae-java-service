package com.mae.workflow;

import com.mae.core.entity.TenantEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class WorkflowModelVO extends TenantEntity {
    private String name;
    private String nameEn;
    private String category;
    private String keycode;

    /**
     * 工作流模型
     * <p>
     * 新增修改时支持xml，或者json
     * 返回时返回json
     */
    @Schema(example = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:zeebe=\"http://camunda.org/schema/zeebe/1.0\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:modeler=\"http://camunda.org/schema/modeler/1.0\" id=\"Definitions_1mynmx0\" targetNamespace=\"http://bpmn.io/schema/bpmn\" exporter=\"Camunda Modeler\" exporterVersion=\"5.2.0\" modeler:executionPlatform=\"Camunda Cloud\" modeler:executionPlatformVersion=\"8.0.0\">\n" +
            "  <bpmn:process id=\"Process_1ap75ye\" isExecutable=\"true\">\n" +
            "    <bpmn:extensionElements>\n" +
            "\t\t<configData>\n" +
            "\t\t\t<![CDATA[ \n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"allowStop\":true,\n" +
            "\t\t\t\t\t\"formDefinition\":[]\n" +
            "\t\t\t\t}\n" +
            "\t\t\t]]>\n" +
            "\t\t</configData>\n" +
            "\t</bpmn:extensionElements>\n" +
            "    <bpmn:startEvent id=\"StartEvent_1\" name=\"开始\">\n" +
            "      <bpmn:outgoing>Flow_1hqviqs</bpmn:outgoing>\n" +
            "    </bpmn:startEvent>\n" +
            "    <bpmn:endEvent id=\"Event_0ilqlvs\" name=\"结束\">\n" +
            "      <bpmn:incoming>Flow_1c6j3mu</bpmn:incoming>\n" +
            "    </bpmn:endEvent>\n" +
            "    <bpmn:sequenceFlow id=\"Flow_1c6j3mu\" name=\"序列流2\" sourceRef=\"Activity_15ejinp\" targetRef=\"Event_0ilqlvs\" />\n" +
            "    <bpmn:sequenceFlow id=\"Flow_1hqviqs\" name=\"序列流1\" sourceRef=\"StartEvent_1\" targetRef=\"Activity_15ejinp\" />\n" +
            "    <bpmn:userTask id=\"Activity_15ejinp\" name=\"用户节点\">\n" +
            "      <bpmn:extensionElements>\n" +
            "\t\t<configData>\n" +
            "\t\t\t<![CDATA[ \n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"assignee\":\"administrator\",\n" +
            "\t\t\t\t\t\"formDefinition\":[{}]\n" +
            "\t\t\t\t}\n" +
            "\t\t\t]]>\n" +
            "\t\t</configData> \n" +
            "\t\t<formDefinition></formDefinition>\n" +
            "        <zeebe:assignmentDefinition assignee=\"liaoxianmu\" />\n" +
            "        <zeebe:ioMapping>\n" +
            "          <zeebe:input source=\"=source\" target=\"InputVariable_3on9356\" />\n" +
            "        </zeebe:ioMapping>\n" +
            "      </bpmn:extensionElements>\n" +
            "      <bpmn:incoming>Flow_1hqviqs</bpmn:incoming>\n" +
            "      <bpmn:outgoing>Flow_1c6j3mu</bpmn:outgoing>\n" +
            "    </bpmn:userTask>\n" +
            "  </bpmn:process>\n" +
            "  <bpmndi:BPMNDiagram id=\"BPMNDiagram_1\">\n" +
            "    <bpmndi:BPMNPlane id=\"BPMNPlane_1\" bpmnElement=\"Process_1ap75ye\">\n" +
            "      <bpmndi:BPMNEdge id=\"Flow_1c6j3mu_di\" bpmnElement=\"Flow_1c6j3mu\">\n" +
            "        <di:waypoint x=\"380\" y=\"120\" />\n" +
            "        <di:waypoint x=\"472\" y=\"120\" />\n" +
            "        <bpmndi:BPMNLabel>\n" +
            "          <dc:Bounds x=\"407\" y=\"102\" width=\"39\" height=\"14\" />\n" +
            "        </bpmndi:BPMNLabel>\n" +
            "      </bpmndi:BPMNEdge>\n" +
            "      <bpmndi:BPMNEdge id=\"Flow_1hqviqs_di\" bpmnElement=\"Flow_1hqviqs\">\n" +
            "        <di:waypoint x=\"188\" y=\"120\" />\n" +
            "        <di:waypoint x=\"280\" y=\"120\" />\n" +
            "        <bpmndi:BPMNLabel>\n" +
            "          <dc:Bounds x=\"215\" y=\"102\" width=\"39\" height=\"14\" />\n" +
            "        </bpmndi:BPMNLabel>\n" +
            "      </bpmndi:BPMNEdge>\n" +
            "      <bpmndi:BPMNShape id=\"_BPMNShape_StartEvent_2\" bpmnElement=\"StartEvent_1\">\n" +
            "        <dc:Bounds x=\"152\" y=\"102\" width=\"36\" height=\"36\" />\n" +
            "        <bpmndi:BPMNLabel>\n" +
            "          <dc:Bounds x=\"159\" y=\"145\" width=\"22\" height=\"14\" />\n" +
            "        </bpmndi:BPMNLabel>\n" +
            "      </bpmndi:BPMNShape>\n" +
            "      <bpmndi:BPMNShape id=\"Event_0ilqlvs_di\" bpmnElement=\"Event_0ilqlvs\">\n" +
            "        <dc:Bounds x=\"472\" y=\"102\" width=\"36\" height=\"36\" />\n" +
            "        <bpmndi:BPMNLabel>\n" +
            "          <dc:Bounds x=\"479\" y=\"145\" width=\"22\" height=\"14\" />\n" +
            "        </bpmndi:BPMNLabel>\n" +
            "      </bpmndi:BPMNShape>\n" +
            "      <bpmndi:BPMNShape id=\"Activity_0359xje_di\" bpmnElement=\"Activity_15ejinp\">\n" +
            "        <dc:Bounds x=\"280\" y=\"80\" width=\"100\" height=\"80\" />\n" +
            "        <bpmndi:BPMNLabel />\n" +
            "      </bpmndi:BPMNShape>\n" +
            "    </bpmndi:BPMNPlane>\n" +
            "  </bpmndi:BPMNDiagram>\n" +
            "</bpmn:definitions>\n")
    private Object model;

    private String status;
    private String lastDeployId;
    private String lastDefinitionId;
    private Date lastDeployTime;
    private Integer version;
}
