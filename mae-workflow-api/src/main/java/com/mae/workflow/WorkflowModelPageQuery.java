package com.mae.workflow;

import com.mae.core.PageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowModelPageQuery extends PageQuery {
    @Schema(title = "关键字查询(流程标识符,中英文名称)", example = "d")
    private String keyword;
}
