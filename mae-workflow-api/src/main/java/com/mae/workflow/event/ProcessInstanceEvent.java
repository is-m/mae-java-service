package com.mae.workflow.event;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

/**
 * 流程实例事件
 */
@Getter
@Setter
public class ProcessInstanceEvent {
    /**
     * 事件类型，CREATE 创建, UPDATE 更新
     */
    private String eventType;

    /**
     * 流程实例ID
     */
    private String id;

    /**
     * 流程标识符
     */
    private String keycode;

    /**
     * 集成业务关联key
     */
    private String businessKey;

    /**
     * 流程定义版本
     */
    private String processDefinitionId;

    /**
     * 流程名称
     */
    private String name;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 申请人
     */
    private String startUserId;

    /**
     * 启动时间
     */
    private Date startTime;

    /**
     * 结束时间（完成状态和终止的）
     */
    private Date endTime;

    /**
     * 流程状态,0 待认领 1 审批中，2 挂起，3 终止，4 完成
     */
    private int status;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务处理人
     */
    private String taskAssignee;

    /**
     * 主表单内容
     */
    private Map<String, Object> variables;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 事件发生时间
     */
    private Date timestamp;
}
