package com.mae.workflow;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowDeploymentVO {
    private String deploymentId;
    private String definitionId;
    private int version;
}
