package com.mae.workflow;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class ProcessStartDTO {
    /**
     * 草稿ID
     */
    private String draftId;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 对接系统业务主键
     */
    private String businessKey;

    /**
     * 主表单变量
     */
    @Schema(title="主表单变量", example = "{}")
    private Map<String, Object> mainform;
}
