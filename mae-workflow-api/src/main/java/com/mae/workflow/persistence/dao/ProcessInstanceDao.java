package com.mae.workflow.persistence.dao;

import org.apache.ibatis.annotations.Param;

/**
 * 流程实例Dao
 */
public interface ProcessInstanceDao {
    /**
     * 指定流程状态
     *
     * @param status 流程状态 0 待分派，1 待处理，2 挂起，3 终止，4 完成
     * @return
     */
    int updateProcessInstanceStatus(
            @Param("id") String id,
            @Param("status") int status);

    void updateRuTaskInfo(
            @Param("id") String id,
            @Param("taskId") String taskId,
            @Param("taskName") String taskName,
            @Param("taskAssignee") String taskAssignee,
            @Param("status") int status);

    /**
     * 根据状态（根据期望状态）
     *
     * @param procInstId   流程实例ID
     * @param toStatus     更新成这个状态
     * @param exceptStatus 期望从这个状态更新
     */
    int updateProcessInstanceStatusForExpect(
            @Param("id") String procInstId,
            @Param("toStatus") int toStatus,
            @Param("exceptStatus") int exceptStatus);
}
