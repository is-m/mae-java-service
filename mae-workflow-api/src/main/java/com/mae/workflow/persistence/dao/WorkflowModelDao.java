package com.mae.workflow.persistence.dao;

import com.mae.core.PagedResult;
import com.mae.workflow.WorkflowModelPageQuery;
import com.mae.workflow.WorkflowModelVO;
import com.mae.workflow.persistence.po.WorkflowModelPO;
import org.apache.ibatis.annotations.Param;

/**
 * @author Liao
 */
public interface WorkflowModelDao {

    /**
     * 創建
     *
     * @param workflowModelPO
     * @return
     */
    int createWorkflowModel(WorkflowModelPO workflowModelPO);

    /**
     * 更新模型基本信息
     *
     * @param workflowModelVO
     * @return
     */
    int updateWorkflowModel(WorkflowModelPO workflowModelPO);

    /**
     * 更新部署信息
     *
     * @param workflowModelVO workflowModelVO
     * @return int
     */
    int updateWorkflowModelDeploymentInfo(WorkflowModelPO workflowModelVO);

    /**
     * 查询模型详情
     *
     * @param id
     * @return
     */
    WorkflowModelPO findOne(String id);

    /**
     * 根据流程标识符查询简易模型信息（注意：包含model属性）
     *
     * @param tenantId
     * @param keycode
     * @return
     */
    WorkflowModelPO findOneByTenantIdAndKeycode(
            @Param("tenantId") String tenantId,
            @Param("keycode") String keycode);

    /**
     * 根据流程标识符查询简易模型信息（注意：不包含model属性）
     *
     * @param tenantId 租户ID
     * @param keycode  流程标识符
     * @return
     */
    WorkflowModelPO findSimpleOneByTenantIdAndKeycode(
            @Param("tenantId") String tenantId,
            @Param("keycode") String keycode);

    int deleteWorkflowModel(String id);

    /**
     * 分页查找
     *
     * @param query query
     * @return pageResult
     */
    PagedResult<WorkflowModelVO> findPagedList(WorkflowModelPageQuery query);

    /**
     * 根据租户和流程标识符统计个数
     *
     * @param tenantId
     * @param keycode
     * @return
     */
    int countByTenantIdAndKeycode(@Param("tenantId") String tenantId, @Param("keycode") String keycode);

    /**
     * 根据租户，部署ID获取
     *
     * @param tenantId
     * @param deploymentId
     * @return
     */
    WorkflowModelPO findSimpleOneByTenantIdAndDeploymentId(
            @Param("tenantId") String tenantId,
            @Param("deploymentId") String deploymentId);
}
