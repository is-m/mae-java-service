package com.mae.workflow.persistence.dao;

import com.mae.core.PagedResult;
import com.mae.workflow.dto.todo.MyApplicationDTO;
import com.mae.workflow.dto.todo.MyApplicationPageQueryDTO;
import com.mae.workflow.dto.todo.MyApplyPageQueryDTO;
import com.mae.workflow.dto.todo.MyTodoPageQueryDTO;

public interface MyApplicationDao {
    /**
     * 我的申请
     *
     * @param myApplyPageQueryDTO
     * @return
     */
    PagedResult<MyApplicationDTO> selectMyApplyPageList(MyApplyPageQueryDTO myApplyPageQueryDTO);

    /**
     * 我的待办
     *
     * @param myTodoPageQueryDTO
     * @return
     */
    PagedResult<MyApplicationDTO> selectMyTodoPageList(MyTodoPageQueryDTO myTodoPageQueryDTO);

    /**
     * 我参与的
     *
     * @param myApplicationPageQueryDTO
     * @return
     */
    PagedResult<MyApplicationDTO> selectMyApplicationPageList(MyApplicationPageQueryDTO myApplicationPageQueryDTO);
}
