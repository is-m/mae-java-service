package com.mae.workflow.persistence.dao;

import com.mae.workflow.persistence.po.WorkflowDeploymentLogPO;

public interface WorkflowDeploymentLogDao {

    int insertDeploymentLog(WorkflowDeploymentLogPO deploymentLogVO);

    WorkflowDeploymentLogPO findOneByProcessDefinitionId(String processDefinitionId);
}
