package com.mae.workflow.persistence.po;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ProcessCommentPO {
    /**
     * 日志ID
     */
    private String id;
    /**
     * 日志类型
     */
    private String type;
    /**
     * 操作备注
     */
    private String msg;
    /**
     * 操作人
     */
    private String userId;

    /**
     * 日志产生时间
     */
    private Date time;

    /**
     * 日志产生的taskId
     */
    private String taskId;

    /**
     * 是否存在差异日志，存在 true， 不存在false
     */
    private boolean existsDifference;
}
