package com.mae.workflow.persistence.dao;

import com.mae.workflow.persistence.po.ProcessDraftPO;

public interface ProcessDraftDao {

    void insertProcessDraft(ProcessDraftPO processDraftPO);

    void deleteById(String processDraftId);
}
