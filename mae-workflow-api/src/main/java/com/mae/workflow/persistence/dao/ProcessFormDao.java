package com.mae.workflow.persistence.dao;

import com.mae.workflow.persistence.po.ProcessFormPO;

public interface ProcessFormDao {
    void insertProcessForm(ProcessFormPO processFormPO);
}
