package com.mae.workflow.persistence.po;

import com.mae.core.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentDifferenceLogPO extends BaseEntity {
    private String action;
    private String difference;
    private String actCommentId;
}
