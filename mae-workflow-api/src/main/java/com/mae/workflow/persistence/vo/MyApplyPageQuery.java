package com.mae.workflow.persistence.vo;

public class MyApplyPageQuery {
    /**
     * 当前用户ID
     */
    private String currentUserId;

    /**
     * 关键字查询，（流程名称，流程实例ID）
     */
    private String keyword;

    /**
     * 流程开始时间（开始）
     */
    private String startTimeBegin;

    /**
     * 流程开始时间（结束）
     */
    private String startTimeEnd;
}
