package com.mae.workflow.persistence.po;

import com.mae.core.entity.TenantEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessDraftPO extends TenantEntity {
    private String keycode;
    private String mainform;
}
