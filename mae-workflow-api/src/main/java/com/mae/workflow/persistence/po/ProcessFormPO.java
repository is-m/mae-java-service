package com.mae.workflow.persistence.po;

import com.mae.core.entity.TenantEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessFormPO extends TenantEntity {
    /**
     * 流程实例ID
     */
    private String procInstId;
    /**
     * 流程实例状态
     */
    private int procInstStatus;
    /**
     * 主流程表单
     */
    private String mainform;
    /**
     * 全任务表单
     */
    private String taskform;
}
