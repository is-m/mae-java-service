package com.mae.workflow.persistence.vo;

import com.mae.core.PageQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentPageQuery extends PageQuery {
    private String procInstId;
}
