package com.mae.workflow.persistence.dao;

import com.mae.workflow.persistence.po.CommentDifferenceLogPO;

public interface CommentDifferenceLogDao {
    /**
     * 插入日志
     *
     * @param commentDifferenceLogPO commentDifferenceLogPO
     */
    void insertCommentDifferenceLog(CommentDifferenceLogPO commentDifferenceLogPO);
}
