package com.mae.workflow.persistence.dao;

public interface ProcessVariableDao {
    void deleteByProcessInstanceId(String procInstId);
}
