package com.mae.workflow.persistence.po;

import com.mae.core.entity.TenantEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class WorkflowModelPO extends TenantEntity {
    private String name;
    private String nameEn;
    private String category;
    private String keycode;
    /**
     * 模型（json）
     */
    private String model;
    private String status;
    private String lastDeployId;
    private String lastDefinitionId;
    private Date lastDeployTime;
    private Integer version;
}
