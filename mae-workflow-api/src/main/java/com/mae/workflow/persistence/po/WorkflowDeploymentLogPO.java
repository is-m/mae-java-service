package com.mae.workflow.persistence.po;

import com.mae.core.entity.TenantEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowDeploymentLogPO extends TenantEntity {
    private String workflowModelId;
    private String keycode;
    private String deploymentId;
    private String definitionId;
    private String model;
    private int version;
}
