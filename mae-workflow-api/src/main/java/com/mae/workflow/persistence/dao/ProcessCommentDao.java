package com.mae.workflow.persistence.dao;

import com.mae.core.PageQuery;
import com.mae.core.PagedResult;
import com.mae.workflow.persistence.po.ProcessCommentPO;
import org.apache.ibatis.annotations.Param;

/**
 * 流程日志
 */
public interface ProcessCommentDao {
    /**
     * 分页查询
     * @param procInstId
     * @param pageQuery
     * @return
     */
    PagedResult<ProcessCommentPO> findPageListByProcessInstanceId(@Param("procInstId") String procInstId, PageQuery pageQuery);

    /**
     * 更新任务节点名称
     *
     * @param id 日志ID
     * @param taskName 任务名称
     * @return
     */
    int updateTaskNameById(@Param("id") String id, @Param("taskName") String taskName);
}
