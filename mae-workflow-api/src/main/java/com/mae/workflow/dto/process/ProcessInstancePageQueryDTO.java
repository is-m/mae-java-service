package com.mae.workflow.dto.process;

import com.mae.core.PageQuery;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ProcessInstancePageQueryDTO extends PageQuery {
    /**
     * 关键字查询，（流程名称，流程实例ID）
     */
    private String keyword;

    /**
     * 业务类型
     */
    private String groupName;

    /**
     * 申请人ID
     */
    private String startUserId;

    /**
     * 流程开始时间（开始）
     */
    private Date startTimeBegin;

    /**
     * 流程开始时间（结束）
     */
    private Date startTimeEnd;
}
