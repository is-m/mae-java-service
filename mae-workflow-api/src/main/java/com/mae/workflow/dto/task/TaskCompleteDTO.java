package com.mae.workflow.dto.task;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class TaskCompleteDTO {
    @Schema(title = "任务表单", example = "{}")
    private Map<String, Object> taskform;
    @Schema(title = "主表单", example = "{}")
    private Map<String, Object> mainform;
}
