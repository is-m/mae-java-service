package com.mae.workflow.dto.model;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.JSONPath;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class ProcessDefinitionDTO {
    private String id;
    private String tenantId;
    private String name;
    private String key;
    private JSONObject model;

    /**
     * 获取主表单属性
     *
     * @return
     */
    public JSONObject getProperties() {
        return model.getJSONObject("properties");
    }

    /**
     * 获取任务属性
     *
     * @param taskDefinitionKey
     * @return
     */
    public JSONObject getTaskProperties(String taskDefinitionKey) {
        // (JSONObject) JSONPath.eval(model, "$.childShapes[?(@.resourceId="+taskDefinitionKey+")]");
        JSONArray childShapes = model.getJSONArray("childShapes");
        for (int i = 0; i < childShapes.size(); i++) {
            JSONObject childShape = childShapes.getJSONObject(i);
            String resourceId = childShape.getString("resourceId");
            if (taskDefinitionKey.equals(resourceId)) {
                return childShape.getJSONObject("properties");
            }
        }
        return new JSONObject();
    }

    public JSONObject getExtensionObject(String activityId) {
        JSONObject taskProperties = getTaskProperties(activityId);
        return taskProperties.getJSONObject("extensions");
    }
}
