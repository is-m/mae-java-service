package com.mae.workflow.dto.draft;

public class SimpleProcessDraftDTO {
    /**
     * 流程 KEY
     */
    private String processKey;

    /**
     * 草稿标题
     */
    private String title;
}
