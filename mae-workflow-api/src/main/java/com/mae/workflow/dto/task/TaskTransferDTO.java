package com.mae.workflow.dto.task;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class TaskTransferDTO {
    @Schema(title = "待转派人员，工作流管理界面使用", example = " ")
    private String from;

    @NotEmpty
    @Schema(title = "转给用户", example = "anonymous")
    private String to;

    @Schema(title = "转派备注")
    private String comment;
}
