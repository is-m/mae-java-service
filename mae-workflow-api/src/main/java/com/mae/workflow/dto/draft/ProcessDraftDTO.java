package com.mae.workflow.dto.draft;

import java.util.Date;

public class ProcessDraftDTO extends ProcessDraftBaseDTO {
    private String id;
    private String createBy;
    private Date createdTime;
    private String updateBy;
    private Date updatedTime;
}
