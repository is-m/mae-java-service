package com.mae.workflow.dto.process;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Schema(title = "流程启动DTO")
@Getter
@Setter
public class ProcessStartDTO {
    @Schema(title = "草稿ID，不为空启动流程时自动删除草稿")
    private String draftId;
    @Schema(title = "流程实例ID")
    private String procInstId;
    @Schema(title = "表单变量，因为这里是流程启动，所以这个表单变量表示主表单")
    private Map<String, Object> variables;
}
