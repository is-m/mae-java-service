package com.mae.workflow.dto.process;

import com.mae.core.entity.TenantEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleProcessInstanceDTO extends TenantEntity {
    /**
     * 流程名称，流程标题
     */
    private String name;

    /**
     * 流程分组，业务类型
     */
    private String groupName;

    /**
     * 流程发起人
     */
    private String startUserId;

    /**
     * 启动时间
     */
    private String startTime;

    /**
     * 流程状态, 0 待认领 1 审批中，2 挂起，3 终止，4 完成
     */
    private int status;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务处理人
     */
    private String taskAssignee;
}
