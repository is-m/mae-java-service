package com.mae.workflow.dto.todo;

import com.mae.core.PageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * 我参与的
 */
@Getter
@Setter
public class MyApplicationPageQueryDTO extends PageQuery {
    /**
     * 关键字查询，（流程名称，流程实例ID）
     */
    private String keyword;

    /**
     * 申请人ID
     */
    private String startUserId;

    /**
     * 流程开始时间（开始）
     */
    private String startTimeBegin;

    /**
     * 流程开始时间（结束）
     */
    private String startTimeEnd;

    /**
     * 当前用户ID，由接口内部设置，外部传入无效
     */
    @Schema(hidden = true)
    private String userId;
}
