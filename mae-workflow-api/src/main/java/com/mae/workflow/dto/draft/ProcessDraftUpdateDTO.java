package com.mae.workflow.dto.draft;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class ProcessDraftUpdateDTO {
    private Map<String, Object> mainform;
}
