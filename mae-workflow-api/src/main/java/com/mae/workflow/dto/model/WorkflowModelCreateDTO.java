package com.mae.workflow.dto.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowModelCreateDTO {
    @NotEmpty
    @Size(max = 50)
    @Schema(title = "模型名称", example = "示例1")
    private String name;

    @NotEmpty
    @Size(max = 255)
    @Schema(title = "模型英文名称", example = "demo1")
    private String nameEn;

    @Size(max = 10)
    @Schema(title = "模型业务类型，general 通用, 数据字典=App.Lookup.WorkflowModelCategory", example = "general")
    private String category;

    @Pattern(regexp = "(draft|save)", message = "模型状态只能为 draft 或者 save")
    @Schema(title = "模型状态, 草稿 draft,保存 save, 启用 valid ,停用 invalid, 数据字典=App.Lookup.WorkflowModelStatus", example = "save")
    private String status;
}
