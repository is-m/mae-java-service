package com.mae.workflow.dto.draft;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class ProcessDraftCreateDTO {
    /**
     * 流程 KEY
     */
    private String keycode;

    /**
     * 表单值
     */
    private Map<String, Object> mainform;
}
