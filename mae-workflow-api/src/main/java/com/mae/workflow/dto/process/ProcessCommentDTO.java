package com.mae.workflow.dto.process;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ProcessCommentDTO {
    /**
     * 日志ID
     */
    private String id;
    /**
     * 日志类型
     */
    private String type;
    /**
     * 操作备注
     */
    private String msg;
    /**
     * 操作人
     */
    private String userId;

    /**
     * 日志产生时间
     */
    private Date time;

    @Schema(title = "任务ID")
    private String taskId;

    @Schema(title = "是否存在表单差异，true 存在，false 不存在")
    private boolean existsDifference;
}
