package com.mae.web.support.config;

import com.mae.core.utils.Mae;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
public class TraceBeanPostProcessor implements BeanPostProcessor {
    private static final AtomicInteger count = new AtomicInteger(0);

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        int i = count.incrementAndGet();
        // 第一个加载bean的时候触发应用启动事件
        String clz = bean.getClass().getName();
        if (Objects.equals(beanName, clz)) {
            log.debug("loaded {} bean {}", i, clz);
        } else {
            log.debug("loaded {} bean {} - {} ", i, beanName, clz);
        }
        Mae.registerBean(beanName, bean, false);
        return bean;
    }
}
