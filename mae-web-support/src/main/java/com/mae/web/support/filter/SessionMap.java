package com.mae.web.support.filter;

import jakarta.servlet.http.HttpSession;

import java.util.*;

public class SessionMap implements Map<String, Object> {
    private final HttpSession delegate;

    public SessionMap(HttpSession session) {
        this.delegate = session;
    }

    public HttpSession getDelegate() {
        return delegate;
    }

    @Override
    public int size() {
        Enumeration<String> attributeNames = delegate.getAttributeNames();
        int size = 0;
        while (attributeNames.hasMoreElements()) {
            size++;
            attributeNames.nextElement();
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return delegate.getAttributeNames().hasMoreElements();
    }

    @Override
    public boolean containsKey(Object key) {
        Enumeration<String> attributeNames = delegate.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String name = attributeNames.nextElement();
            if (Objects.equals(key, name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        Enumeration<String> attributeNames = delegate.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String name = attributeNames.nextElement();
            Object attribute = delegate.getAttribute(name);
            if (Objects.equals(value, attribute)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object get(Object key) {
        return delegate.getAttribute((String) key);
    }

    @Override
    public Object put(String key, Object value) {
        delegate.setAttribute(key, value);
        return value;
    }

    @Override
    public Object remove(Object key) {
        Object attribute = delegate.getAttribute((String) key);
        delegate.removeAttribute((String) key);
        return attribute;
    }

    @Override
    public void putAll(Map<? extends String, ?> m) {
        m.forEach(delegate::setAttribute);
    }

    @Override
    public void clear() {
        Enumeration<String> attributeNames = delegate.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String name = attributeNames.nextElement();
            delegate.removeAttribute(name);
        }
    }

    @Override
    public Set<String> keySet() {
        Enumeration<String> attributeNames = delegate.getAttributeNames();
        Set<String> keySet = new HashSet<>();
        while (attributeNames.hasMoreElements()) {
            String name = attributeNames.nextElement();
            keySet.add(name);
        }
        return Collections.unmodifiableSet(keySet);
    }

    @Override
    public Collection<Object> values() {
        Enumeration<String> attributeNames = delegate.getAttributeNames();
        List<Object> values = new ArrayList<>(this.size());
        while (attributeNames.hasMoreElements()) {
            String name = attributeNames.nextElement();
            Object attribute = delegate.getAttribute(name);
            values.add(attribute);
        }
        return Collections.unmodifiableList(values);
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        Enumeration<String> attributeNames = delegate.getAttributeNames();
        Set<Entry<String, Object>> entrySet = new HashSet<>(this.size());
        while (attributeNames.hasMoreElements()) {
            String name = attributeNames.nextElement();
            entrySet.add(new SessionMapEntry(delegate, name));
        }
        return Collections.unmodifiableSet(entrySet);
    }

    private static class SessionMapEntry implements Map.Entry<String, Object> {
        private final HttpSession delegate;
        private final String key;

        SessionMapEntry(HttpSession delegate, String key) {
            this.delegate = delegate;
            this.key = key;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Object getValue() {
            return delegate.getAttribute(key);
        }

        @Override
        public Object setValue(Object value) {
            delegate.setAttribute(key, value);
            return value;
        }
    }
}
