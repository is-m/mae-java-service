package com.mae.web.support.config;

import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.mae")
@ServletComponentScan(basePackages = "com.mae")
public class GlobalComponentScanConfig {
}
