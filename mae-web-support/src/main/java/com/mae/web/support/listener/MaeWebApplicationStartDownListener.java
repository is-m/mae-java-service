package com.mae.web.support.listener;

import com.mae.core.event.ApplicationBeforeStartEvent;
import com.mae.core.event.ApplicationStoppedEvent;
import com.mae.core.utils.ApplicationUtils;
import com.mae.core.utils.EventUtils;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MaeWebApplicationStartDownListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        // EventUtils.trigger(new ApplicationBeforeStartEvent(this));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        EventUtils.trigger(new ApplicationStoppedEvent(this));
    }

    /*
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if(event instanceof ServletWebServerInitializedEvent){
            log.info("Mae application servlet server initialized.");
        }
        if(event instanceof ContextStartedEvent){
            log.info("Mae application starting.");
        }

        if(event instanceof ContextRefreshedEvent){
            log.info("Mae application context ready.");
        }

        if(event instanceof ContextClosedEvent){
            log.info("Mae application context closed.");
        }

        if(event instanceof ContextStoppedEvent){
            log.info("Mae application context stopped.");
        }
    }*/

}
