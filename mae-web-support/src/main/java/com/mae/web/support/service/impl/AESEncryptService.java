package com.mae.web.support.service.impl;

import com.mae.core.service.IEncryptService;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

@Primary
@Component("AES")
@ConfigurationProperties(prefix = "encryption")
public class AESEncryptService implements IEncryptService {

    private String secret;

    @Override
    public String encrypt(String text) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        byte[] keyBytes = new byte[16];
        System.arraycopy(secret.getBytes(), 0, keyBytes, 0, Math.min(keyBytes.length, secret.getBytes().length));
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] encryptedBytes = cipher.doFinal(text.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    @Override
    public String decrypt(String encryptedText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        byte[] keyBytes = new byte[16];
        System.arraycopy(secret.getBytes(), 0, keyBytes, 0, Math.min(keyBytes.length, secret.getBytes().length));
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
        return new String(decryptedBytes);
    }
}
