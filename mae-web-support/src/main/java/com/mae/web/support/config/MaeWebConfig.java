package com.mae.web.support.config;

import com.mae.core.spi.BeanManager;
import com.mae.core.spi.EventPublisher;
import com.mae.web.support.config.delegation.SpringBeanManager;
import com.mae.web.support.config.delegation.SpringEventPublisher;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MaeWebConfig {

    @ConditionalOnMissingBean
    @Bean
    public EventPublisher eventPublisherDelegate() {
        return new SpringEventPublisher();
    }

    @ConditionalOnMissingBean
    @Bean
    public BeanManager beanFactoryDelegate() {
        return new SpringBeanManager();
    }
}
