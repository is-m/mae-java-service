package com.mae.web.support.config.delegation;

import com.mae.core.spi.BeanManager;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class SpringBeanManager implements BeanManager, BeanFactoryPostProcessor {
    private ConfigurableListableBeanFactory delegate;

    @Override
    public void registerBean(String name, Object bean) {
        delegate.registerSingleton(name, bean);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        this.delegate = beanFactory;
    }
}
