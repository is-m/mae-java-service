package com.mae.web.support.filter;

import com.mae.core.constant.RequestConstant;
import com.mae.core.context.RequestContext;
import com.mae.core.context.RequestContextManager;
import com.mae.core.context.UserPrincipal;
import com.mae.core.event.RequestContextDestroyEvent;
import com.mae.core.event.RequestContextReadyEvent;
import com.mae.core.utils.EventUtils;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class RequestFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        RequestContext requestContext = buildRequestContext(request);
        RequestContextManager.set(requestContext);
        EventUtils.trigger(new RequestContextReadyEvent(this, requestContext));
        String oldName = rewriteThreadName();
        String method = request.getMethod();
        log.info("{} {}", method, request.getRequestURI());
        long start = System.currentTimeMillis();
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            log.info("request end cost {} ms", System.currentTimeMillis() - start);
            Thread.currentThread().setName(oldName);
            RequestContextManager.remove();
            EventUtils.trigger(new RequestContextDestroyEvent(this, requestContext));
        }
    }

    private RequestContext buildRequestContext(HttpServletRequest request) {
        String traceId = Optional.ofNullable(request.getHeader(RequestConstant.KEY_TRACE_ID)).orElse(newTraceId());
        UserPrincipal user = new UserPrincipal() {
        };
        return new RequestContext() {
            private final Map<String, Object> attributes = new HashMap<>();

            @Override
            public UserPrincipal getUser() {
                return user;
            }

            @Override
            public String getTraceId() {
                return traceId;
            }

            @Override
            public void setAttribute(String key, Object value) {
                attributes.put(key, value);
            }

            @Override
            public Object getAttribute(String key) {
                return attributes.get(key);
            }

            @Override
            public Map<String, Object> getSession() {
                return new SessionMap(request.getSession());
            }
        };
    }

    // https://github.com/sofastack-guides/sofa-tracer-guides/tree/master/tracer-sample-with-springmvc
    // 可以使用其他框架
    private String newTraceId() {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(18);
    }

    // 重新请求线程名字
    private String rewriteThreadName() {
        String oldName = Thread.currentThread().getName();
        RequestContext current = RequestContext.getCurrent();
        Thread.currentThread().setName(String.format("http-%s-%s", current.getUser().getEmpNo(), current.getTraceId()));
        return oldName;
    }
}
