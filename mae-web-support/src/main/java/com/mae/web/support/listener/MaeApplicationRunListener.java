package com.mae.web.support.listener;

import com.mae.core.context.RequestContextManager;
import com.mae.core.event.ApplicationBeforeStartEvent;
import com.mae.core.spi.Environment;
import com.mae.core.utils.EventUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.time.Duration;

/**
 * 应用运行监听器
 * register for spring.factories
 *
 * https://www.cnblogs.com/kukuxjx/p/17373029.html
 */
@Slf4j
public class MaeApplicationRunListener implements SpringApplicationRunListener {

    /**
     * Called immediately when the run method has first started. Can be used for very
     * early initialization.
     * @param bootstrapContext the bootstrap context
     */
    public void starting(ConfigurableBootstrapContext bootstrapContext) {
        log.info("Application starting");
        // 应用启动前初始化当前请求上下文，防止启动过程中
        RequestContextManager.initForApplicationStarting();

        //System.out.println("Application starting");
    }

    /**
     * Called once the environment has been prepared, but before the
     * {@link ApplicationContext} has been created.
     * @param bootstrapContext the bootstrap context
     * @param environment the environment
     */
    public void environmentPrepared(ConfigurableBootstrapContext bootstrapContext,
                                     ConfigurableEnvironment environment) {
        log.info("Application environmentPrepared");
        EventUtils.trigger(new ApplicationBeforeStartEvent(this, new Environment() {
            @Override
            public String getProperty(String key) {
                return environment.getProperty(key);
            }

            @Override
            public String getProperty(String key, String defaultValue) {
                return environment.getProperty(key, defaultValue);
            }

            @Override
            public Object resolvePlaceholder(Object obj, String prefix) {
                return "environment.place";
            }
        }));
    }

    /**
     * Called once the {@link ApplicationContext} has been created and prepared, but
     * before sources have been loaded.
     * @param context the application context
     */
    public void contextPrepared(ConfigurableApplicationContext context) {
        log.info("Application contextPrepared");
    }

    /**
     * Called once the application context has been loaded but before it has been
     * refreshed.
     * @param context the application context
     */
    public void contextLoaded(ConfigurableApplicationContext context) {
        log.info("Application contextLoaded");
    }

    /**
     * The context has been refreshed and the application has started but
     * {@link CommandLineRunner CommandLineRunners} and {@link ApplicationRunner
     * ApplicationRunners} have not been called.
     * @param context the application context.
     * @param timeTaken the time taken to start the application or {@code null} if unknown
     * @since 2.6.0
     */
    public void started(ConfigurableApplicationContext context, Duration timeTaken) {
        log.info("Application started");
        RequestContextManager.remove();
    }

    /**
     * Called immediately before the run method finishes, when the application context has
     * been refreshed and all {@link CommandLineRunner CommandLineRunners} and
     * {@link ApplicationRunner ApplicationRunners} have been called.
     * @param context the application context.
     * @param timeTaken the time taken for the application to be ready or {@code null} if
     * unknown
     * @since 2.6.0
     */
    public void ready(ConfigurableApplicationContext context, Duration timeTaken) {
        log.info("Application ready");
    }

    /**
     * Called when a failure occurs when running the application.
     * @param context the application context or {@code null} if a failure occurred before
     * the context was created
     * @param exception the failure
     * @since 2.0.0
     */
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        log.error("Application failed");
    }
}
