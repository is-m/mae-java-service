package com.mae.web.support.config;

import com.mae.core.ReturnErrors;
import com.mae.core.exception.Error;
import com.mae.core.exception.ErrorMeta;
import com.mae.core.exception.MaeException;
import com.mae.logging.SystemErrorLogVO;
import com.mae.logging.service.ISystemErrorLogService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class RestExceptionHandler {
    @Autowired
    private ISystemErrorLogService systemErrorLogService;

    /**
     * 全局异常处理
     *
     * @param exception exception
     * @return ResponseEntity
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ReturnErrors> handleException(HttpServletRequest request, Exception exception) {
        writeLog(request, exception, ReturnErrors.SINGLETON_INTERNAL_ERROR);
        return new ResponseEntity<>(ReturnErrors.SINGLETON_INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 自定义异常处理
     *
     * @param exception exception
     * @return ResponseEntity
     */
    @ExceptionHandler(MaeException.class)
    public ResponseEntity<ReturnErrors> handleMaeException(HttpServletRequest request, MaeException exception) {
        ReturnErrors returnErrors = new ReturnErrors(exception.getErrors());
        writeLog(request, exception, returnErrors);
        return new ResponseEntity<>(returnErrors, HttpStatus.valueOf(exception.getTopStatus()));
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<ReturnErrors> handleBindException(HttpServletRequest request, BindException exception) {
        List<Error> errors = exception.getBindingResult().getFieldErrors().stream().map(fieldError -> {
            ErrorMeta meta = new ErrorMeta();
            meta.setSource(fieldError.getField());
            return new Error(400, "Parameter Error", fieldError.getDefaultMessage(), meta);
        }).collect(Collectors.toList());
        ReturnErrors returnErrors = new ReturnErrors(errors);
        writeLog(request, exception, returnErrors);
        return new ResponseEntity<>(returnErrors, HttpStatus.BAD_REQUEST);
    }

    private void writeLog(HttpServletRequest request, Exception error, ReturnErrors errors) {
        error.printStackTrace();

        SystemErrorLogVO logVO = new SystemErrorLogVO();
        logVO.initForCreate();
        logVO.setMessage(error.getMessage());
        logVO.setFullMessage(ExceptionUtils.getStackTrace(error));
        logVO.setType("api");
        logVO.setSource(request.getRequestURI());
        try {
            // 这里使用db
            systemErrorLogService.createSystemErrorLog(logVO);
        } catch (Exception createException) {
            log.warn("write system error fail", createException);
            log.error("request uri {} has error", request.getRequestURI(), error);
        }
    }
}
