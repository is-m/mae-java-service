package com.mae.web.support.config;

import com.mae.web.support.filter.RequestFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

@Configuration
public class ServletConfiguration {

    @Bean
    @ConditionalOnMissingBean(name = "maeRequestFilter")
    public FilterRegistrationBean<RequestFilter> maeRequestFilter() {
        FilterRegistrationBean<RequestFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new RequestFilter());
        registrationBean.setBeanName("maeRequestFilter");
        registrationBean.setOrder(0);
        registrationBean.setUrlPatterns(Collections.singletonList("/*"));
        return registrationBean;
    }



}
