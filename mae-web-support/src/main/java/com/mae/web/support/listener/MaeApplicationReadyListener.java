package com.mae.web.support.listener;

import com.mae.core.context.ApplicationContext;
import com.mae.core.utils.Mae;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@Component
public class MaeApplicationReadyListener implements ApplicationListener<ApplicationReadyEvent> {
    @SneakyThrows
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        ConfigurableApplicationContext context = event.getApplicationContext();
        ConfigurableEnvironment environment = context.getEnvironment();
        String port = environment.getProperty("server.port", "8080");
        String contextPath = environment.getProperty("server.servlet.context-path", "/");

        Mae.setContext(new ApplicationContext() {
            @Override
            public Object getDelegate() {
                return context;
            }

            @Override
            public <T> T getBean(Class<T> clazz) {
                return context.getBean(clazz);
            }

            @Override
            public <T> T getBean(String name, Class<T> clazz) {
                return context.getBean(name, clazz);
            }
        });

        String ip = InetAddress.getLocalHost().getHostAddress();
        log.info("应用启动成功");
        log.info("本地访问：http://localhost:{}{}", port, contextPath);
        log.info("网络访问：http://{}:{}{}", ip, port, contextPath);
        log.info("本地接口：http://localhost:{}{}swagger-ui/index.html", port, contextPath);
        log.info("网络接口：http://{}:{}{}swagger-ui/index.html", ip, port, contextPath);

        String arthasHttpPort = environment.getProperty("arthas.httpPort", "8563");
        String arthasTelnetPort = environment.getProperty("arthas.telnetPort", "3658");
        try {
            Class.forName("com.taobao.arthas.agent.attach.ArthasAgent");
            log.info("Arthas Http Endpoint：http://127.0.0.1:{}", arthasHttpPort);
            log.info("Arthas Telnet Endpoint：127.0.0.1 {}", arthasTelnetPort);
        } catch (ClassNotFoundException ignore) {}
    }
}
