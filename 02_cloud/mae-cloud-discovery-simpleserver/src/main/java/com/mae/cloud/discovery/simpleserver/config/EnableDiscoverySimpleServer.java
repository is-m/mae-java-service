package com.mae.cloud.discovery.simpleserver.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@ComponentScan("com.mae.cloud.discovery.simpleserver")
@Import(EnableDiscoverySimpleServer.EnableDiscoverySimpleServerImport.class)
public @interface EnableDiscoverySimpleServer {

    class EnableDiscoverySimpleServerImport {




    }
}
