package com.mae.cloud.discovery.simpleclient.registration;

import com.mae.cloud.discovery.simpleclient.config.MaeRegistrationConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;

@Slf4j
public class CommonServiceAutoRegistration implements InitializingBean {
    private MaeRegistrationProperties properties;


    public CommonServiceAutoRegistration(MaeRegistrationProperties properties){
        this.properties = properties;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("register self common service");


    }
}
