package com.mae.cloud.discovery.simpleclient.registration;

import com.mae.rpc.utils.HttpUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.URI;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * 配置类
 */
@Slf4j
@Getter
public class MaeRegistrationProperties implements InitializingBean {
    private static final String HEALTH_CHECK_SUFFIX = "/actuator/health";
    @Value("${spring.cloud.mae.discovery.enable:true}")
    private boolean enabled;
    @Value("${spring.application.name:}")
    private String serviceId;
    @Value("${eureka.client.service-url.defaultZone:http://127.0.0.1:8000}")
    private String regServerCluster;
    @Value("${server.port:80}")
    private int port;
    private boolean secure;
    private Map<String, String> metadata;
    // 可用的注册中心地址
    private URI availableRegServer;
    // 服务实例信息
    private ServiceInstance serviceInstance;

    public String getHost() {
        try {
            InetAddress address = InetAddress.getLocalHost();
            return address.getHostAddress();
        } catch (Exception e) {
            log.warn("inet address get host address has error", e);
            return "127.0.0.1";
        }
    }

    /**
     * 获取可用的注册中心地址
     *
     * @return
     */
    public String getAvailableUrl() {
        String[] servers = regServerCluster.split(",");
        List<String> healthUrls = Stream.of(servers).map(server -> server + HEALTH_CHECK_SUFFIX).toList();
        try {
            HttpResponse<String> httpResponse = HttpUtils.anyGet(healthUrls);
            String url = httpResponse.request().uri().toString();
            //availableRegServer = URI.create(url.substring(0, url.indexOf(HEALTH_CHECK_SUFFIX)));
            return url.substring(0, url.indexOf(HEALTH_CHECK_SUFFIX));
        } catch (Exception e) {
            throw new IllegalStateException("no registration service available", e);
        }
    }

    /**
     * 设置端口
     *
     * @param toPort
     */
    public void setPort(int toPort) {
        if (!Objects.equals(this.port, toPort)) {
            log.warn("the port {} change to {}", this.port, toPort);
            this.port = toPort;

            // FIXME：看看是否需要联动处理端口变化事件
            ((DefaultServiceInstance)getServiceInstance()).setPort(this.port);
        }
    }

    /**
     * 获取注册服务地址
     *
     * @return
     */
    public String getRegisterUrl() {
        return getAvailableUrl() + "/service-instance/register";
    }

    /**
     * 获取取消注册服务地址
     *
     * @return
     */
    public String getDeregisterUrl() {
        return getAvailableUrl() + "/service-instance/deregister";
    }

    public ServiceInstance getServiceInstance() {
        if (serviceInstance == null) {
            DefaultServiceInstance defaultServiceInstance = new DefaultServiceInstance();
            defaultServiceInstance.setServiceId(serviceId);
            defaultServiceInstance.setInstanceId(getHost() + ":" + port + ":" + serviceId);
            defaultServiceInstance.setPort(port);
            defaultServiceInstance.setSecure(secure);
            defaultServiceInstance.setHost(getHost());
            String url = this.getAvailableUrl();
            defaultServiceInstance.setUri(URI.create(url));
            serviceInstance = defaultServiceInstance;
            log.info("service instance really {}", defaultServiceInstance);
        }
        return serviceInstance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
