package com.mae.cloud.discovery.simpleclient.registration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Map;

@Slf4j
public class MaeRegistration implements Registration {

    private MaeRegistrationProperties properties;

    public MaeRegistration(MaeRegistrationProperties properties) {
        this.properties = properties;
    }

    @Override
    public String getServiceId() {
        return properties.getServiceId();
    }

    @Override
    public String getHost() {
        return properties.getHost();
    }

    @Override
    public int getPort() {
        return properties.getPort();
    }

    @Override
    public boolean isSecure() {
        return properties.isSecure();
    }

    @Override
    public URI getUri() {
        String url = properties.getAvailableUrl();
        return URI.create(url);
    }

    @Override
    public Map<String, String> getMetadata() {
        return properties.getMetadata();
    }

    public MaeRegistrationProperties getProperties() {
        return properties;
    }
}
