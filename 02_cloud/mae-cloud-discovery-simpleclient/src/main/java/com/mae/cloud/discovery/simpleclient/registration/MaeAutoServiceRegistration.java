package com.mae.cloud.discovery.simpleclient.registration;


import org.springframework.cloud.client.serviceregistry.AbstractAutoServiceRegistration;
import org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationProperties;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;

/**
 * 应用启动时的服务注册
 */
public class MaeAutoServiceRegistration extends AbstractAutoServiceRegistration<MaeRegistration> {
    private MaeRegistration registration;

    public MaeAutoServiceRegistration(ServiceRegistry<MaeRegistration> serviceRegistry, AutoServiceRegistrationProperties properties, MaeRegistration registration) {
        super(serviceRegistry, properties);
        this.registration = registration;
    }

    @Override
    protected Object getConfiguration() {
        return registration.getProperties();
    }

    @Override
    protected boolean isEnabled() {
        return registration.getProperties().isEnabled();
    }

    @Override
    protected MaeRegistration getRegistration() {
        int port = this.getPort().get();
        if (port > 0) {
            registration.getProperties().setPort(port);
        }
        return registration;
    }

    @Override
    protected MaeRegistration getManagementRegistration() {
        return null;
    }
}
