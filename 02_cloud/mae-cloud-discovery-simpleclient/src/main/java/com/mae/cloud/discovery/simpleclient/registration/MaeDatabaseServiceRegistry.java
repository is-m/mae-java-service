package com.mae.cloud.discovery.simpleclient.registration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;

/**
 * 数据库注册
 */
@Slf4j
public class MaeDatabaseServiceRegistry implements ServiceRegistry<MaeRegistration> {

    @Override
    public void register(MaeRegistration registration) {

    }

    @Override
    public void deregister(MaeRegistration registration) {

    }

    @Override
    public void close() {

    }

    @Override
    public void setStatus(MaeRegistration registration, String status) {

    }

    @Override
    public <T> T getStatus(MaeRegistration registration) {
        return null;
    }
}
