package com.mae.cloud.discovery.simpleclient.registration;

import com.mae.cloud.discovery.simpleclient.spi.IRegistrationService;
import com.mae.rpc.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;

@Slf4j
public class MaeServiceRegistry implements ServiceRegistry<MaeRegistration> {
    private MaeRegistrationProperties properties;
    private IRegistrationService registrationService;

    public MaeServiceRegistry(IRegistrationService registrationService, MaeRegistrationProperties properties) {
        this.registrationService = registrationService;
        this.properties = properties;
    }

    @Override
    public void register(MaeRegistration registration) {
        log.info("service register");
        registrationService.register(properties.getServiceInstance());
        log.info("service register finished!");
        // TODO 服务注册
    }

    @Override
    public void deregister(MaeRegistration registration) {
        log.info("service deregister");
        registrationService.deregister(properties.getServiceInstance());
        log.info("service deregister finished!");
        // TODO 取消服务注册
    }

    @Override
    public void close() {
        log.warn("service close");
    }

    @Override
    public void setStatus(MaeRegistration registration, String status) {
        log.warn("service set status {}", status);
    }

    @Override
    public <T> T getStatus(MaeRegistration registration) {
        log.warn("service get status");
        return null;
    }
}
