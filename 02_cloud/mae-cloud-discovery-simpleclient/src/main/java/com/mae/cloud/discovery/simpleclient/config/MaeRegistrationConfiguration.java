package com.mae.cloud.discovery.simpleclient.config;

import com.mae.cloud.discovery.simpleclient.registration.*;
import com.mae.cloud.discovery.simpleclient.spi.IRegistrationService;
import com.mae.cloud.discovery.simpleclient.spi.impl.RemoteRegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationAutoConfiguration;
import org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationConfiguration;
import org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationProperties;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@ConditionalOnProperty(value = "spring.cloud.mae.discovery.enable", matchIfMissing = true, havingValue = "true")
@AutoConfigureAfter({AutoServiceRegistrationConfiguration.class, AutoServiceRegistrationAutoConfiguration.class})
public class MaeRegistrationConfiguration {
    @Bean
    @ConfigurationProperties("spring.cloud.mae.discovery")
    public MaeRegistrationProperties maeRegistrationProperties() {
        return new MaeRegistrationProperties();
    }

    /**
     * 上报的信息
     *
     * @param maeRegistrationProperties
     * @return
     */
    @Bean
    public MaeRegistration maeRegistration(MaeRegistrationProperties maeRegistrationProperties) {
        return new MaeRegistration(maeRegistrationProperties);
    }

    /**
     * 注册，注销，关闭接口
     *
     * @return
     */
    @Bean
    public MaeServiceRegistry maeServiceRegistry(IRegistrationService registrationService, MaeRegistrationProperties maeRegistrationProperties) {
        return new MaeServiceRegistry(registrationService, maeRegistrationProperties);
    }

    @Bean
    public MaeAutoServiceRegistration maeAutoServiceRegistration(ServiceRegistry<MaeRegistration> serviceRegistry, AutoServiceRegistrationProperties autoServiceRegistrationProperties, MaeRegistration maeRegistration) {
        return new MaeAutoServiceRegistration(serviceRegistry, autoServiceRegistrationProperties, maeRegistration);
    }

    @ConditionalOnMissingClass("com.mae.starter.simplecommonservice.config.SimpleCommonServiceAutoConfig")
    @Configuration
    public class NonCommonServiceConfiguration {
        public NonCommonServiceConfiguration() {
            log.info("bunisess service init");
        }

        /**
         * 远程注册服务
         * @param maeRegistrationProperties
         * @return
         */
        @Bean
        public RemoteRegistrationService remoteRegistrationService(MaeRegistrationProperties maeRegistrationProperties) {
            return new RemoteRegistrationService(maeRegistrationProperties);
        }


    }

    /**
     * 公共服务注册
     */
    @ConditionalOnClass(name = "com.mae.starter.simplecommonservice.config.SimpleCommonServiceAutoConfig")
    @Configuration
    public class CommonServiceConfiguration {
        public CommonServiceConfiguration() {
            log.info("common service init");
        }

    }

}
