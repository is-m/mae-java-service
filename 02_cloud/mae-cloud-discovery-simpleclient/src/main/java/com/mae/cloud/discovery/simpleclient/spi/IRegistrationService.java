package com.mae.cloud.discovery.simpleclient.spi;

import org.springframework.cloud.client.ServiceInstance;

import java.security.Provider;

/**
 *
 */
public interface IRegistrationService {
    /**
     * 服务注册
     *
     * @param serviceInstance
     */
    void register(ServiceInstance serviceInstance);

    /**
     * 取消注册
     *
     * @param serviceInstance
     */
    void deregister(ServiceInstance serviceInstance);


}
