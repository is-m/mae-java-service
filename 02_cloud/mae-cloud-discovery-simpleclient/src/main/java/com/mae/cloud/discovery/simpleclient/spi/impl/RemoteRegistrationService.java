package com.mae.cloud.discovery.simpleclient.spi.impl;

import com.mae.cloud.discovery.simpleclient.registration.MaeRegistrationProperties;
import com.mae.cloud.discovery.simpleclient.spi.IRegistrationService;
import com.mae.rpc.utils.HttpUtils;
import org.springframework.cloud.client.ServiceInstance;

/**
 * 远程注册服务
 */
public class RemoteRegistrationService implements IRegistrationService {
    private MaeRegistrationProperties properties;

    public RemoteRegistrationService(MaeRegistrationProperties properties) {
        this.properties = properties;
    }

    @Override
    public void register(ServiceInstance serviceInstance) {
        HttpUtils.post(properties.getRegisterUrl(), serviceInstance);
    }

    @Override
    public void deregister(ServiceInstance serviceInstance) {
        HttpUtils.post(properties.getDeregisterUrl(), serviceInstance);
    }
}
