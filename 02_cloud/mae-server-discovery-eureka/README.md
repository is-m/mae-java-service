服务注册与发现

首页
http://localhost:8761/
服务页
http://localhost:8761/eureka/apps


https://blog.csdn.net/peixiancccc/article/details/138430749

https://github.com/Netflix/eureka/wiki/Eureka-REST-operations

| Operation | HTTP action | Description |

| — | — | — |

| Register new application instance | POST /eureka/v2/apps/appID | Input: JSON/XMLpayload HTTPCode: 204 on success |

| De-register application instance | DELETE /eureka/v2/apps/appID/instanceID | HTTP Code: 200 on success |

| Send application instance heartbeat | PUT /eureka/v2/apps/appID/instanceID | HTTP Code: * 200 on success * 404 if instanceIDdoesn’t exist |

| Query for all instances | GET /eureka/v2/apps | HTTP Code: 200 on success Output: JSON/XML |

| Query for all appID instances | GET /eureka/v2/apps/appID | HTTP Code: 200 on success Output: JSON/XML |

| Query for a specific appID/instanceID | GET /eureka/v2/apps/appID/instanceID | HTTP Code: 200 on success Output: JSON/XML |

| Query for a specific instanceID | GET /eureka/v2/instances/instanceID | HTTP Code: 200 on success Output: JSON/XML |

| Take instance out of service | PUT /eureka/v2/apps/appID/instanceID/status?value=OUT_OF_SERVICE | HTTP Code: * 200 on success * 500 on failure |

| Move instance back into service (remove override) | DELETE /eureka/v2/apps/appID/instanceID/status?value=UP (The value=UP is optional, it is used as a suggestion for the fallback status due to removal of the override) | HTTP Code: * 200 on success * 500 on failure |

| Update metadata | PUT /eureka/v2/apps/appID/instanceID/metadata?key=value | HTTP Code: * 200 on success * 500 on failure |

| Query for all instances under a particular vip address | GET /eureka/v2/vips/vipAddress | * HTTP Code: 200 on success Output: JSON/XML * 404 if the vipAddressdoes not exist. |

| Query for all instances under a particular secure vip address | GET /eureka/v2/svips/svipAddress | * HTTP Code: 200 on success Output: JSON/XML * 404 if the svipAddressdoes not exist. |

查询所有的服务：
http://localhost:8761/eureka/apps

或者某个服务：
http://localhost:8761/eureka/instances/DESKTOP-GRIPBVL:Provider1:6060
http://localhost:8761/eureka/apps/PROVIDER1

服务状态
http://localhost:8761/eureka/status

基于 RabbitMQ 实现 Eureka服务平滑灰度发布
https://juejin.cn/post/7362238093485539378

上一任留下的 Eureka，我该如何提升她的性能和稳定性（含数据比对）？
https://juejin.cn/post/7327878308757504035

Eureka 多环境隔离方案（包含本地开发人员间隔离）
https://segmentfault.com/a/1190000041632921

微服务边界守卫：Eureka中服务隔离策略的实现
https://blog.csdn.net/2401_85742452/article/details/140445795

Eureka使用技巧： 服务间调用时，巧妙利用zone，指定要调用哪个实例
https://www.cnblogs.com/xushengbin/p/17988551