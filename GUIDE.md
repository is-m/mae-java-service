FLYWAY


flyway locations 多模块脚本
在使用Flyway进行数据库迁移时，如果你的项目有多个模块需要执行数据库迁移脚本，你可以为每个模块配置不同的脚本路径。Flyway允许你指定locations属性，以便它可以加载不同位置的迁移脚本。

以下是一个示例配置，假设你有两个模块module-a和module-b，它们各自有自己的迁移脚本：

# 在 module-a 的配置文件中
flyway.locations=classpath:db/migration/module-a

# 在 module-b 的配置文件中
flyway.locations=classpath:db/migration/module-b
每个模块的迁移脚本应该放在各自模块的资源目录下，例如：

src/main/resources/db/migration/module-a/V1__Initial_setup.sql
src/main/resources/db/migration/module-a/V2__Add_users_table.sql
src/main/resources/db/migration/module-b/V1__Initial_setup.sql
src/main/resources/db/migration/module-b/V2__Add_products_table.sql
确保每个模块的初始化版本号是唯一的，以防止冲突。在实际部署时，Flyway会按照配置的locations顺序依次执行所有迁移脚本。