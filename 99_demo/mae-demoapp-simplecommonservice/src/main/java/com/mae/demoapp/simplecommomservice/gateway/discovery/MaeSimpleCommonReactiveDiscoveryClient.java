package com.mae.demoapp.simplecommomservice.gateway.discovery;

import com.mae.core.ReturnValue;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInfo;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoSingletonQuery;
import com.mae.starter.simplecommonservice.serviceinfo.service.IServiceQueryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
import reactor.core.publisher.Flux;

import java.net.URI;
import java.util.Collections;
import java.util.List;

@Slf4j
public class MaeSimpleCommonReactiveDiscoveryClient implements ReactiveDiscoveryClient {
    private IServiceQueryService serviceQueryService;

    public MaeSimpleCommonReactiveDiscoveryClient(IServiceQueryService serviceQueryService){
        this.serviceQueryService = serviceQueryService;
    }

    @Override
    public String description() {
        return "Mae Simple Common Reactive Discovery Client";
    }

    @Override
    public Flux<ServiceInstance> getInstances(String serviceId) {
        log.info("load reactive {} service instances", serviceId);
        ServiceInfoSingletonQuery serviceInfoSingletonQuery = ServiceInfoSingletonQuery.only(serviceId);
        serviceInfoSingletonQuery.setIncludeInstance(true);
        ReturnValue<ServiceInfo> returnValue = serviceQueryService.getOne(serviceInfoSingletonQuery);
        ServiceInfo content = returnValue.getContent();
        if(content != null){
            log.info("the service {} has {} instances found", serviceId, content.getInstances().size());
            List<ServiceInstance> serviceInstances = content.getInstances().stream().map(item -> {
                DefaultServiceInstance instance = new DefaultServiceInstance();
                instance.setPort(item.getPort());
                instance.setInstanceId(item.getId());
                instance.setServiceId(item.getServiceId());
                instance.setHost(item.getHost());
                instance.setUri(URI.create(item.getEndpoint()));
                instance.setSecure(item.getEndpoint().startsWith("https:"));
                return (ServiceInstance) instance;
            }).toList();
            return Flux.fromIterable(serviceInstances);
        }
        log.warn("the service {} is not found", serviceId);
        return Flux.empty();
    }

    @Override
    public Flux<String> getServices() {
        return Flux.fromIterable(serviceQueryService.getAllServices());
    }
}
