package com.mae.demoapp.simplecommomservice.config;

import com.mae.demoapp.simplecommomservice.gateway.discovery.MaeSimpleCommonReactiveDiscoveryClient;
import com.mae.starter.simplecommonservice.serviceinfo.service.IServiceQueryService;
import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleCommonServiceGatewayConfiguration {

    @Bean
    public ReactiveDiscoveryClient reactiveDiscoveryClient(IServiceQueryService serviceQueryService){
        return new MaeSimpleCommonReactiveDiscoveryClient(serviceQueryService);
    }

}
