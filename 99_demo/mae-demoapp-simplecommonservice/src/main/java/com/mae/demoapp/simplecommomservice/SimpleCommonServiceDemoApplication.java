package com.mae.demoapp.simplecommomservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan(basePackages = {"io.seata"})
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
//@PropertySource("classpath:application.properties")
@ImportResource("classpath:applicationContext.xml")
@EnableScheduling
public class SimpleCommonServiceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleCommonServiceDemoApplication.class, args);
    }

}
