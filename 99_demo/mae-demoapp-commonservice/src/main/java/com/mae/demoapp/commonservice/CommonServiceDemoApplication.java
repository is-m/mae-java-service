package com.mae.demoapp.commonservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan(basePackages = {"io.seata"})
@SpringBootApplication
@EnableScheduling
public class CommonServiceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonServiceDemoApplication.class, args);
    }

}
