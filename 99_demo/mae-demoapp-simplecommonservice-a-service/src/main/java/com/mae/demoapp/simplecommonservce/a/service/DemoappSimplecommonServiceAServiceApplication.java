package com.mae.demoapp.simplecommonservce.a.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class DemoappSimplecommonServiceAServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoappSimplecommonServiceAServiceApplication.class, args);
    }
}
