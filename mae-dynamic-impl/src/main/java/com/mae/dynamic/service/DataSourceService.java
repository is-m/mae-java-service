package com.mae.dynamic.service;

import com.mae.core.*;
import com.mae.core.utils.Assert;
import com.mae.core.utils.RequestContextUtils;
import com.mae.dynamic.DataSourcePageQuery;
import com.mae.dynamic.datasource.entity.DataSource;
import com.mae.dynamic.dao.DataSourceDao;
import com.mae.dynamic.utils.DataSourceUtils;
import com.mae.dynamic.utils.JdbcConnection;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.Driver;
import java.util.Properties;

@RestController
public class DataSourceService implements IDataSourceService {
    @Autowired
    private DataSourceDao dataSourceDao;

    @Valid
    @Override
    public ReturnValue<DataSource> createDataSource(@Valid DataSource dataSource) {
        // 检查是否存在名称冲突
        int repeatCount = dataSourceDao.countByTenantIdAndNameAndId(RequestContextUtils.getTenantId(), dataSource.getName(), null);
        Assert.isEquals(0, repeatCount, "数据源名称已经存在");

        dataSource.initForCreate();
        dataSourceDao.createDataSource(dataSource);

        return ReturnValue.of(dataSource);
    }

    @Override
    public ReturnMessage deleteDataSource(String id) {
        return null;
    }

    @Override
    public ReturnValue<DataSource> updateDataSource(DataSource dataSource) {
        return null;
    }

    @Override
    public ReturnValue<DataSource> findOne(String id) {
        DataSource dataSource = dataSourceDao.findOne(id);
        return ReturnValue.of(dataSource);
    }

    @Override
    public ReturnPagedCollection<DataSource> findPageList(DataSourcePageQuery query) {
        PagedResult<DataSource> pageList = dataSourceDao.findPageList(query);
        return ReturnPagedCollection.of(pageList);
    }

    @Override
    public ReturnMessage testConnection(DataSource dataSource) {
        if (!StringUtils.hasText(dataSource.getPassword())) {
            // 不包含密码时通过数据库获取密码
            Assert.notEmpty(dataSource.getId(), "密码不能为空");
            DataSource persistenceDataSource = dataSourceDao.findOne(dataSource.getId());
            dataSource.setPassword(persistenceDataSource.getPassword());
        }
        JdbcConnection jdbcConnection = DataSourceUtils.getJdbcConnection(dataSource.getDatabaseType());
        String jdbcDriver = jdbcConnection.getDriver();
        String jdbcUrl = jdbcConnection.formatJdbcUrl(dataSource.getHost(), dataSource.getPort(), dataSource.getDatabase());

        try {
            Driver driver = (Driver) Class.forName(jdbcDriver).newInstance();
            Properties properties = new Properties();
            properties.put("user", dataSource.getUsername());
            properties.put("password", dataSource.getPassword());
            Connection connection = driver.connect(jdbcUrl, properties);
            connection.close();
        } catch (Exception err) {
            throw new IllegalArgumentException("连接数据库错误", err);
        }
        return ReturnMessage.ok("测试连接成功");
    }
}
