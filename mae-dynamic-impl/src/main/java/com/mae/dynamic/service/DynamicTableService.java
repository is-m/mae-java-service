package com.mae.dynamic.service;

import com.mae.core.ReturnMessage;
import com.mae.core.ReturnValue;
import com.mae.dynamic.DynamicTableVO;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DynamicTableService implements IDynamicTableService {
    @Override
    public ReturnValue<DynamicTableVO> createDynamicTable(DynamicTableVO dynamicTableVO) {
        return null;
    }

    @Override
    public ReturnValue<DynamicTableVO> updateDynamicTable(DynamicTableVO dynamicTableVO) {
        return null;
    }

    @Override
    public ReturnMessage deleteTableColumn() {
        return null;
    }
}
