package com.mae.dynamic.service;

import com.mae.core.utils.Assert;
import com.mae.core.ReturnCollection;
import com.mae.core.ReturnValue;
import com.mae.core.exception.MaeException;
import com.mae.dynamic.*;
import com.mae.dynamic.datasource.entity.DataSource;
import com.mae.dynamic.utils.DataSourceUtils;
import com.mae.dynamic.utils.JdbcConnection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@Slf4j
@RestController
public class DataBaseService implements IDataBaseService {
    @Autowired
    private IDataSourceService dataSourceService;

    @Override
    public ReturnCollection<DataBaseVO> connectByDataSourceId(String dataSourceId) {
        DataSource dataSource = dataSourceService.findOne(dataSourceId).getContent();
        Assert.notNull(dataSource, "数据源不存在");
        JdbcConnection jdbcConnection = DataSourceUtils.getJdbcConnection(dataSource.getDatabaseType());
        String jdbcUrl = jdbcConnection.formatJdbcUrl(dataSource.getHost(), dataSource.getPort(), dataSource.getDatabase());
        List<DataBaseVO> databases = new ArrayList<>();

        // 根据数据源查找所有数据库
        try {
            Driver driver = (Driver) Class.forName(jdbcConnection.getDriver()).newInstance();
            Properties properties = new Properties();
            properties.put("user", dataSource.getUsername());
            properties.put("password", dataSource.getPassword());
            Connection connection = driver.connect(jdbcUrl, properties);
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSet = metaData.getCatalogs();
            while (resultSet.next()) {
                String databaseName = resultSet.getString("TABLE_CAT");
                DataBaseVO dataBaseVO = new DataBaseVO();
                dataBaseVO.setName(databaseName);

                ResultSet tables = metaData.getTables(databaseName, null, null, null);
                List<TableDefineVO> tableDefines = new ArrayList<>();
                List<ViewDefineVO> viewDefines = new ArrayList<>();
                List<ProcedureDefineVO> procedureDefines = new ArrayList<>();
                List<FunctionDefineVO> functionDefines = new ArrayList<>();
                List<EventDefineVO> eventDefines = new ArrayList<>();
                while (tables.next()) {
                    String tableName = tables.getString("TABLE_NAME");
                    String tableType = tables.getString("TABLE_TYPE");
                    if ("TABLE".equalsIgnoreCase(tableType)) {
                        TableDefineVO tableDefineVO = new TableDefineVO();
                        tableDefineVO.setName(tableName);
                        tableDefines.add(tableDefineVO);
                    } else if ("VIEW".equalsIgnoreCase(tableType)) {
                        ViewDefineVO viewDefineVO = new ViewDefineVO();
                        viewDefineVO.setName(tableName);
                        viewDefines.add(viewDefineVO);
                    } else {
                        String message = "[{0}@{1}] name {2} has table type {3} is not support";
                        log.warn(MessageFormat.format(message, databaseName, dataSource.getUsername(), tableName, tableType));
                    }
                }

                ResultSet procedures = metaData.getProcedures(databaseName, null, null);
                while (procedures.next()) {
                    String procedureName = procedures.getString("PROCEDURE_NAME");
                    ProcedureDefineVO procedureDefineVO = new ProcedureDefineVO();
                    procedureDefineVO.setName(procedureName);
                    procedureDefines.add(procedureDefineVO);
                }

                ResultSet functions = metaData.getFunctions(databaseName, null, null);
                while (functions.next()) {
                    String functionName = functions.getString("FUNCTION_NAME");
                    FunctionDefineVO functionDefineVO = new FunctionDefineVO();
                    functionDefineVO.setName(functionName);
                    functionDefines.add(functionDefineVO);
                }

                ResultSet events = metaData.getUDTs(databaseName, null, null, null);
                while (events.next()) {
                    String eventName = events.getString("EVENT_NAME");
                    EventDefineVO eventDefineVO = new EventDefineVO();
                    eventDefineVO.setName(eventName);
                    eventDefines.add(eventDefineVO);
                }

                // 触发器测试代码
                // CREATE TABLE tr_account (acct_num INT,amount DECIMAL (10,2));
                // CREATE TRIGGER ins_sum BEFORE INSERT ON tr_account
                // FOR EACH ROW SET @sum = @sum + NEW.amount;

                // 事件测试代码
                // CREATE EVENT my_event
                // ON SCHEDULE EVERY 1 DAY -- 每天执行一次
                // STARTS CURRENT_TIMESTAMP -- 从当前时间开始
                // DO
                // -- 这里写上您想要执行的SQL语句
                // DELETE FROM tr_account ;
                dataBaseVO.setTables(tableDefines);
                dataBaseVO.setViews(viewDefines);
                dataBaseVO.setProcedures(procedureDefines);
                dataBaseVO.setFunctions(functionDefines);
                dataBaseVO.setTriggers(Collections.emptyList());
                dataBaseVO.setEvents(eventDefines);
                databases.add(dataBaseVO);
            }
            connection.close();
        } catch (Exception err) {
            throw new IllegalArgumentException("连接数据库错误", err);
        }
        ReturnCollection<DataBaseVO> result = ReturnCollection.of(databases);
        result.getMeta().put("datasourceHost", dataSource.getHost());
        result.getMeta().put("datasourceUser", dataSource.getUsername());
        result.getMeta().put("datasourceDatabase", dataSource.getUsername());
        return result;
    }

    @Override
    public ReturnValue<String> executeSql(String dataSourceId, String sql) {
        log.debug("datasource {} execute sql {}", dataSourceId, sql);
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.openConnection(dataSourceId);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();
            List<TableColumnDefineVO> columnDefines = new ArrayList<>();
            for (int i = 0; i < columnCount; i++) {
                int colIndex = i + 1;
                String columnName = resultSetMetaData.getColumnName(colIndex);
                String columnTypeName = resultSetMetaData.getColumnTypeName(colIndex);
                TableColumnDefineVO columnDefineVO = new TableColumnDefineVO();
                columnDefineVO.setName(columnName);
                columnDefineVO.setType(columnTypeName);
                columnDefines.add(columnDefineVO);
            }

            List<Object> tableData = new ArrayList<>();
            while (resultSet.next()) {
                List<Object> rowData = new ArrayList<>();
                for (int i = 0; i < columnCount; i++) {
                    int colIndex = i + 1;
                    Object cellValue = resultSet.getObject(colIndex);
                    rowData.add(cellValue);
                }
                tableData.add(rowData);
            }

            TableDataVO tableDataVO = new TableDataVO();
            tableDataVO.getTableDefineVO().setColumns(columnDefines);
            tableDataVO.setData(tableData);
            ReturnValue<String> returnValue = ReturnValue.of("执行查询完成");
            returnValue.getMeta().put("tableDatas", Collections.singletonList(tableDataVO));
            return returnValue;
        } catch (Exception e) {
            throw new MaeException("执行失败", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                    log.warn("result set close fail", e);
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                    log.warn("statement close fail", e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                    log.warn("connection close fail", e);
                }
            }
        }
    }
}
