package com.mae.workflow.service;

import com.mae.core.ReturnMessage;
import com.mae.core.ReturnPagedCollection;
import com.mae.workflow.dto.process.SimpleProcessInstanceDTO;
import com.mae.workflow.dto.task.TaskCompleteDTO;
import com.mae.workflow.dto.task.TaskTransferDTO;
import com.mae.workflow.service.internal.IProcessInstanceInternalService;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
public class ProcessInstanceManagementService implements IProcessInstanceManagementService {
    @Inject
    private IProcessInstanceInternalService processInstanceInternalService;

    @Override
    public ReturnPagedCollection<SimpleProcessInstanceDTO> findProcessInstancePageList() {
        return null;
    }

    @Override
    public ReturnMessage suspend(String procInstId) {
        return null;
    }

    @Override
    public ReturnMessage resume(String procInstId) {
        return null;
    }

    @Override
    public ReturnMessage stop(String procInstId) {
        return null;
    }

    @Override
    public ReturnMessage complete(String procInstId, String taskId, TaskCompleteDTO taskCompleteDTO) {
        return processInstanceInternalService.complete(procInstId, taskId, taskCompleteDTO, false);
    }

    @Override
    public ReturnMessage transfer(String procInstId, String taskId, TaskTransferDTO taskTransferDTO) {
        return processInstanceInternalService.transfer(procInstId, taskId, taskTransferDTO, false);
    }
}
