package com.mae.workflow.service;

import com.mae.core.ReturnPagedCollection;
import com.mae.core.context.RequestContext;
import com.mae.workflow.dto.todo.MyApplicationDTO;
import com.mae.workflow.dto.todo.MyApplicationPageQueryDTO;
import com.mae.workflow.dto.todo.MyApplyPageQueryDTO;
import com.mae.workflow.dto.todo.MyTodoPageQueryDTO;
import com.mae.workflow.persistence.dao.MyApplicationDao;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
public class MyApplicationQueryService implements IMyApplicationQueryService {
    @Inject
    private MyApplicationDao myApplicationDao;

    @Override
    public ReturnPagedCollection<MyApplicationDTO> getMyApplyApplications(MyApplyPageQueryDTO myApplyPageQueryDTO) {
        myApplyPageQueryDTO.setUserId(RequestContext.getUserId());
        return ReturnPagedCollection.of(myApplicationDao.selectMyApplyPageList(myApplyPageQueryDTO));
    }

    @Override
    public ReturnPagedCollection<MyApplicationDTO> getMyTodoApplications(MyTodoPageQueryDTO myTodoPageQueryDTO) {
        myTodoPageQueryDTO.setUserId(RequestContext.getUserId());
        return ReturnPagedCollection.of(myApplicationDao.selectMyTodoPageList(myTodoPageQueryDTO));
    }

    @Override
    public ReturnPagedCollection<MyApplicationDTO> getMyApplications(MyApplicationPageQueryDTO myApplicationPageQueryDTO) {
        myApplicationPageQueryDTO.setUserId(RequestContext.getUserId());
        return ReturnPagedCollection.of(myApplicationDao.selectMyApplicationPageList(myApplicationPageQueryDTO));
    }
}
