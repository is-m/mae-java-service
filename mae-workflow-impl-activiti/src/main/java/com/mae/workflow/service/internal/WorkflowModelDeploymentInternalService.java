package com.mae.workflow.service.internal;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mae.core.utils.Assert;
import com.mae.workflow.persistence.dao.WorkflowDeploymentLogDao;
import com.mae.workflow.persistence.dao.WorkflowModelDao;
import com.mae.workflow.persistence.po.WorkflowDeploymentLogPO;
import com.mae.workflow.persistence.po.WorkflowModelPO;
import com.mae.workflow.util.ActivitiUtils;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

@Named
public class WorkflowModelDeploymentInternalService implements IWorkflowModelDeployInternalService {
    @Inject
    private WorkflowDeploymentLogDao workflowDeploymentLogDao;
    @Inject
    private RepositoryService repositoryService;
    @Inject
    private WorkflowModelDao workflowModelDao;

    public WorkflowDeploymentLogPO deploy(WorkflowModelPO modelPO) {
        String jsonModel = modelPO.getModel();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode modelNode = null;
        try {
            modelNode = objectMapper.readTree(jsonModel);
        } catch (IOException exception) {
            throw new IllegalArgumentException("the model json string convert to json has error", exception);
        }

        BpmnModel bpmnModel = new BpmnJsonConverter().convertToBpmnModel(modelNode);
        Deployment deploy = repositoryService.createDeployment()
                // 注意：这里如果没有放.bpmn 无法参数流程定义表信息
                .addBpmnModel("dynamic_" + modelPO.getTenantId() + "_" + modelPO.getKeycode() + ".bpmn", bpmnModel)
                .tenantId(modelPO.getTenantId())
                .deploy();

        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        ProcessDefinition processDefinition = processDefinitionQuery.deploymentId(deploy.getId()).singleResult();

        // 保存部署日志
        WorkflowDeploymentLogPO deploymentLogVO = new WorkflowDeploymentLogPO();
        deploymentLogVO.setWorkflowModelId(modelPO.getId());
        deploymentLogVO.setKeycode(modelPO.getKeycode());
        deploymentLogVO.setDeploymentId(deploy.getId());
        deploymentLogVO.setVersion(deploy.getVersion());
        deploymentLogVO.setDefinitionId(processDefinition.getId());
        deploymentLogVO.setTenantId(modelPO.getTenantId());
        deploymentLogVO.initForCreate();
        deploymentLogVO.setModel(jsonModel);
        workflowDeploymentLogDao.insertDeploymentLog(deploymentLogVO);

        return deploymentLogVO;
    }

    @Override
    public void uninstall(String deploymentId) {
        Assert.isFalse(ActivitiUtils.hasUndoProcessInstance(deploymentId), "存在运行中的流程");
        repositoryService.deleteDeployment(deploymentId, true);
    }

}
