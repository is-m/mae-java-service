package com.mae.workflow.service;

import com.alibaba.fastjson2.JSON;
import com.mae.core.ReturnMessage;
import com.mae.workflow.dto.draft.DraftPageQueryDTO;
import com.mae.workflow.dto.draft.ProcessDraftCreateDTO;
import com.mae.workflow.dto.draft.ProcessDraftUpdateDTO;
import com.mae.workflow.persistence.dao.ProcessDraftDao;
import com.mae.workflow.persistence.po.ProcessDraftPO;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
public class ProcessDraftService implements IProcessDraftService {
    @Inject
    private ProcessDraftDao processDraftDao;

    @Override
    public ReturnMessage createDraft(String procInstId, ProcessDraftCreateDTO processDraftCreateDTO) {
        ProcessDraftPO processDraftPO = new ProcessDraftPO();
        processDraftPO.initForCreate();
        processDraftPO.setKeycode(processDraftCreateDTO.getKeycode());
        processDraftPO.setMainform(JSON.toJSONString(processDraftCreateDTO.getMainform()));
        processDraftDao.insertProcessDraft(processDraftPO);
        return ReturnMessage.ok();
    }

    @Override
    public void updateDraft(String procInstId, ProcessDraftUpdateDTO processDraftUpdateDTO) {

    }

    @Override
    public void deleteDraft(String processDraftId) {
        processDraftDao.deleteById(processDraftId);
    }

    @Override
    public void findDraftPageList(DraftPageQueryDTO draftPageQueryDTO) {

    }

    @Override
    public void findDraft(String draftId) {

    }
}
