package com.mae.workflow.service.internal;

import com.alibaba.fastjson2.JSON;
import com.mae.core.utils.Assert;
import com.mae.workflow.dto.model.ProcessDefinitionDTO;
import com.mae.workflow.persistence.dao.WorkflowDeploymentLogDao;
import com.mae.workflow.persistence.po.WorkflowDeploymentLogPO;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class ProcessDefinitionQueryService implements IProcessDefinitionQueryService {
    @Inject
    private WorkflowDeploymentLogDao deploymentLogDao;
    @Inject
    private RepositoryService repositoryService;

    @Override
    // @Cacheable(key = "'processDefinition_' + #processDefinitionId")
    public ProcessDefinitionDTO findById(String processDefinitionId) {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult();
        Assert.notNull(processDefinition, "未找到流程定义");

        WorkflowDeploymentLogPO deploymentLogPO = deploymentLogDao.findOneByProcessDefinitionId(processDefinitionId);
        Assert.notNull(deploymentLogPO, "未找到模型部署记录");

        ProcessDefinitionDTO dto = new ProcessDefinitionDTO();
        dto.setId(processDefinition.getId());
        dto.setName(processDefinition.getName());
        dto.setKey(processDefinition.getKey());
        dto.setModel(JSON.parseObject(deploymentLogPO.getModel()));
        dto.setTenantId(processDefinition.getTenantId());

        return dto;
    }
}
