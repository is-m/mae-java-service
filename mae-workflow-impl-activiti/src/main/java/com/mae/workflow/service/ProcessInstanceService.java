package com.mae.workflow.service;

import com.mae.core.*;
import com.mae.core.context.RequestContext;
import com.mae.core.utils.Assert;
import com.mae.workflow.ProcessStartDTO;
import com.mae.workflow.constant.ActivitRequestContextConstant;
import com.mae.workflow.dto.process.ProcessCommentDTO;
import com.mae.workflow.dto.process.ProcessCommentDifferenceDTO;
import com.mae.workflow.dto.process.ProcessInstanceDTO;
import com.mae.workflow.persistence.dao.ProcessCommentDao;
import com.mae.workflow.persistence.dao.ProcessInstanceDao;
import com.mae.workflow.persistence.dao.WorkflowModelDao;
import com.mae.workflow.persistence.po.ProcessCommentPO;
import com.mae.workflow.persistence.po.WorkflowModelPO;
import com.mae.workflow.service.internal.IProcessInstanceInternalService;
import com.mae.workflow.util.ProcessFormUtils;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProcessInstanceService implements IProcessInstanceService {
    @Inject
    private ProcessInstanceDao processInstanceDao;
    @Inject
    private WorkflowModelDao workflowModelDao;
    @Inject
    private RuntimeService runtimeService;
    @Inject
    private TaskService taskService;
    @Inject
    private RepositoryService repositoryService;
    @Inject
    private IProcessDraftService processDraftService;
    @Inject
    private ProcessCommentDao processCommentDao;
    @Inject
    private IProcessInstanceInternalService processInstanceInternalService;
    @Inject
    private ApplicationEventPublisher eventPublisher;

    @Override
    public ReturnValue<ProcessInstanceDTO> startByDeployment(String deploymentId, ProcessStartDTO startDTO) {
        WorkflowModelPO modelPO = workflowModelDao.findSimpleOneByTenantIdAndDeploymentId(RequestContext.getTenantId(), deploymentId);
        Assert.notNull(modelPO, "启动流程失败，无法找到流程配置信息");
        ProcessInstanceDTO processInstanceDTO = startProcess(startDTO, modelPO);
        return ReturnValue.of(processInstanceDTO);
    }


    @Override
    public ReturnValue<ProcessInstanceDTO> startByProcessKey(String keycode, ProcessStartDTO startDTO) {
        WorkflowModelPO workflowModelPO = workflowModelDao.findSimpleOneByTenantIdAndKeycode(RequestContext.getTenantId(), keycode);
        Assert.notNull(workflowModelPO, "启动流程失败，无法找到流程配置信息");
        ProcessInstanceDTO processInstanceDTO = startProcess(startDTO, workflowModelPO);
        return ReturnValue.of(processInstanceDTO);
    }

    @Override
    public ReturnMessage suspend(String procInstId) {
        return processInstanceInternalService.suspend(procInstId, true);
    }

    @Override
    public ReturnMessage resume(String procInstId) {
        return processInstanceInternalService.resume(procInstId, true);
    }

    @Override
    public ReturnMessage stop(String procInstId) {
        return processInstanceInternalService.stop(procInstId, true);
    }

    @Override
    public ReturnPagedCollection<ProcessCommentDTO> getPagedComments(
            String procInstId, PageQuery pageQuery) {
        PagedResult<ProcessCommentPO> pagedResult = processCommentDao.findPageListByProcessInstanceId(procInstId, pageQuery);
        pagedResult.getContent().forEach(item -> {

        });
        return null;
    }

    @Override
    public ReturnPagedCollection<ProcessCommentDifferenceDTO> getPagedCommentDifferences(
            String commentId, PageQuery pageQuery) {
        return null;
    }

    private ProcessInstanceDTO startProcess(ProcessStartDTO startDTO, WorkflowModelPO modelPO) {
        ProcessInstanceDTO processInstanceDTO = new ProcessInstanceDTO();
        processInstanceDTO.setId(startDTO.getProcessInstanceId());
        processInstanceDTO.setVariables(startDTO.getMainform());
        RequestContext.put(ActivitRequestContextConstant.PROCESS_INSTANCE, processInstanceDTO);
        String title = ProcessFormUtils.getTitle(startDTO.getMainform());
        String name = StringUtils.hasText(title) ? title : modelPO.getName();

        // 设置流程当前上下文用户
        Authentication.setAuthenticatedUserId(RequestContext.getUserId());
        ProcessInstance instance = runtimeService.createProcessInstanceBuilder()
                .tenantId(RequestContext.getTenantId())
                .name(name)
                .businessKey(startDTO.getBusinessKey())
                .processDefinitionId(modelPO.getLastDefinitionId())
                .variables(startDTO.getMainform())
                .start();

        processInstanceDTO.setId(instance.getId());
        processInstanceDTO.setName(instance.getName());
        processInstanceDTO.setKeycode(instance.getProcessDefinitionKey());
        processInstanceDTO.setStatus(1);
        processInstanceDTO.setStartUserId(instance.getStartUserId());
        processInstanceDTO.setStartTime(instance.getStartTime());
        processInstanceDTO.setVariables(startDTO.getMainform());
        // 启动就结束的流程，更新流程状态为已完成
        if (instance.isEnded()) {
            processInstanceDTO.setStatus(4);
            taskService.addComment(null, instance.getId(), "START", "流程结束");
        } else {
            updateProcessInstanceRuTaskInfo(instance);
            taskService.addComment(null, instance.getId(), "START", "启动");
        }

        if (StringUtils.hasText(startDTO.getDraftId())) {
            processDraftService.deleteDraft(startDTO.getDraftId());
        }
        return processInstanceDTO;
    }

    private void updateProcessInstanceRuTaskInfo(ProcessInstance instance) {
        List<Task> runningTasks = taskService.createTaskQuery().processInstanceId(instance.getId()).active().list();
        List<String> taskIdList = new ArrayList<>();
        List<String> taskNameList = new ArrayList<>();
        List<String> taskAssigneeList = new ArrayList<>();

        runningTasks.forEach(task -> {
            taskIdList.add(task.getId());
            taskNameList.add(task.getName());
            taskAssigneeList.add(task.getAssignee());
        });

        // 更新下一环节处理人信息到流程主表
        String taskId = String.join(";", taskIdList);
        String taskName = String.join(";", taskNameList);
        String taskAssignee = String.join(";", taskAssigneeList);
        processInstanceDao.updateRuTaskInfo(instance.getId(), taskId, taskName, taskAssignee, 1);
    }
}
