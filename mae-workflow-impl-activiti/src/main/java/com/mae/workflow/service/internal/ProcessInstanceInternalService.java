package com.mae.workflow.service.internal;

import com.alibaba.fastjson2.JSON;
import com.mae.core.utils.Assert;
import com.mae.core.ReturnMessage;
import com.mae.core.context.RequestContext;
import com.mae.core.exception.MaeException;
import com.mae.workflow.dto.task.TaskCompleteDTO;
import com.mae.workflow.dto.task.TaskSaveDTO;
import com.mae.workflow.dto.task.TaskTransferDTO;
import com.mae.workflow.persistence.dao.CommentDifferenceLogDao;
import com.mae.workflow.persistence.dao.ProcessCommentDao;
import com.mae.workflow.persistence.dao.ProcessInstanceDao;
import com.mae.workflow.persistence.po.CommentDifferenceLogPO;
import com.mae.workflow.util.ProcessFormDifference;
import com.mae.workflow.util.ProcessFormUtils;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.*;

@Service
public class ProcessInstanceInternalService implements IProcessInstanceInternalService {
    @Inject
    private TaskService taskService;
    @Inject
    private RuntimeService runtimeService;
    @Inject
    private CommentDifferenceLogDao commentDifferenceLogDao;
    @Inject
    private ProcessInstanceDao processInstanceDao;
    @Inject
    private ProcessCommentDao processCommentDao;

    @Override
    public ReturnMessage suspend(String procInstId, boolean checkPermission) {
        int modifyCount = processInstanceDao.updateProcessInstanceStatusForExpect(procInstId, 2, 1);
        Assert.isEquals(1, modifyCount, "不能挂起不是处理中状态的流程");
        runtimeService.suspendProcessInstanceById(procInstId);
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage resume(String procInstId, boolean checkPermission) {
        int modifyCount = processInstanceDao.updateProcessInstanceStatusForExpect(procInstId, 1, 2);
        Assert.isEquals(1, modifyCount, "不能激活不是挂起状态的流程");
        runtimeService.activateProcessInstanceById(procInstId);
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage stop(String procInstId, boolean checkPermission) {
        return null;
    }

    @Override
    public ReturnMessage save(String procInstId, String taskId, TaskSaveDTO taskSaveDTO, boolean checkPermission) {
        Task task = taskService.createTaskQuery().processInstanceId(procInstId).taskId(taskId).singleResult();
        Assert.notNull(task, "任务不存在或者已经被处理");

        if (checkPermission) {
            Assert.isContains(task.getAssignee(), RequestContext.getUserId(), "没有操作权限");
        }

        ProcessFormDifference processFormDifference = new ProcessFormDifference();

        Authentication.setAuthenticatedUserId(RequestContext.getUserId());
        saveProcessForm(task, taskSaveDTO.getMainform(), taskSaveDTO.getTaskform());
        // TODO 检查当前任务是否有保存操作，如果有则直接按就的日志进行累加差异

        Comment comment = taskService.addComment(taskId, procInstId, "SAVE", ProcessFormUtils.getComment(taskSaveDTO.getTaskform()));
        processCommentDao.updateTaskNameById(comment.getId(), task.getName());

        // 检查模型配置是否要记录表单差异
        addCommentDifference(comment.getId(), processFormDifference);
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage complete(String procInstId, String taskId, TaskCompleteDTO taskCompleteDTO, boolean checkPermission) {
        Task task = taskService.createTaskQuery().processInstanceId(procInstId).taskId(taskId).singleResult();
        Assert.notNull(task, "任务不存在或者已经被处理");
        if (checkPermission) {
            Assert.isContains(task.getAssignee(), RequestContext.getUserId(), "没有操作权限");
        }

        ProcessFormDifference processFormDifference = new ProcessFormDifference();

        Authentication.setAuthenticatedUserId(RequestContext.getUserId());
        saveProcessForm(task, taskCompleteDTO.getMainform(), taskCompleteDTO.getTaskform());

        // 先添加审批日志，审批后添加会提示任务不存在
        Comment comment = taskService.addComment(taskId, procInstId, "COMPLETE", ProcessFormUtils.getComment(taskCompleteDTO.getTaskform()));
        processCommentDao.updateTaskNameById(comment.getId(), task.getName());
        taskService.claim(taskId, RequestContext.getUserId());
        try {
            taskService.complete(taskId);
        } catch (ActivitiException exception) {
            String message = exception.getMessage();
            if (org.apache.commons.lang3.StringUtils.contains(message, "No outgoing sequence flow")) {
                throw new MaeException("提交失败，没有匹配到有效的下一个处理节点，请检查模型，以及路由条件是否有效配置");
            }
        }
        // 检查模型配置是否要记录表单差异
        addCommentDifference(comment.getId(), processFormDifference);

        updateProcessInstanceRuTaskInfo(procInstId);
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage transfer(String procInstId, String taskId, TaskTransferDTO taskTransferDTO, boolean checkPermission) {
        Task task = taskService.createTaskQuery().processInstanceId(procInstId).taskId(taskId).singleResult();
        Assert.notNull(task, "任务不存在或者已经被处理");
        if (checkPermission) {
            Assert.isContains(task.getAssignee(), RequestContext.getUserId(), "没有操作权限");
        }

        Authentication.setAuthenticatedUserId(RequestContext.getUserId());

        // TODO 如果当前处理人是主表单变量绑定设置的，则回写主表单字段
        String oldAssignee = Optional.ofNullable(task.getAssignee()).orElse("");
        String newAssignee = null;
        // 旧的处理人为空，或者待转派的人为空时，直接设置为新的处理人
        if (!StringUtils.hasText(oldAssignee) || !StringUtils.hasText(taskTransferDTO.getFrom())) {
            newAssignee = taskTransferDTO.getTo();
        } else if (oldAssignee.contains(taskTransferDTO.getFrom())) {
            // 旧的处理人列表包含当前待转派的用户时，将旧人移除，增加新的处理人
            Set<String> assigneeSet = new LinkedHashSet<>(Arrays.asList(oldAssignee.split(";")));
            assigneeSet.remove(taskTransferDTO.getFrom());
            assigneeSet.addAll(Arrays.asList(taskTransferDTO.getTo().split(";")));
            newAssignee = String.join(";", assigneeSet);
        } else {
            throw new MaeException("转派失败，待转派人不在当前任务处理人的列表");
        }
        taskService.setAssignee(taskId, newAssignee);
        Comment comment = taskService.addComment(taskId, procInstId, "TRANSFER", Optional.ofNullable(taskTransferDTO.getComment()).orElse(""));
        processCommentDao.updateTaskNameById(comment.getId(), task.getName());

        // 修改前的处理人，修改后的处理人
        updateProcessInstanceRuTaskInfo(procInstId);
        return ReturnMessage.ok();
    }

    private void saveProcessForm(Task task, Map<String, Object> mainform, Map<String, Object> taskform) {
        // 保存主表单
        if (!CollectionUtils.isEmpty(mainform)) {
            runtimeService.setVariables(task.getProcessInstanceId(), mainform);

            // 如果内置属性 title 有值，则触发流程名称修改
            String title = ProcessFormUtils.getTitle(mainform);
            if (StringUtils.hasText(title)) {
                runtimeService.setProcessInstanceName(task.getProcessInstanceId(), title);
            }
        }

        // 保存任务表单
        if (!CollectionUtils.isEmpty(taskform)) {
            taskService.setVariablesLocal(task.getId(), taskform);
        }
    }

    private void addCommentDifference(String commentId, ProcessFormDifference difference) {
        Map<String, Object> differences = difference.getDifferences();
        if (!CollectionUtils.isEmpty(differences)) {
            CommentDifferenceLogPO commentDifferenceLogPO = new CommentDifferenceLogPO();
            commentDifferenceLogPO.initForCreate();
            commentDifferenceLogPO.setAction(difference.getAction());
            commentDifferenceLogPO.setDifference(JSON.toJSONString(differences));
            commentDifferenceLogPO.setActCommentId(commentId);
            commentDifferenceLogDao.insertCommentDifferenceLog(commentDifferenceLogPO);
        }
    }

    private void updateProcessInstanceRuTaskInfo(String procInstId) {
        List<Task> runningTasks = taskService.createTaskQuery().processInstanceId(procInstId).active().list();
        List<String> taskIdList = new ArrayList<>();
        List<String> taskNameList = new ArrayList<>();
        List<String> taskAssigneeList = new ArrayList<>();

        runningTasks.forEach(task -> {
            taskIdList.add(task.getId());
            taskNameList.add(task.getName());
            taskAssigneeList.add(task.getAssignee());
        });

        // 更新下一环节处理人信息到流程主表
        String taskId = String.join(";", taskIdList);
        String taskName = String.join(";", taskNameList);
        String taskAssignee = String.join(";", taskAssigneeList);
        processInstanceDao.updateRuTaskInfo(procInstId, taskId, taskName, taskAssignee, 1);
    }
}
