package com.mae.workflow.service;

import com.mae.core.ReturnMessage;
import com.mae.core.context.RequestContext;
import com.mae.workflow.dto.task.TaskCompleteDTO;
import com.mae.workflow.dto.task.TaskSaveDTO;
import com.mae.workflow.dto.task.TaskTransferDTO;
import com.mae.workflow.service.internal.IProcessInstanceInternalService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class ProcessInstanceTaskService implements IProcessInstanceTaskService {
    @Inject
    private IProcessInstanceInternalService processInstanceInternalService;

    @Override
    public ReturnMessage save(String procInstId, String taskId, TaskSaveDTO taskSaveDTO) {
        return processInstanceInternalService.save(procInstId, taskId, taskSaveDTO, true);
    }

    @Override
    public ReturnMessage complete(String procInstId, String taskId, TaskCompleteDTO completeDTO) {
        return processInstanceInternalService.complete(procInstId, taskId, completeDTO, true);
    }

    @Override
    public ReturnMessage transfer(String procInstId, String taskId, TaskTransferDTO transferDTO) {
        transferDTO.setFrom(RequestContext.getUserId());
        return processInstanceInternalService.transfer(procInstId, taskId, transferDTO, true);
    }
}
