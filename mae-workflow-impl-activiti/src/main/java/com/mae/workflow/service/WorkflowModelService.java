package com.mae.workflow.service;

import com.alibaba.fastjson2.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mae.core.utils.Assert;
import com.mae.core.ReturnMessage;
import com.mae.core.context.RequestContext;
import com.mae.workflow.dto.model.WorkflowModelXmlCreateDTO;
import com.mae.workflow.dto.model.WorkflowModelXmlUpdateDTO;
import com.mae.workflow.persistence.dao.WorkflowModelDao;
import com.mae.workflow.persistence.po.WorkflowDeploymentLogPO;
import com.mae.workflow.persistence.po.WorkflowModelPO;
import com.mae.workflow.service.internal.IWorkflowModelDeployInternalService;
import com.mae.workflow.util.ActivitiUtils;
import com.mae.workflow.util.BpmnModelUtils;
import com.mae.workflow.util.MaeBpmnJsonConverter;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.model.BpmnModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.Map;

@Slf4j
@Named
@RestController
public class WorkflowModelService implements IWorkflowModelService {
    @Inject
    private WorkflowModelDao workflowModelDao;
    @Inject
    private IWorkflowModelDeployInternalService workflowModelDeployService;

    @Override
    public ReturnMessage createModelForXml(String keycode, WorkflowModelXmlCreateDTO xmlCreateDTO) {
        // 检查租户下模型是否已经存在
        int count = workflowModelDao.countByTenantIdAndKeycode(RequestContext.getCurrent().getUser().getTenantId(), keycode);
        Assert.isEquals(0, count, "当前租户下已经存在相同的流程标识符 " + keycode);
        WorkflowModelPO workflowModelPO = new WorkflowModelPO();
        BeanUtils.copyProperties(xmlCreateDTO, workflowModelPO, "model");
        if (!"draft".equals(workflowModelPO.getStatus())) {
            workflowModelPO.setStatus("save");
        }
        if (StringUtils.isEmpty(workflowModelPO.getCategory())) {
            workflowModelPO.setCategory("general");
        }
        String modelJson = readModelJsonForXml(keycode, xmlCreateDTO.getModel());
        workflowModelPO.setModel(modelJson);
        workflowModelPO.setKeycode(keycode);
        workflowModelPO.initForCreate();
        workflowModelDao.createWorkflowModel(workflowModelPO);
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage updateModelForXml(String keycode, WorkflowModelXmlUpdateDTO xmlUpdateDTO) {
        WorkflowModelPO modelPO = workflowModelDao.findSimpleOneByTenantIdAndKeycode(RequestContext.getTenantId(), keycode);
        BeanUtils.copyProperties(xmlUpdateDTO, modelPO, "model");
        modelPO.setModel(readModelJsonForXml(keycode, xmlUpdateDTO.getModel()));
        // 重置状态为保存
        if (!"draft".equals(modelPO.getStatus())) {
            modelPO.setStatus("save");
        }
        workflowModelDao.updateWorkflowModel(modelPO);
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage installModel(String keycode) {
        WorkflowModelPO modelPO = workflowModelDao.findOneByTenantIdAndKeycode(RequestContext.getTenantId(), keycode);
        Assert.notNull(modelPO, "模型不存在");
        String status = modelPO.getStatus();
        Assert.isTrue(!"draft".equals(status), "不能部署草稿状态的记录");
        Assert.isTrue(!"valid".equals(status), "当前模型未发生过修改");
        WorkflowDeploymentLogPO deploymentPO = workflowModelDeployService.deploy(modelPO);
        WorkflowModelPO workflowModelVO = new WorkflowModelPO();
        workflowModelVO.setId(modelPO.getId());
        workflowModelVO.initForUpdate();
        workflowModelVO.setStatus("valid");
        workflowModelVO.setLastDeployTime(new Date());
        workflowModelVO.setLastDefinitionId(deploymentPO.getDefinitionId());
        workflowModelVO.setLastDeployId(deploymentPO.getDeploymentId());
        workflowModelVO.setVersion(deploymentPO.getVersion());
        workflowModelDao.updateWorkflowModelDeploymentInfo(workflowModelVO);
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage uninstallModel(String keycode) {
        WorkflowModelPO workflowModelVO = workflowModelDao.findOneByTenantIdAndKeycode(RequestContext.getTenantId(), keycode);
        Assert.notNull(workflowModelVO, "模型不存在");
        String lastDeployId = workflowModelVO.getLastDeployId();
        workflowModelDeployService.uninstall(lastDeployId);
        workflowModelVO.setStatus("invalid");
        workflowModelVO.setLastDefinitionId(null);
        workflowModelVO.setLastDeployId(null);
        workflowModelVO.setVersion(null);
        workflowModelDao.updateWorkflowModelDeploymentInfo(workflowModelVO);
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage deleteModel(String keycode) {
        // 检查是否存在运行中的流程，存在时不允许删除
        WorkflowModelPO workflowModelVO = workflowModelDao.findSimpleOneByTenantIdAndKeycode(RequestContext.getTenantId(), keycode);
        Assert.notNull(workflowModelVO, "模型不存在");
        boolean hasUndo = ActivitiUtils.hasUndoProcessInstance(workflowModelVO.getLastDeployId());
        Assert.isFalse(hasUndo, "存在运行中的流程，不能删除模型");
        workflowModelDao.deleteWorkflowModel(workflowModelVO.getId());
        return ReturnMessage.ok();
    }

    @Override
    public ReturnMessage uploadBpmn20Model() {
        return null;
    }

    @Override
    public ReturnMessage downloadBpmn20Model() {
        return null;
    }

    private String readModelJsonForXml(String keycode, String model) {
        BpmnModel bpmnModel = BpmnModelUtils.xmlModelToBpmnModel(model);
        ObjectNode modelJSON = new MaeBpmnJsonConverter().convertToJson(bpmnModel);
        ObjectMapper objectMapper = new ObjectMapper();
        Map modelMap = objectMapper.convertValue(modelJSON, Map.class);
        ((Map) modelMap.get("properties")).put("process_id", keycode);
        return JSON.toJSONString(modelMap);
    }
}
