package com.mae.workflow.service.internal;

import com.mae.core.utils.Mae;
import com.mae.workflow.dto.model.ProcessDefinitionDTO;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class EasyIdGeneratorService implements IdGeneratorService {
    @Override
    public String next(String procDefinitionId) {
        IProcessDefinitionQueryService processDefinitionQueryService = Mae.context().getBean(IProcessDefinitionQueryService.class);
        ProcessDefinitionDTO processDefinitionDTO = processDefinitionQueryService.findById(procDefinitionId);
        String suffix = Long.toString(System.currentTimeMillis(), 36) + RandomUtils.nextInt(1000, 9999);
        String rule = processDefinitionDTO.getProperties().getString("idRule");
        return (StringUtils.hasText(rule) ? rule : procDefinitionId.split(":")[0]) + suffix;
    }
}
