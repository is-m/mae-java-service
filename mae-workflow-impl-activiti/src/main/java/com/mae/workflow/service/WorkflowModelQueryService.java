package com.mae.workflow.service;

import com.mae.core.*;
import com.mae.core.annotations.SecuritySchema;
import com.mae.core.context.RequestContext;
import com.mae.core.utils.Assert;
import com.mae.workflow.WorkflowModelPageQuery;
import com.mae.workflow.WorkflowModelVO;
import com.mae.workflow.dto.model.WorkflowModelXmlDTO;
import com.mae.workflow.persistence.dao.WorkflowModelDao;
import com.mae.workflow.persistence.po.WorkflowModelPO;
import com.mae.workflow.util.BpmnModelUtils;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
public class WorkflowModelQueryService implements IWorkflowModelQueryService {
    @Inject
    private WorkflowModelDao workflowModelDao;

    @Override
    @SecuritySchema("根据ID获取详情")
    public ReturnValue<WorkflowModelXmlDTO> findXmlDetail(String keycode) {
        WorkflowModelPO modelPO = workflowModelDao.findOneByTenantIdAndKeycode(RequestContext.getTenantId(), keycode);
        Assert.notNull(modelPO, "模型不存在");
        WorkflowModelXmlDTO dto = new WorkflowModelXmlDTO();
        BeanUtils.copyProperties(modelPO, dto, "model");
        dto.setModel(BpmnModelUtils.jsonModelToXml(modelPO.getModel()));
        return ReturnValue.of(dto);
    }

    @Override
    @Validated
    @SecuritySchema("分页获取列表")
    public ReturnPagedCollection<WorkflowModelVO> findSimplePagedList(@Valid WorkflowModelPageQuery query) {
        PagedResult<WorkflowModelVO> result = workflowModelDao.findPagedList(query);
        return ReturnPagedCollection.of(result);
    }
}
