package com.mae.workflow.activiti.dynamic;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.mae.core.utils.Mae;
import com.mae.workflow.dto.model.ProcessDefinitionDTO;
import com.mae.workflow.service.internal.IProcessDefinitionQueryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Objects;

/**
 * 序列流处理器（路径条件）
 */
@Named
public class SequenceFlowHandler {
    @Inject
    private RuntimeService runtimeService;

    /**
     * 解析路由条件
     *
     * @param execution
     * @return
     */
    public boolean evalCondition(DelegateExecution execution, String sequenceFlowActivityId) {
        IProcessDefinitionQueryService processDefinitionQueryService = Mae.context().getBean(IProcessDefinitionQueryService.class);
        // 获取节点配置
        ProcessDefinitionDTO processDefinition = processDefinitionQueryService.findById(execution.getProcessDefinitionId());
        JSONObject extensionObject = processDefinition.getExtensionObject(sequenceFlowActivityId);
        if (CollectionUtils.isEmpty(extensionObject)) {
            return true;
        }
        JSONArray conditionArray = extensionObject.getJSONArray("condition");
        if (conditionArray != null) {
            for (int i = 0; i < conditionArray.size(); i++) {
                JSONObject conditionObject = conditionArray.getJSONObject(i);
                // resource 只支持主表单和当前审批任务节点的参数
                String resourceId = conditionObject.getString("resource");
                String field = conditionObject.getString("field");
                String compare = conditionObject.getString("compare");
                Object value = conditionObject.get("value");

                // 比较
                Object fieldValue = getFieldValue(execution, resourceId, field);
                if (!compare(compare, fieldValue, value)) {
                    return false;
                }
            }
        }
        return true;
    }

    private Object getFieldValue(DelegateExecution execution, String resourceId, String field) {
        if ("mainform".equals(resourceId)) {
            return runtimeService.getVariable(execution.getProcessInstanceId(), field);
        }
        return execution.getVariable(field);
    }

    private boolean compare(String compare, Object fieldValue, Object compareValue) {
        if ("empty".equals(compare)) {
            return StringUtils.isEmpty(fieldValue);
        }
        if ("notEmpty".equals(compare)) {
            return !StringUtils.isEmpty(fieldValue);
        }
        // TODO 待整体实现比较逻辑
        if (fieldValue == null || compareValue == null) {
            return false;
        }
        String s = fieldValue.getClass().toString();
        return Objects.equals(fieldValue, compareValue);
    }
}
