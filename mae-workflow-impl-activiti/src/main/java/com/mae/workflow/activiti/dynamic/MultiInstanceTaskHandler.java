package com.mae.workflow.activiti.dynamic;

import org.activiti.engine.delegate.DelegateExecution;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 多实例任务处理器（会签）
 */
@Named
public class MultiInstanceTaskHandler {
    /**
     * 获取会签用户列表
     *
     * @param execution
     * @return
     */
    public List<String> getTaskAssignee(DelegateExecution execution) {
        return Collections.emptyList();
    }

    /**
     * 是否会签中断，在当前节点配置中断条件，例如组合变量条件满足时进行中断，中断后会直接按序列流的条件执行
     *
     * @param execution
     * @return
     */
    public boolean intercept(DelegateExecution execution) {
        return false;
    }

    /**
     * 会签节点时，多路径符合条件则进行路径选择
     *
     * @param execution
     * @return
     */
    public boolean complete(DelegateExecution execution) {
        return false;
    }
}
