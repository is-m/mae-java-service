package com.mae.workflow.activiti;

import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandContextCloseListener;

import java.util.Objects;

/**
 * 工作流内部访问时无法实时获取数据库数据，使用Context.监听关闭的方法来获取数据
 *
 */
public class ClosedCommandContextCloseListener implements CommandContextCloseListener {
    private Runnable runnable;

    public ClosedCommandContextCloseListener(Runnable runnable) {
        Objects.requireNonNull(runnable, "runnable is not be null");
        this.runnable = runnable;
    }

    @Override
    public void closing(CommandContext commandContext) {

    }

    @Override
    public void afterSessionsFlush(CommandContext commandContext) {

    }

    @Override
    public void closed(CommandContext commandContext) {
        runnable.run();
    }

    @Override
    public void closeFailure(CommandContext commandContext) {

    }
}
