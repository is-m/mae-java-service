package com.mae.workflow.activiti;

import com.alibaba.fastjson2.JSON;
import com.mae.core.utils.Mae;
import com.mae.workflow.persistence.dao.ProcessFormDao;
import com.mae.workflow.persistence.dao.ProcessInstanceDao;
import com.mae.workflow.persistence.dao.ProcessVariableDao;
import com.mae.workflow.persistence.po.ProcessFormPO;
import org.activiti.engine.HistoryService;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.impl.context.Context;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 全局 Activiti 事件监听处理
 */
public class GlobalActivitiEventListener implements ActivitiEventListener {
    @Inject
    private ProcessInstanceDao processInstanceDao;
    @Inject
    private ProcessVariableDao processVariableDao;
    @Inject
    private ProcessFormDao processFormDao;

    @Override
    public void onEvent(ActivitiEvent activitiEvent) {
        String procInstId = activitiEvent.getProcessInstanceId();
        switch (activitiEvent.getType()) {
            case PROCESS_CANCELLED:
                updateStatusAndProcessFormBackup(procInstId, 3);
                break;
            case PROCESS_COMPLETED:
                updateStatusAndProcessFormBackup(procInstId, 4);
                break;
        }
    }

    private void updateStatusAndProcessFormBackup(String procInstId, int status) {
        processInstanceDao.updateProcessInstanceStatus(procInstId, status);
        Context.getCommandContext().addCloseListener(new ClosedCommandContextCloseListener(() -> {
            // 流程结束时对表单数据进行归档后删除表单数据
            List<HistoricVariableInstance> vars = Mae.context().getBean(HistoryService.class).createHistoricVariableInstanceQuery().processInstanceId(procInstId).list();
            Map<String, Map<String, Object>> formMap = new HashMap<>();
            formMap.put("mainform", new HashMap<>());
            vars.forEach(item -> {
                String formName = StringUtils.hasText(item.getTaskId()) ? item.getTaskId() : "mainform";
                if (!formMap.containsKey(formName)) {
                    formMap.put(formName, new HashMap<>());
                }
                formMap.get(formName).put(item.getVariableName(), item.getValue());
            });

            // 归档流程表单数据
            ProcessFormPO processFormPO = new ProcessFormPO();
            processFormPO.initForCreate();
            processFormPO.setProcInstId(procInstId);
            Map<String, Object> mainform = formMap.remove("mainform");
            processFormPO.setMainform(JSON.toJSONString(mainform));
            processFormPO.setTaskform(JSON.toJSONString(formMap));
            processFormDao.insertProcessForm(processFormPO);

            // 清空已经归档的数据
            processVariableDao.deleteByProcessInstanceId(procInstId);
        }));
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
