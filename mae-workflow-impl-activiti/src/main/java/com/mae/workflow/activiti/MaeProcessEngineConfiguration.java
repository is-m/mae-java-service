package com.mae.workflow.activiti;

import com.mae.workflow.service.internal.IdGeneratorService;
import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.persistence.CountingExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;

public class MaeProcessEngineConfiguration extends SpringProcessEngineConfiguration {
    @Autowired
    private IdGeneratorService idGeneratorService;

    @Override
    public void init() {
        super.init();
        // 重写 ExecutionEntityManagerImpl 支持流程id实现自定义
        this.executionEntityManager = new MaeExecutionEntityManagerImpl(
                this, this.executionDataManager, this.idGeneratorService);
    }
}
