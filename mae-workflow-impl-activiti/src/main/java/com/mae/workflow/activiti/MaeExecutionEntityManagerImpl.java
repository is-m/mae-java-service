package com.mae.workflow.activiti;

import com.mae.core.context.RequestContext;
import com.mae.workflow.constant.ActivitRequestContextConstant;
import com.mae.workflow.dto.process.ProcessInstanceDTO;
import com.mae.workflow.service.internal.IdGeneratorService;
import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.persistence.CountingExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ExecutionEntityManagerImpl;
import org.activiti.engine.impl.persistence.entity.data.ExecutionDataManager;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.util.StringUtils;

public class MaeExecutionEntityManagerImpl extends ExecutionEntityManagerImpl {
    private IdGeneratorService idGeneratorService;

    public MaeExecutionEntityManagerImpl(
            ProcessEngineConfigurationImpl processEngineConfiguration,
            ExecutionDataManager executionDataManager,
            IdGeneratorService idGeneratorService) {
        super(processEngineConfiguration, executionDataManager);
        this.idGeneratorService = idGeneratorService;
    }

    // 重新实现流程实例ID自定义
    @Override
    public ExecutionEntity createProcessInstanceExecution(ProcessDefinition processDefinition, String businessKey, String tenantId, String initiatorVariableName) {
        ExecutionEntity processInstanceExecution = (ExecutionEntity) this.executionDataManager.create();

        if (idGeneratorService != null) {
            // 获取请求上下文流程实例
            ProcessInstanceDTO processInstanceDTO = (ProcessInstanceDTO) RequestContext.get(ActivitRequestContextConstant.PROCESS_INSTANCE);
            // 重置ID
            String id = null;
            if (processInstanceDTO != null && StringUtils.hasText(processInstanceDTO.getId())) {
                id = processInstanceDTO.getId();
            } else {
                id = idGeneratorService.next(processDefinition.getId());
            }
            processInstanceExecution.setId(id);
        }

        if (this.isExecutionRelatedEntityCountEnabledGlobally()) {
            ((CountingExecutionEntity) processInstanceExecution).setCountEnabled(true);
        }

        processInstanceExecution.setProcessDefinitionId(processDefinition.getId());
        processInstanceExecution.setProcessDefinitionKey(processDefinition.getKey());
        processInstanceExecution.setProcessDefinitionName(processDefinition.getName());
        processInstanceExecution.setProcessDefinitionVersion(processDefinition.getVersion());
        processInstanceExecution.setAppVersion(processDefinition.getAppVersion());
        processInstanceExecution.setBusinessKey(businessKey);
        processInstanceExecution.setScope(true);
        if (tenantId != null) {
            processInstanceExecution.setTenantId(tenantId);
        }

        String authenticatedUserId = Authentication.getAuthenticatedUserId();
        processInstanceExecution.setStartTime(Context.getProcessEngineConfiguration().getClock().getCurrentTime());
        processInstanceExecution.setStartUserId(authenticatedUserId);
        this.insert(processInstanceExecution, false);
        if (initiatorVariableName != null) {
            processInstanceExecution.setVariable(initiatorVariableName, authenticatedUserId);
        }



        processInstanceExecution.setProcessInstanceId(processInstanceExecution.getId());
        processInstanceExecution.setRootProcessInstanceId(processInstanceExecution.getId());
        if (authenticatedUserId != null) {
            this.getIdentityLinkEntityManager().addIdentityLink(processInstanceExecution, authenticatedUserId, (String) null, "starter");
        }

        if (this.getEventDispatcher().isEnabled()) {
            this.getEventDispatcher().dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_CREATED, processInstanceExecution));
        }

        return processInstanceExecution;
    }
}
