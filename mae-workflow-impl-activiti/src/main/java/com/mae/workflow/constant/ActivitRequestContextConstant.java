package com.mae.workflow.constant;

public interface ActivitRequestContextConstant {
    /**
     * 流程实例
     */
    String PROCESS_INSTANCE = "$PROCESS_INSTANCE";

    /**
     * 当前任务实例
     */
    String TASK_INSTANCE = "$PROCESS_TASK_INSTANCE";
}
