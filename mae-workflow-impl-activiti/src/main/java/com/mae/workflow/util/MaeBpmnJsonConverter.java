package com.mae.workflow.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mae.core.exception.MaeException;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;
import org.activiti.editor.language.json.converter.*;
import org.activiti.editor.language.json.model.ModelInfo;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Slf4j
public class MaeBpmnJsonConverter extends BpmnJsonConverter {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void processFlowElement(FlowElement flowElement, FlowElementsContainer container, BpmnModel model, ArrayNode shapesArrayNode, Map<String, ModelInfo> formKeyMap, Map<String, ModelInfo> decisionTableKeyMap, double containerX, double containerY) {
        Class<? extends BaseBpmnJsonConverter> converter = convertersToJsonMap.get(flowElement.getClass());
        if (converter != null) {
            try {
                BaseBpmnJsonConverter converterInstance = converter.newInstance();
                if (converterInstance instanceof FormKeyAwareConverter) {
                    ((FormKeyAwareConverter) converterInstance).setFormKeyMap(formKeyMap);
                }

                if (converterInstance instanceof DecisionTableKeyAwareConverter) {
                    ((DecisionTableKeyAwareConverter) converterInstance).setDecisionTableKeyMap(decisionTableKeyMap);
                }

                converterInstance.convertToJson(flowElement, this, model, container, shapesArrayNode, containerX, containerY);
                setModelNodeExtensionElement(flowElement, shapesArrayNode);
            } catch (Exception var13) {
                LOGGER.error("Error converting {}", flowElement, var13);
            }
        }
    }

    // 将扩展属性写入当前模型节点的 properties.extension 里面
    private void setModelNodeExtensionElement(FlowElement flowElement, ArrayNode shapesArrayNode) {
        // 转换为json后，检查扩展元素属性写入json
        Map<String, List<ExtensionElement>> extensionElementMap = flowElement.getExtensionElements();
        if (CollectionUtils.isEmpty(extensionElementMap)) {
            return;
        }

        ObjectNode currentNode = (ObjectNode) shapesArrayNode.get(shapesArrayNode.size() - 1);
        ObjectNode properties = (ObjectNode) currentNode.get("properties");
        extensionElementMap.forEach((extensionName, extensionElements) -> {
            ObjectNode extensionNode = (ObjectNode) properties.get("extensions");
            if (extensionNode == null) {
                extensionNode = objectMapper.createObjectNode();
                properties.set("extensions", extensionNode);
            }
            JsonNode element = extensionNode.get(extensionName);
            if (element != null) {
                log.warn("flow element {} conflict extensionElement for name {}", flowElement.getId(), extensionName);
            }

            if (extensionElements.isEmpty()) {
                return;
            }

            if (extensionElements.size() > 1) {
                log.warn("flow element {} conflict extensionElement for name {} use first element", flowElement.getId(), extensionName);
            }

            String elementText = StringUtils.trimAllWhitespace(extensionElements.get(0).getElementText());
            if (!StringUtils.hasText(elementText)) {
                return;
            }

            // 如果是json格式则转换成json存储
            if (elementText.startsWith("{") && elementText.endsWith("}")) {
                try {
                    extensionNode.set(extensionName, objectMapper.readTree(elementText));
                } catch (JsonProcessingException jsonError) {
                    log.warn(elementText);
                    throw new MaeException("flow element " + flowElement.getId() + " extensionName json parse error", jsonError);
                }
            } else {
                // 其他格式保留为字符串
                extensionNode.put(extensionName, elementText);
            }
        });
    }

    @Override
    public BpmnModel convertToBpmnModel(JsonNode modelNode) {
        BpmnModel bpmnModel = convertToBpmnModel(modelNode, null, null);
        JsonNode extensionNode = modelNode.get("properties").get("extension");
        if (extensionNode != null) {
            extensionNode.fieldNames().forEachRemaining(extensionName -> {
                ExtensionElement extensionElement = new ExtensionElement();
                extensionElement.setName(extensionName);
                extensionElement.setElementText(extensionNode.get(extensionName).textValue());
                bpmnModel.getMainProcess().getExtensionElements().put(extensionName, Collections.singletonList(extensionElement));
            });
        }
        return bpmnModel;
    }

    @Override
    public void processJsonElements(JsonNode shapesArrayNode,
                                    JsonNode modelNode,
                                    BaseElement parentElement,
                                    Map<String, JsonNode> shapeMap,
                                    Map<String, String> formMap,
                                    Map<String, String> decisionTableMap,
                                    BpmnModel bpmnModel) {

        for (JsonNode shapeNode : shapesArrayNode) {
            String stencilId = BpmnJsonConverterUtil.getStencilId(shapeNode);
            Class<? extends BaseBpmnJsonConverter> converter = convertersToBpmnMap.get(stencilId);
            try {
                BaseBpmnJsonConverter converterInstance = converter.newInstance();
                if (converterInstance instanceof DecisionTableAwareConverter) {
                    ((DecisionTableAwareConverter) converterInstance).setDecisionTableMap(decisionTableMap);
                }

                if (converterInstance instanceof FormAwareConverter) {
                    ((FormAwareConverter) converterInstance).setFormMap(formMap);
                }

                converterInstance.convertToBpmnModel(shapeNode, modelNode, this, parentElement, shapeMap, bpmnModel);
                setFlowElementExtensions(shapeNode, parentElement);
            } catch (Exception e) {
                LOGGER.error("Error converting {}",
                        BpmnJsonConverterUtil.getStencilId(shapeNode),
                        e);
            }
        }
    }

    private void setFlowElementExtensions(JsonNode shapeNode, BaseElement parentElement) {
        JsonNode extensionNode = shapeNode.get("properties").get("extensions");
        if (extensionNode == null || extensionNode.isEmpty()) {
            return;
        }

        String resourceId = shapeNode.get("resourceId").textValue();
        FlowElement flowElement = ((Process) parentElement).getFlowElementMap().get(resourceId);

        extensionNode.fieldNames().forEachRemaining(extensionName -> {
            ExtensionElement extensionElement = new ExtensionElement();
            extensionElement.setName(extensionName);
            if (extensionNode.get(extensionName).isTextual()) {
                extensionElement.setElementText(extensionNode.get(extensionName).textValue());
            } else {
                try {
                    extensionElement.setElementText(objectMapper.writeValueAsString(extensionNode.get(extensionName)));
                } catch (Exception exception) {
                    throw new MaeException("flow element " + resourceId + " extension " + extensionName + " to json error");
                }
            }
            flowElement.getExtensionElements().put(extensionName, Collections.singletonList(extensionElement));
        });
    }
}
