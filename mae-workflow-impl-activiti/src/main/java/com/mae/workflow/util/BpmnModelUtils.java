package com.mae.workflow.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mae.core.utils.Mae;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
public class BpmnModelUtils {

    public static String jsonModelToXml(String jsonModel) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode modelNode = null;
        try {
            modelNode = objectMapper.readTree(jsonModel);
        } catch (IOException exception) {
            throw new IllegalArgumentException("bpmn json model read has error", exception);
        }
        BpmnModel bpmnModel = new MaeBpmnJsonConverter().convertToBpmnModel(modelNode);
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] bytes = bpmnXMLConverter.convertToXML(bpmnModel, "UTF-8");
        return new String(bytes);
    }

    public static BpmnModel xmlModelToBpmnModel(String xmlModel) {
        InputStream xmlInputStream = null;
        XMLStreamReader xmlStreamReader = null;
        try {
            XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
            xmlInputStream = new ByteArrayInputStream(xmlModel.getBytes(StandardCharsets.UTF_8));
            xmlStreamReader = xmlInputFactory.createXMLStreamReader(xmlInputStream);
            return new BpmnXMLConverter().convertToBpmnModel(xmlStreamReader);
        } catch (XMLStreamException exception) {
            throw new IllegalArgumentException("read xml model has error", exception);
        } finally {
            if (xmlInputStream != null) {
                try {
                    xmlInputStream.close();
                } catch (Exception closeException) {
                    log.warn("close xml input stream has error", closeException);
                }
            }
            if (xmlStreamReader != null) {
                try {
                    xmlStreamReader.close();
                } catch (XMLStreamException closeException) {
                    log.warn("close xml input stream has error", closeException);
                }
            }
        }
    }
}
