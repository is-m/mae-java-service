package com.mae.workflow.util;

import java.util.Map;
import java.util.Optional;

public class ProcessFormUtils {
    /**
     * 内置字段：流程标题，流程名称
     */
    private static final String TITLE = "_title";

    /**
     * 内置字段：审批意见
     */
    private static final String COMMENT = "_comment";

    public static String getTitle(Map<String, Object> mainform) {
        return getString(mainform, TITLE);
    }

    public static String getComment(Map<String, Object> taskform) {
        return getString(taskform, COMMENT);
    }

    private static String getString(Map<String, Object> map, String key) {
        if (map == null || !map.containsKey(key)) {
            return "";
        }
        return Optional.ofNullable(map.get(key)).orElse("").toString();
    }
}
