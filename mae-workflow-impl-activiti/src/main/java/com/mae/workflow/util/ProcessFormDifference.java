package com.mae.workflow.util;

import lombok.Getter;

import java.util.Collections;
import java.util.Map;


public class ProcessFormDifference {
    private boolean enabled;
    @Getter
    private String action;
    private Map<String, Object> oldMainform;
    private Map<String, Object> newMainform;
    private Map<String, Object> oldTaskform;
    private Map<String, Object> newTaskform;

    public Map<String, Object> getDifferences() {
        if (enabled) {

        }
        return Collections.emptyMap();
    }
}
