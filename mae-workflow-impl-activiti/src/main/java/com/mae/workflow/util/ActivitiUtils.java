package com.mae.workflow.util;

import com.mae.core.utils.Mae;
import org.activiti.engine.RuntimeService;

public class ActivitiUtils {

    public static boolean hasUndoProcessInstance(String deploymentId) {
        return !Mae.context().getBean(RuntimeService.class)
                .createProcessInstanceQuery().deploymentId(deploymentId)
                .active().listPage(0, 1).isEmpty();
    }

}
