package com.mae.workflow.service;

import com.mae.core.ReturnPagedCollection;
import com.mae.core.context.RequestContext;
import com.mae.workflow.dto.todo.MyApplicationDTO;
import com.mae.workflow.dto.todo.MyApplicationPageQueryDTO;
import com.mae.workflow.dto.todo.MyTodoPageQueryDTO;
import com.mae.workflow.persistence.dao.MyApplicationDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * https://blog.csdn.net/cold___play/article/details/135732392
 */
@ExtendWith(MockitoExtension.class)
public class MyApplicationQueryServiceTest {

    @InjectMocks
    private MyApplicationQueryService myApplicationQueryService;

    @Mock
    private MyApplicationDao myApplicationDao;

    private static final String userId = "test";

    @BeforeAll
    public static void setup() {
        Mockito.mockStatic(RequestContext.class);
        Mockito.when(RequestContext.getUserId()).thenReturn(userId);
    }

    @Test
    public void testGetMyApplyApplications() {
        MyApplicationPageQueryDTO myApplicationPageQueryDTO = new MyApplicationPageQueryDTO();
        ReturnPagedCollection<MyApplicationDTO> myApplications = myApplicationQueryService.getMyApplications(myApplicationPageQueryDTO);
        Mockito.verify(myApplicationDao, Mockito.times(1)).selectMyApplicationPageList(myApplicationPageQueryDTO);
        Assertions.assertEquals(myApplicationPageQueryDTO.getUserId(), userId);
    }

    @Test
    public void testGetMyTodoApplications() {
        MyTodoPageQueryDTO myTodoPageQueryDTO = new MyTodoPageQueryDTO();
        ReturnPagedCollection<MyApplicationDTO> myApplications = myApplicationQueryService.getMyTodoApplications(myTodoPageQueryDTO);
        Mockito.verify(myApplicationDao, Mockito.times(1)).selectMyTodoPageList(myTodoPageQueryDTO);
        Assertions.assertEquals(myTodoPageQueryDTO.getUserId(), userId);
    }

    @Test
    public void testGetMyApplications() {
        MyApplicationPageQueryDTO myApplicationPageQueryDTO = new MyApplicationPageQueryDTO();
        ReturnPagedCollection<MyApplicationDTO> myApplications = myApplicationQueryService.getMyApplications(myApplicationPageQueryDTO);
        Mockito.verify(myApplicationDao, Mockito.times(1)).selectMyApplicationPageList(myApplicationPageQueryDTO);
        Assertions.assertEquals(myApplicationPageQueryDTO.getUserId(), userId);
    }

}
