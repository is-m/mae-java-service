package com.mae.code;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CodeGeneratePreviewVO {
    private String name;
    private String code;
}
