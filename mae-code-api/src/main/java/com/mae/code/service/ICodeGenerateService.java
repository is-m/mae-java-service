package com.mae.code.service;

import com.mae.code.CodeGeneratePreviewVO;
import com.mae.core.ReturnMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/api/code-generates")
public interface ICodeGenerateService {
    /**
     * 预览
     *
     * @return
     */
    @GetMapping("/preview")
    List<CodeGeneratePreviewVO> preview();


}
