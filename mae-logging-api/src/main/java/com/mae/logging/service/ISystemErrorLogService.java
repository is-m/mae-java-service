package com.mae.logging.service;

import com.mae.logging.SystemErrorLogVO;

/**
 * 系统异常日志，包含应用启动，api访问时，异步时，消息异常等各类异常
 */
public interface ISystemErrorLogService {

    void createSystemErrorLog(SystemErrorLogVO systemErrorLogVO);
}
