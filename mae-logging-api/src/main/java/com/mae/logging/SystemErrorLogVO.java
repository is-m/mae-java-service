package com.mae.logging;

import com.mae.core.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class SystemErrorLogVO extends BaseEntity {
    /**
     * 类型，api,消息，等
     */
    private String type;
    /**
     * 来源,为api异常时来源为接口地址，其他可能时java类等
     */
    private String source;

    /**
     *
     */
    private Map<String, Object> payload;

    private String message;

    private String fullMessage;
}
