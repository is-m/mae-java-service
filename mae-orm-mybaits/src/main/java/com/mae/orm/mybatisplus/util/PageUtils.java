package com.mae.orm.mybatisplus.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.PageDTO;
import com.mae.core.PageQuery;
import com.mae.core.ReturnPagedCollection;
import com.mae.core.ReturnPagedMeta;

import java.util.List;

/**
 * 分页工具类
 */
public class PageUtils {
    /**
     * 获取mybaitsplus 分页查询对象
     *
     * @param pageQuery
     * @param <T>
     * @return
     */
    public static <T> IPage<T> toPage(PageQuery pageQuery){
        PageDTO<T> result = new PageDTO<>();
        result.setCurrent(pageQuery.getPage());
        result.setSize(pageQuery.getSize());
        result.setSearchCount(pageQuery.isCountable());
        return result;
    }

    /**
     * 获取mybaitsplus 分页查询对象
     *
     * @param page
     * @param size
     * @param <T>
     * @return
     */
    public static <T> IPage<T> toPage(long page, long size){
        IPage<T> result = new PageDTO<>();
        result.setCurrent(page);
        result.setSize(size);
        return result;
    }

    /**
     * 获取分页集合
     *
     * @param page
     * @param records
     * @param <T>
     * @return
     */
    public static <T> ReturnPagedCollection<T> toPagedCollection(IPage<T> page, List<T> records){
        ReturnPagedMeta meta = ReturnPagedMeta.ofTotalElements((int) page.getCurrent(), (int) page.getSize(), page.getTotal());
        return ReturnPagedCollection.of(meta, records);
    }
}
