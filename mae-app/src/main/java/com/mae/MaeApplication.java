package com.mae;

import com.mae.core.JarUtils;
import com.mae.core.utils.Mae;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import java.io.IOException;

@EnableCaching
@SpringBootApplication
public class MaeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaeApplication.class, args);
        /*Mae.getResources().forEach(item-> {
            try {
                System.out.println(item.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });*/

        //JarUtils.printLoad();
        //JarUtils.getJarPath();
    }

}
