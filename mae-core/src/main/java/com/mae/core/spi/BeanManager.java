package com.mae.core.spi;

/**
 * bean 管理
 */
public interface BeanManager {
    /**
     * 注册bean
     * @param name
     * @param bean
     */
    void registerBean(String name, Object bean);
}
