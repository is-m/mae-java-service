package com.mae.core.spi;

/**
 * 事件发布者
 */
public interface EventPublisher {
    void publish(Object event);
}
