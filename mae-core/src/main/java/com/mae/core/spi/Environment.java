package com.mae.core.spi;

import java.lang.reflect.Constructor;

/**
 * 环境
 */
public interface Environment {
    /**
     * 获取环境属性
     *
     * @param key 属性名
     * @return 属性值
     */
    String getProperty(String key);

    /**
     * 获取环境属性（带默认值）
     *
     * @param key          属性名
     * @param defaultValue 默认值
     * @return 属性值，不存在属性时返回默认值
     */
    String getProperty(String key, String defaultValue);

    /**
     * 解析占位符，
     *
     * @param obj    待解析的内容
     * @param prefix
     * @return
     */
    <T> T resolvePlaceholder(T obj, String prefix);

    default <T> T resolve(Class<T> clz, String prefix) {
        try {
            Constructor<T> constructor = clz.getConstructor();
            T obj = constructor.newInstance();
            return resolvePlaceholder(obj, prefix);
        } catch (Exception e) {
            return null;
        }
    }
}
