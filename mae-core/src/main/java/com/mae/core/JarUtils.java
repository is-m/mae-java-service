package com.mae.core;

import java.io.File;
import java.net.URISyntaxException;

/**
 * Jar
 */
public class JarUtils {

    public static void getJarPath() {
        try {
            Exception exception = new Exception();
            String jarPath = new File(JarUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParent();
            System.out.println(jarPath);
        } catch (URISyntaxException e) {
            throw new RuntimeException("uri syntax", e);
        }
    }

    public static void printLoad() {
        Package[] packages = Package.getPackages();
        for (Package p : packages) {
            String jarFile = p.getImplementationTitle();
            String version = p.getImplementationVersion();
            String vendor = p.getImplementationVendor();
            if (jarFile != null) {
                System.out.println("Jar: " + jarFile);
                if (version != null) {
                    System.out.println("Version: " + version);
                }
                if (vendor != null) {
                    System.out.println("Vendor: " + vendor);
                }
            }
        }
    }
}
