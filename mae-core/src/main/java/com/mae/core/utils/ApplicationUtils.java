package com.mae.core.utils;

import com.mae.core.event.ApplicationBeforeStartEvent;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 应用
 */
@Slf4j
public class ApplicationUtils {

    public static void run(Class<?> startClass, String... args) {
        log.info("mae application start");
        // 应用启动前是个独立的存在，应用启动前无法注入IOC的对象，因为环境未完成初始化
        // EventUtils.trigger(new ApplicationBeforeStartEvent(ApplicationUtils.class));
        // 检查系统是否存在springboot的启动类，如果存在使用springboot的方式启动应用
        try {
            Class<?> SpringApplicationClass = Class.forName("org.springframework.boot.SpringApplication");
            Method runMethod = SpringApplicationClass.getMethod("run", Class.class, String[].class);
            // 调用 SpringApplication.run 静态方法
            runMethod.invoke(null, startClass, args);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            log.info("");
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
