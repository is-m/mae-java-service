package com.mae.core.utils;

import java.text.MessageFormat;

/**
 * 消息工具类
 */
public class MessageUtils {

    /**
     * 获取系统配置为空的提示消息
     *
     * @param name 配置键
     * @return 提示消息
     */
    public static String getAppConfigEmpty(String name){
        return MessageFormat.format("application config '{0}' is empty", name);
    }
}
