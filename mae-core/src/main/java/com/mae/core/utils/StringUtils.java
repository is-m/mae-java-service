package com.mae.core.utils;

import java.util.regex.Pattern;

/**
 * 字符串工具类
 */
public class StringUtils {

    /**
     * 是否有字符
     *
     * @param text 待判断的字符串
     * @return 非空时返回 true，为空时返回 false
     */
    public static boolean hasText(String text) {
        return text != null && !text.isEmpty();
    }

    /**
     * 是否为空
     *
     * @param text 待判断的字符串
     * @return 为空时返回 true，非空时返回 false
     */
    public static boolean isEmpty(String text) {
        return text == null || text.isEmpty();
    }

    /**
     * 去除前后空格
     *
     * @param text 待去除空格的字符串
     * @return 去除空格后的字符串
     */
    public static String trim(String text) {
        return text == null ? null : text.trim();
    }

    /**
     * 纯字符替换
     *
     * @param text 文本
     * @param from 待替换字符
     * @param to   替换为
     * @return 替换后的字符串
     */
    public static String replace(String text, String from, String to) {
        return text == null ? null : text.replaceAll(Pattern.quote(from), to);
    }

    /**
     * 截取字符串
     *
     * @param text  字符串
     * @param start 开始位置，起始 0
     * @param end   截取结束位置
     * @return 截取的字符串
     */
    public static String substring(String text, int start, int end) {
        if (text == null) {
            return null;
        }
        int lastIndex = text.length() - 1;
        if (start < 0 || start > lastIndex) {
            return "";
        }
        if (end < start || end > lastIndex) {
            end = lastIndex;
        }

        return text.substring(start, end);
    }

    /**
     * 截取字符串
     *
     * @param text  字符串
     * @param start 开始位置，起始 0
     * @return 截取的字符串
     */
    public static String substring(String text, int start) {
        if (text == null) {
            return null;
        }
        int lastIndex = text.length() - 1;
        if (start < 0 || start > lastIndex) {
            return "";
        }
        return text.substring(start);
    }

    public static boolean contains(String message, String text) {
        return hasText(message) && message.contains(text);
    }

    /**
     * 字符串为空时取默认值
     *
     * @param text 待校验的字符串
     * @param defaultText 默认字符串
     * @return text不为空(nullOrEmpty)返回text， 否则返回defaultText
     */
    public static String nvl(String text, String defaultText) {
        return hasText(text) ? text : defaultText;
    }
}
