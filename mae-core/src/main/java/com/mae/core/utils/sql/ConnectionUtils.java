package com.mae.core.utils.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {
    /**
     * @return
     */
    public static Connection getConnection(String driverClass, String url, String user, String password) {
        try {
            Class.forName(driverClass);
            return DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("driver '" + driverClass + "' class is not found", e);
        } catch (SQLException e) {
            throw new IllegalArgumentException("driver '" + driverClass + "' class is not found", e);
        }
    }
}
