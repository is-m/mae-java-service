package com.mae.core.utils;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

/**
 * 类工具
 */
public class ClassUtils {

    public static void main(String[] args) throws Exception {
        String packageName = "com.mae"; // 替换为你的包名
        findAllClasses(packageName);
    }

    public static void findAllClasses(String packageName) throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            String protocol = resource.getProtocol();
            if ("file".equals(protocol)) {
                findClassesInDirectory(new File(URLDecoder.decode(resource.getPath(), StandardCharsets.UTF_8)), packageName);
            }
        }
    }

    private static void findClassesInDirectory(File directory, String packageName) throws ClassNotFoundException {
        if (!directory.exists()) {
            return;
        }
        File[] files = directory.listFiles();
        if (files == null) {
            return;
        }
        for (File file : files) {
            if (file.isFile() && file.getName().endsWith(".class")) {
                String className = packageName + '.' + file.getName().substring(0, file.getName().length() - 6);
                System.out.println(className);
                Class.forName(className);
            } else if (file.isDirectory()) {
                findClassesInDirectory(file, packageName + "." + file.getName());
            }
        }
    }

    /**
     * 根据完整类名实例化对象
     *
     * @param fullName 带有包路径的类名，如 test.a.ClassA
     * @param <T> 返回类型
     * @return 类型实例
     */
    public static <T> T newInstance(String fullName) {
        try {
            Class<?> clz = Class.forName(fullName);
            return (T) clz.getConstructor().newInstance();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("类型不存在 " + fullName, e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("无参的构造函数未找到 " + fullName, e);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("执行构造函数失败 " + fullName, e);
        } catch (ClassCastException e) {
            throw new RuntimeException("接收对象的类型不匹配 " + fullName, e);
        }
    }
}
