package com.mae.core.utils;

import com.mae.core.exception.MaeException;

import java.util.Objects;

public class Assert {

    public static void notNull(Object obj, String message) {
        if (obj == null) {
            throw new MaeException(message);
        }
    }

    public static void notEmpty(Object obj, String message) {
        if (obj == null) {
            throw new MaeException(message);
        }

        if (obj instanceof String && obj.toString().trim().length() == 0) {
            throw new MaeException(message);
        }

        throw new MaeException("not implementation");
    }

    public static void isTrue(boolean expr, String message) {
        if (!expr) {
            throw new MaeException(message);
        }
    }

    public static void isFalse(boolean expr, String message) {
        if (expr) {
            throw new MaeException(message);
        }
    }

    public static void isFalse(boolean expr, String message, Object... args) {
        if (expr) {
            throw new MaeException(message, args);
        }
    }

    public static void isEquals(Object valA, Object valB, String message) {
        if (!Objects.equals(valA, valB)) {
            throw new MaeException(message);
        }
    }

    public static void isEquals(Object valA, Object valB, String message, Object... args) {
        if (!Objects.equals(valA, valB)) {
            throw new MaeException(message, args);
        }
    }

    public static void notEquals(Object valA, Object valB, String message) {
        if (Objects.equals(valA, valB)) {
            throw new MaeException(message);
        }
    }

    public static void isContains(String container, String match, String message) {
        if (container == null || !container.contains(match)) {
            throw new MaeException(message);
        }
    }

    public static void notContains(String container, String match, String message) {
        Objects.requireNonNull(container, "container is not be null");
        if (container.contains(match)) {
            throw new MaeException(message);
        }
    }

    public static void hasText(String text, String message) {
        if (StringUtils.isEmpty(text)) {
            throw new IllegalArgumentException(message);
        }
    }
}
