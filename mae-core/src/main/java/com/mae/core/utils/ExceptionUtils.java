package com.mae.core.utils;

import java.util.Optional;

/**
 * 异常工具类
 */
public class ExceptionUtils {
    private int SHORT_LENGTH = 500;

    /**
     * 获取跟异常消息
     *
     * @param throwable 异常
     * @return 获取跟消息
     */
    public static String getRootMessage(Throwable throwable) {
        Assert.notNull(throwable, "throwable not be null");
        Throwable cause = throwable;
        while (cause.getCause() != null) {
            cause = cause.getCause();
        }
        return Optional.ofNullable(cause.getMessage()).orElse("");
    }

    public static String getShortRootMessage(Throwable throwable){
        return StringUtils.substring(getRootMessage(throwable), 0, 500);
    }

}
