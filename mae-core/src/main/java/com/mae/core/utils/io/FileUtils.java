package com.mae.core.utils.io;

import com.mae.core.utils.HashAlgorithm;
import com.mae.core.utils.HashUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

public class FileUtils {
    /**
     * 文件哈希
     *
     * @param file          待hash的文件
     * @param hashAlgorithm 算法
     * @return hash
     */
    public static String hash(File file, HashAlgorithm hashAlgorithm) {
        try {
            return HashUtils.hash(Files.readAllBytes(file.toPath()), hashAlgorithm);
        } catch (IOException e) {
            throw new IllegalStateException("read file hash has error", e);
        }
    }
}
