package com.mae.core.utils;

import com.mae.core.principle.IExpire;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 过期工具类
 */
public class ExpireUtils {

    /**
     * 获取未过期的记录
     *
     * @param expires
     * @param <T>
     * @return
     */
    public static <T extends IExpire> List<T> getNotExpireList(List<T> expires) {
        return getNotExpireStream(expires).collect(Collectors.toList());
    }

    /**
     * 获取未过期的列表
     *
     * @param expires
     * @param <T>
     * @return
     */
    public static <T extends IExpire> Stream<T> getNotExpireStream(List<T> expires) {
        if (expires == null || expires.isEmpty()) {
            return Stream.empty();
        }
        return expires.stream().filter(ExpireUtils::notExpire);
    }

    /**
     * 获取临期天数
     *
     * @param expires
     * @param <T>
     * @return
     */
    public static <T extends IExpire> Integer getAdventDays(T expires) {
        if (expires == null) {
            return null;
        }

        throw new RuntimeException("not implementation");
    }

    /**
     * 未过期
     *
     * @param expire
     * @param <T>
     * @return
     */
    public static <T extends IExpire> boolean notExpire(T expire) {
        return !isExpire(expire);
    }

    /**
     * 已过期
     *
     * @param expire
     * @param <T>
     * @return
     */
    public static <T extends IExpire> boolean isExpire(T expire) {
        Date expireDate = expire.getExpireDate();
        if (expireDate == null) {
            return false;
        }
        return expireDate.before(DateUtils.getNowDate());
    }
}
