package com.mae.core.utils;

import com.mae.core.spi.EventPublisher;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventUtils {
    /**
     * 触发系统事件
     *
     * @param event
     */
    public static void trigger(Object event) {
        Assert.notNull(event, "event cannot be null");
        // 触发mae定义的事件
        // 找到所有事件处理者的代码
        // ClassUtils.findMethod(com.mae.core.annotations.EventListener);
        EventPublisher publisher = Mae.getBean(EventPublisher.class);
        if (publisher != null) {
            // 触发事件代理组件的事件
            publisher.publish(event);

        } else {
            log.warn("application context not found EventPublisher ignore the event publish {} ", event.getClass());
        }
    }

}
