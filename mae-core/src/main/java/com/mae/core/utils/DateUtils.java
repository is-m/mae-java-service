package com.mae.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具
 */
@Slf4j
public class DateUtils {
    /**
     * 获取当前系统日期
     *
     * @return
     */
    public static Date getNowDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf.format(sdf);
        try {
            return sdf.parse(format);
        } catch (ParseException e) {
            throw new RuntimeException("get now date has error", e);
        }
    }
}
