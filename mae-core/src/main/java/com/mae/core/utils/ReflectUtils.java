package com.mae.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedHashSet;

/**
 * 反射工具类
 */
@Slf4j
public class ReflectUtils {

    public static Field[] getAllFields(Class<?> type) {
        // 使用LinkedHashSet保留字段声明的顺序
        LinkedHashSet<Field> fields = new LinkedHashSet<>();
        // 递归获取类及其所有父类的字段
        while (type != null) {
            Field[] declaredFields = type.getDeclaredFields();
            Collections.addAll(fields, declaredFields);
            type = type.getSuperclass();
        }
        // 将Set转换为数组
        return fields.toArray(new Field[0]);
    }

    /**
     * 获取类型字段，不存在时返回null，会卷积父类查找字段
     *
     * @param type  类型
     * @param field 字段
     * @return 字段，不存在时返回null
     */
    public static Field getDeclaredField(Class<?> type, String field) {
        // 递归获取类及其所有父类的字段
        while (type != null) {
            try {
                return type.getDeclaredField(field);
            } catch (NoSuchFieldException e) {
                log.trace("class {} field {} is no such", type, field);
            }

            type = type.getSuperclass();
        }
        return null;
    }
}
