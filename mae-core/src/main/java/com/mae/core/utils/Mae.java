package com.mae.core.utils;

import com.mae.core.context.ApplicationContext;
import com.mae.core.spi.BeanManager;
import com.mae.core.struct.Tuple3;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * MAE 核心工具类
 */
@Slf4j
public class Mae {
    private static final Tuple3<String, Class<?>, Object> BEANS = new Tuple3<>();
    private static final List<File> APPLICATION_RESOURCES;
    private static final List<File> APPLICATION_NON_CLASS_RESOURCES;

    static {
        List<File> resources = Collections.emptyList();
        List<File> nonClassResources = Collections.emptyList();
        try {
            resources = ApplicationResourceUtils.scan();
            nonClassResources = resources.stream().filter(file -> !file.getName().endsWith(".class")).toList();
            log.info("load resource size {}, non class resource size {}", resources.size(), nonClassResources.size());

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            APPLICATION_RESOURCES = resources;
            APPLICATION_NON_CLASS_RESOURCES = nonClassResources;
        }
    }

    private static ApplicationContext CONTEXT;

    public static ApplicationContext context() {
        return CONTEXT;
    }

    public static void setContext(ApplicationContext context) {
        Assert.notNull(context, "application context cannot set to null");
        Mae.CONTEXT = context;
    }

    /**
     * 获取 bean
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getBean(Class<T> clazz) {
        ApplicationContext context = context();
        if (context == null) {
            Tuple3.Tuple3Entry<String, Class<?>, Object> entry = BEANS.getByT(clazz);
            if (entry == null) {
                log.warn("application context not initialized, unable to retrieve bean {}", clazz);
                return null;
            } else {
                log.info("application context not initialized, register beans found {}", clazz);
                return (T) entry.getV();
            }
        }
        return context.getBean(clazz);
    }


    /**
     * 注册bean
     *
     * @param name                  bean名称
     * @param instance              bean实例
     * @param registerToImplementer IOC实现者传递注册
     */
    public static void registerBean(String name, Object instance, boolean registerToImplementer) {
        Assert.hasText(name, "register bean name not be null or empty");
        Assert.notNull(instance, "register bean instance not be null");
        boolean added = BEANS.add(name, instance.getClass(), instance);
        if (added) {
            if (registerToImplementer) {
                BeanManager manager = getBean(BeanManager.class);
                if (manager != null) {
                    manager.registerBean(name, instance);
                }
            }
        } else {
            log.warn("bean {} is registered", name);
        }
    }

    /**
     * 过滤上下文的bean
     *
     * @param filter
     * @return
     */
    public static List<Object> filterBeans(Predicate<Tuple3.Tuple3Entry<String, Class<?>, Object>> filter) {
        Assert.notNull(filter, "filter beans the filter not be null");
        return BEANS.entries().stream().filter(filter).map(Tuple3.Tuple3Entry::getV).collect(Collectors.toList());
    }

    /**
     * 获取全部资源
     *
     * @return
     */
    public static List<File> getResources() {
        return APPLICATION_RESOURCES;
    }

    /**
     * 获取非类资源
     *
     * @return
     */
    public static List<File> getNonClassResources() {
        return APPLICATION_NON_CLASS_RESOURCES;
    }

}
