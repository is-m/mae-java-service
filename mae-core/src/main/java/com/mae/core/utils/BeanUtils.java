package com.mae.core.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Bean 工具类
 */
public class BeanUtils {
    /**
     * Bean to map
     *
     * @param bean javabean
     * @return map
     */
    public static Map<String, Object> toMap(Object bean) {
        if (bean instanceof Map) {
            return (Map<String, Object>) bean;
        }
        Map<String, Object> resultMap = new HashMap<>();
        if (bean == null) {
            return resultMap;
        }

        Field[] allFields = ReflectUtils.getAllFields(bean.getClass());
        for (Field field : allFields) {
            try {
                field.setAccessible(true);
                Object value = field.get(bean);
                resultMap.put(field.getName(), value);
            } catch (Exception e) {
                throw new IllegalArgumentException("bean field handle error", e);
            }
        }
        return resultMap;
    }
}
