package com.mae.core.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class AESUtils {

    public String encrypt(String secret, String text) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        byte[] keyBytes = new byte[16];
        System.arraycopy(secret.getBytes(), 0, keyBytes, 0, Math.min(keyBytes.length, secret.getBytes().length));
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] encryptedBytes = cipher.doFinal(text.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    public String decrypt(String secret,String encryptedText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        byte[] keyBytes = new byte[16];
        System.arraycopy(secret.getBytes(), 0, keyBytes, 0, Math.min(keyBytes.length, secret.getBytes().length));
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
        return new String(decryptedBytes);
    }
}
