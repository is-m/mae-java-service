package com.mae.core.utils;

public enum HashAlgorithm {
    MD5("MD5"),
    SHA_256("SHA-256"),
    SHA_512("SHA-512"),
    SHA_1("SHA1");

    public final String algorithm;

    HashAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

}
