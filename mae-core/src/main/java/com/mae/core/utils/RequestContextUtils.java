package com.mae.core.utils;

import com.mae.core.context.RequestContext;
import com.mae.core.context.UserPrincipal;

import java.util.Objects;

/**
 * 请求上下文工具类
 */
public class RequestContextUtils {
    /**
     * 获取租户ID
     *
     * @return 租户ID
     * @throws NullPointerException 找不到用戶信息或者租戶信息時提示异常
     */
    public static String getTenantId() throws NullPointerException {
        UserPrincipal user = RequestContext.getCurrent().getUser();
        Objects.requireNonNull(user, "current request context no user be found");
        String tenantId = user.getTenantId();
        Objects.requireNonNull(tenantId, "user tenant is null");
        return tenantId;
    }
}
