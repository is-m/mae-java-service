package com.mae.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * 应用资源工具
 */
@Slf4j
public class ApplicationResourceUtils {

    public static List<File> scan() throws IOException, ClassNotFoundException {
        List<File> resultList = new ArrayList<>(1000);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Enumeration<URL> resources = classLoader.getResources("");

        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            String protocol = resource.getProtocol();
            if ("file".equals(protocol)) {
                File file = new File(URLDecoder.decode(resource.getPath(), StandardCharsets.UTF_8));
                findClassesInDirectory(file, "", resultList);
            }
        }
        return resultList;
    }

    private static void findClassesInDirectory(File directory, String packageName, List<File> fileContainer) throws ClassNotFoundException {
        if (!directory.exists()) {
            return;
        }
        File[] files = directory.listFiles();
        if(files == null){
            return;
        }
        for (File file : files) {
            if (file.isFile()) {
                fileContainer.add(file);
            } else if (file.isDirectory()) {
                findClassesInDirectory(file, packageName + "/" + file.getName(),fileContainer);
            } else {
                log.info("file {} is not file or directory", file.getName());
            }
        }
    }
}
