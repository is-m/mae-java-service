package com.mae.core.utils.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 集合工具类
 */
public class CollectionUtils {

    /**
     * 集合分片
     *
     * @param list 待分片的集合
     * @param size 集合大小
     * @param <T> 泛型
     * @return 分片后的多个集合
     */
    public static <T> List<List<T>> partition(List<T> list, int size) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }

        int listSize = (list.size() + size - 1) / size;
        List<List<T>> result = new ArrayList<>(listSize);
        for (int i = 0; i < listSize; i++) {
            int start = i * size;
            int end = start + size;
            if(end > listSize){
                end = listSize;
            }
            result.add(list.subList(start, end));
        }
        return result;
    }
}
