package com.mae.core.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtils {
    /**
     * 字符哈希
     *
     * @param text          文本
     * @param hashAlgorithm 算法
     * @return hash
     */
    public static String hash(String text, HashAlgorithm hashAlgorithm) {
        return hash(text.getBytes(StandardCharsets.UTF_8), hashAlgorithm);
    }

    /**
     * 字节哈希
     *
     * @param bytes         bytes
     * @param hashAlgorithm 算法枚举
     * @return hash
     */
    public static String hash(byte[] bytes, HashAlgorithm hashAlgorithm) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(hashAlgorithm.algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("hash algorithm '" + hashAlgorithm.algorithm + "' is no such", e);
        }
        messageDigest.update(bytes);
        byte[] digestBytes = messageDigest.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : digestBytes) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
