package com.mae.core.annotations;

/**
 * 配置属性
 */
public @interface MaeConfigurationProperties {

    String value() default "";

    String prefix() default "";

}
