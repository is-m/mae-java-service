package com.mae.core.annotations;

public enum SecurityPolicy {
    /**
     * 所有用户
     */
    ALL,
    /**
     * 登录用户
     */
    LOGIN,
    /**
     * 系统用户，
     */
    SYSTEM,
    /**
     * 第一个带权限的接口具备权限，则不再进行权限检查
     */
    DEFAULT,
    /**
     * 每次都进行权限权限检查
     */
    REQUIRED,
}
