package com.mae.core.annotations;

import java.lang.annotation.*;

/**
 * 事件收听
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MaeEventListener {
    /**
     * 事件类型
     *
     * @return
     */
    Class<?> value();

}
