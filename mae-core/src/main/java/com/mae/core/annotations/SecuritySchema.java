package com.mae.core.annotations;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SecuritySchema {
    String value() default "";
    String code() default "";
    String desc() default "";
    SecurityPolicy policy() default SecurityPolicy.DEFAULT;

    /**
     * 默认具备权限的角色编码
     * @return
     */
    String roles() default "System Admin";
}
