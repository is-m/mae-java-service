package com.mae.core.annotations.orm;

public enum IndexType {
    /**
     * 数据库默认索引
     */
    DEFAULT,
    /**
     * 唯一索引
     */
    UNIQUE;
}
