package com.mae.core.annotations;

import java.lang.annotation.*;

/**
 * 表
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {
    /**
     * 表名称，为空时表名字跟实体名称一致
     * @return
     */
    String value() default "";
}
