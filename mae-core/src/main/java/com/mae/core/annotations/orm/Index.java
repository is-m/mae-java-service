package com.mae.core.annotations.orm;

public @interface Index {
    /**
     * 索引名称
     * @return
     */
    String value() default  "";

    /**
     * 索引类型
     *
     * @return
     */
    IndexType type() default IndexType.DEFAULT;

}
