package com.mae.core.context;

import com.mae.core.principle.IExpire;
import com.mae.core.principle.ITenant;
import com.mae.core.utils.ExpireUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.mae.core.constant.SecurityConstant.ANONYMOUS;

/**
 * 用户信息
 */
public interface UserPrincipal extends IExpire, ITenant {
    /**
     * 用户id
     *
     * @return
     */
    default String getId() {
        return ANONYMOUS;
    }

    /**
     * 工号
     *
     * @return
     */
    default String getEmpNo() {
        return ANONYMOUS;
    }

    /**
     * 中文名
     *
     * @return
     */
    default String getName() {
        return ANONYMOUS;
    }

    /**
     * 英文名
     *
     * @return
     */
    default String getNameEn() {
        return getName();
    }

    /**
     * 当前组织
     *
     * @return
     */
    default String getCurrentOrganization() {
        List<? extends OrganizationPrincipal> organizations = getOrganizations();
        return ExpireUtils.getNotExpireStream(organizations).map(OrganizationPrincipal::getCode).collect(Collectors.joining(";"));
    }

    /**
     * 用户组织(部门)
     *
     * @return
     */
    default List<? extends OrganizationPrincipal> getOrganizations() {
        return Collections.emptyList();
    }

    /**
     * 获取当前角色，当用户权限未合并时
     *
     * @return
     */
    default String getCurrentRole() {
        List<? extends RolePrincipal> roles = getRoles();
        if (roles == null || roles.isEmpty()) {
            return "";
        }
        return ExpireUtils.getNotExpireStream(roles).map(RolePrincipal::getCode).collect(Collectors.joining(";"));
    }

    /**
     * 用户角色
     *
     * @return
     */
    default List<? extends RolePrincipal> getRoles() {
        return Collections.emptyList();
    }

    /**
     * 用户租户列表
     *
     * @return
     */
    default List<? extends TenantPrincipal> getTenants() {
        return Collections.emptyList();
    }

}
