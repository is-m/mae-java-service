package com.mae.core.context;

import com.mae.core.principle.IExpire;

/**
 * 权限
 */
public interface PermissionPrincipal extends IExpire {
    /**
     * 权限编码
     * @return
     */
    String getCode();

    /**
     * 说明
     * @return
     */
    String getDesc();
}
