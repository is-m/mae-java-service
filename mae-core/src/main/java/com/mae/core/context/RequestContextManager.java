package com.mae.core.context;

import com.mae.core.utils.Assert;

import java.util.Map;
import java.util.UUID;

/**
 * 请求上下文管理器
 */
public class RequestContextManager {
    // InheritableThreadLocal 可以让子线程获取父线程的值,但是线程池下会失效，可以使用包装类来设置当前线程的上下文
    private static final ThreadLocal<RequestContext> CONTEXTS = new ThreadLocal<>();

    public static void set(RequestContext context) {
        CONTEXTS.set(context);
    }

    public static RequestContext get() {
        return CONTEXTS.get();
    }

    public static void remove() {
        CONTEXTS.remove();
    }

    /**
     * 应用启动时初始化
     */
    public static void initForApplicationStarting() {
        Assert.isTrue(get() == null, "the request context already exists");
        UserPrincipal appUser = new UserPrincipal() {
            @Override
            public String getId() {
                return "applicationStart";
            }
        };

        set(new RequestContext() {
            @Override
            public UserPrincipal getUser() {
                return appUser;
            }

            @Override
            public String getTraceId() {
                return UUID.randomUUID().toString();
            }

            @Override
            public Map<String, Object> getSession() {
                return null;
            }

            @Override
            public Object getAttribute(String key) {
                return null;
            }

            @Override
            public void setAttribute(String key, Object value) {

            }
        });
    }

}
