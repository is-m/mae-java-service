package com.mae.core.context;

import com.mae.core.principle.IExpire;
import com.mae.core.principle.ITenant;

import java.util.Collections;
import java.util.List;

/**
 * 组织机构/部门
 */
public interface OrganizationPrincipal extends IExpire, ITenant {
    /**
     * 组织ID
     * @return
     */
    String getId();

    /**
     * 组织编码
     * @return
     */
    String getCode();

    /**
     * 组织名称
     * @return
     */
    String getName();

    /**
     * 组织英文名称
     * @return
     */
    default String getNameEn() {
        return getName();
    }

    /**
     * 父组织ID
     * @return
     */
    String getParentId();

    /**
     * 完整名称，如 /xx公司/xx部门
     * @return
     */
    String getFullName();

    /**
     * 组织机构权限
     *
     * @return
     */
    default List<? extends PermissionPrincipal> getPermissions(){
        return Collections.emptyList();
    }

    /**
     * 组织角色
     *
     * @return
     */
    default List<? extends RolePrincipal> getRoles() {
        return Collections.emptyList();
    }
}
