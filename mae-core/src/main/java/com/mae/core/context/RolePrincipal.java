package com.mae.core.context;


import com.mae.core.principle.IExpire;
import com.mae.core.principle.ITenant;

import java.util.Collections;
import java.util.List;

public interface RolePrincipal extends IExpire, ITenant {
    /**
     * ID
     * @return
     */
    String getId();

    /**
     * 角色编码
     * @return
     */
    String getCode();

    /**
     * 中文名
     * @return
     */
    String getName();

    /**
     * 英文名
     * @return
     */
    default String getNameEn() {
        return getName();
    }

    /**
     * 权限
     *
     * @return
     */
    default List<? extends PermissionPrincipal> getPermissions() {
        return Collections.emptyList();
    }

}
