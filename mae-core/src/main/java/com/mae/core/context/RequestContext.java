package com.mae.core.context;

import java.util.Map;
import java.util.UUID;

public interface RequestContext {
    /**
     * 当前请求用户
     *
     * @return
     */
    UserPrincipal getUser();

    /**
     * 当前请求链路
     *
     * @return 请求链路ID
     */
    String getTraceId();

    /**
     * 获取当前会话Map
     *
     * @return
     */
    Map<String, Object> getSession();

    /**
     * 获取请求上下文属性
     *
     * @param key key
     * @return 属性
     */
    Object getAttribute(String key);

    /**
     * 设置请求上下文属性
     *
     * @param key   key
     * @param value value
     */
    void setAttribute(String key, Object value);

    /**
     * 获取当前请求上下文
     *
     * @return
     */
    static RequestContext getCurrent() {
        return RequestContextManager.get();
    }

    /**
     * 获取当前请求上下文租户ID
     *
     * @return 租户ID
     */
    static String getTenantId() {
        return getCurrent().getUser().getTenantId();
    }

    /**
     * 获取当前请求上下文用户ID
     *
     * @return 用户ID
     */
    static String getUserId() {
        return getCurrent().getUser().getId();
    }

    static Object get(String key) {
        return getCurrent().getAttribute(key);
    }

    static void put(String key, Object value) {
        getCurrent().setAttribute(key, value);
    }


}
