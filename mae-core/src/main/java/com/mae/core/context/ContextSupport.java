package com.mae.core.context;

/**
 * 上下文
 */
public class ContextSupport<T> {
    // InheritableThreadLocal 可以让子线程获取父线程的值,但是线程池下会失效，可以使用包装类来设置当前线程的上下文
    private final ThreadLocal<T> LOCAL = new ThreadLocal<>();

    /**
     * 设置当前上下文
     */
    public void set(T context) {
        LOCAL.set(context);
    }

    /**
     * 获取
     *
     * @return 当前上下文中的数据
     */
    public T get() {
        return LOCAL.get();
    }

    /**
     * 移除
     */
    public void remove() {
        LOCAL.remove();
    }

}
