package com.mae.core.context;

/**
 * 应用上下文
 */
public interface ApplicationContext {

    /**
     * 代理的类
     *
     * @return delegate
     */
    Object getDelegate();

    /**
     * 获取bean
     *
     * @param clazz 类
     * @param <T>   T
     * @return bean
     */
    <T> T getBean(Class<T> clazz);

    /**
     * 获取bean
     *
     * @param name  bean名称
     * @param clazz 类
     * @param <T>   T
     * @return bean
     */
    <T> T getBean(String name, Class<T> clazz);

}
