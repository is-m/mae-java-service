package com.mae.core.context;

import com.mae.core.principle.IExpire;

public interface TenantPrincipal extends IExpire {
    /**
     * ID
     * @return
     */
    String getId();

    /**
     * 编码
     * @return
     */
    String getCode();

    /**
     * 租户名称
     * @return
     */
    String getName();
}
