package com.mae.core;

import java.text.MessageFormat;

public class ReturnPagedMeta extends ReturnMeta {
    public ReturnPagedMeta() {
        this(1, 20, 0, 1, false);
    }

    private ReturnPagedMeta(int page, int size, long totalElements, int totalPages, boolean hasNext) {
        this.set("page", page);
        this.set("size", size);
        if (totalElements > -1) {
            this.set("totalElements", totalElements);
            this.set("totalPages", totalPages);
        }
        this.set("hasNext", hasNext);
    }

    public static ReturnPagedMeta ofTotalElements(int page, int size, long totalElements) {
        int totalPages = (int) ((totalElements + size - 1) / size);
        return new ReturnPagedMeta(page, size, totalElements, totalPages, page < totalPages);
    }

    public static ReturnPagedMeta ofNonTotalElements(int page, int size, long elements) {
        if (elements > size) {
            String error = MessageFormat.format("page elements {0} more then size {1}", page, size);
            throw new IllegalArgumentException(error);
        }
        return new ReturnPagedMeta(page, size, -1, -1, elements == size);
    }

    public static ReturnPagedMeta ofPageVO(PageVO pageVO) {
        if (pageVO.isIncludeTotal()) {
            return ofTotalElements(pageVO.getCurPage(), pageVO.getPageSize(), pageVO.getTotalRecord());
        }
        return ofNonTotalElements(pageVO.getCurPage(), pageVO.getPageSize(), pageVO.getTotalRecord());
    }
}
