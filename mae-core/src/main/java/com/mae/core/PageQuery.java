package com.mae.core;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageQuery {
    @Min(1)
    private int page = 1;
    @Min(1)
    @Max(500)
    private int size = 15;
    // 返回总记录数
    private boolean countable = true;
}
