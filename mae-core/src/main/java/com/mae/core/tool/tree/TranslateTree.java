package com.mae.core.tool.tree;

import com.mae.core.utils.Assert;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 树结构数据转换处理
 * [{ id:'', pid:'' }]
 * @param <T>
 */
@Slf4j
public class TranslateTree<T> {
    private final List<TranslateTreeNode<T>> children = new ArrayList<>();
    private final int size;
    private Comparator<T> comparator = null;
    private Field idField;
    private Field parentIdField;

    public void setComparator(Comparator<T> comparator){
        this.comparator = comparator;
    }

    private void sort() {
        Assert.notNull(comparator, "the comparator is not be set");
        if (!children.isEmpty()) {
            children.sort((o1, o2) -> comparator.compare(o1.data, o2.data));
            children.forEach(item -> item.sort(comparator));
        }
    }

    public static class TranslateTreeNode<T> {
        private final List<TranslateTreeNode<T>> children = new ArrayList<>();
        private final T data;
        private String state = "normal";

        public TranslateTreeNode(T data) {
            this.data = data;
        }

        public String getState() {
            return state;
        }

        public void setState(String pDataState, boolean deepWrite) {
            this.state = pDataState;
            if (deepWrite) {
                this.children.forEach(item -> item.setState(pDataState, deepWrite));
            }
        }

        private void sort(Comparator<T> comparator) {
            if (!children.isEmpty()) {
                children.sort((o1, o2) -> comparator.compare(o1.data, o2.data));
                children.forEach(item -> item.sort(comparator));
            }
        }

        private void compareChildrenByCode(TranslateTreeNode<T> that, String codeFieldName) throws NoSuchFieldException {
            handlePDataState(this.children, that.children, codeFieldName);
        }
    }

    public TranslateTree(List<T> list, String idFieldName, String parentIdFieldName) throws NoSuchFieldException, IllegalAccessException {
        // 找到一层节点
        // 先遍历一次构造临时一层父子结构
        Map<String, TranslateTreeNode<T>> itemMap = new HashMap<>();
        for (T item : list) {
            TranslateTreeNode<T> node = new TranslateTreeNode<>(item);
            if (idField == null) {
                idField = item.getClass().getDeclaredField(idFieldName);
                idField.setAccessible(true);
            }
            String id = idField.get(item).toString();
            itemMap.put(id, node);
        }

        // 组装数据
        for (Map.Entry<String, TranslateTreeNode<T>> entry : itemMap.entrySet()) {
            TranslateTreeNode<T> value = entry.getValue();
            T data = value.data;
            if (parentIdField == null) {
                parentIdField = data.getClass().getDeclaredField(parentIdFieldName);
                parentIdField.setAccessible(true);
            }
            String parentId = parentIdField.get(data).toString();
            if (parentId == null || parentId.trim().length() == 0) {
                this.children.add(entry.getValue());
            } else {
                if (itemMap.containsKey(parentId)) {
                    itemMap.get(parentId).children.add(entry.getValue());
                } else {
                    log.warn("has not found parent id `{}` ", parentId);
                }
            }
        }
        this.sort();
        size = list.size();
    }

    public List<T> getList() {
        List<T> result = new ArrayList<>(size);
        this.children.forEach(item -> deepUnwrap(result, item));
        return result;
    }

    private void deepUnwrap(List<T> container, TranslateTreeNode<T> node) {
        container.add(node.data);
        node.children.forEach(item -> deepUnwrap(container, item));
    }

    public List<T> selectListByParentId(String fid) {
        List<T> result = new ArrayList<>(size);
        this.children.forEach(item -> {
            try {
                deepChild(result, item, fid);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        return result;
    }

    private void deepChild(List<T> container, TranslateTreeNode<T> node, String parentId) throws IllegalAccessException {
        if (parentId == null) {
            return;
        }
        // 如果找到了子，则将子作为父往下找
        if (parentId.equals(String.valueOf(parentIdField.get(node.data)))) {
            container.add(node.data);
            node.children.forEach(item -> {
                try {
                    deepChild(container, item, idField.get(node.data).toString());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        } else {
            node.children.forEach(item -> {
                try {
                    deepChild(container, item, parentId);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    /**
     * 树结构差异比较
     * 差异数据在一个表中,id不重复时使用
     * [{ id:1,parentId:,code:1},{id:2,parentId:1,code:2}]
     * [{ id:3,parentId:,code:1 },{id:4,parentId:3,code:2}]
     * @param tree 待比较的树
     */
    public void compareByCode(TranslateTree<T> tree, String codeFieldName) throws NoSuchFieldException {
        handlePDataState(this.children, tree.children, codeFieldName);
    }

    /**
     * 树结构差异比较
     * 差异数据不在一个列表中,id重复时使用
     * [{ id:1,parentId:},{id:2,parentId:1}]
     * [{ id:1,parentId:},{id:2,parentId:1}]
     * @param tree tree
     * @throws NoSuchFieldException e
     */
    public void compare(TranslateTree<T> tree) throws NoSuchFieldException {
        handlePDataState(this.children, tree.children, idField.getName());
    }

    private static <T> void handlePDataState(List<TranslateTreeNode<T>> thisChildren, List<TranslateTreeNode<T>> thatChildren, String codeFieldName) throws NoSuchFieldException {
        if (thisChildren.isEmpty() && thatChildren.isEmpty()) {
            return;
        }
        Field tempCodeField = null;
        if (!thisChildren.isEmpty()) {
            tempCodeField = thisChildren.get(0).data.getClass().getDeclaredField(codeFieldName);
        }
        if (tempCodeField == null) {
            tempCodeField = thatChildren.get(0).data.getClass().getDeclaredField(codeFieldName);
        }
        Assert.notNull(tempCodeField, "code field is not found");

        Field codeField = tempCodeField;
        // 构造两边的编码集合取并集
        Map<String, TranslateTreeNode<T>> thisChildrenCodeMap = thisChildren.stream().collect(Collectors.toMap(item -> {
            try {
                return String.valueOf(codeField.get(item.data));
            } catch (IllegalAccessException e) {
                throw new RuntimeException("this children code field get value error", e);
            }
        }, Function.identity()));
        Map<String, TranslateTreeNode<T>> thatChildrenCodeMap = thatChildren.stream().collect(Collectors.toMap(item -> {
            try {
                return String.valueOf(codeField.get(item.data));
            } catch (IllegalAccessException e) {
                throw new RuntimeException("that children code field get value error", e);
            }
        }, Function.identity()));
        Set<String> thisKeySet = thisChildrenCodeMap.keySet();
        Set<String> thatKeySet = thatChildrenCodeMap.keySet();
        Set<String> allSet = new HashSet<>(thisKeySet);
        allSet.addAll(thatKeySet);
        // 便利编码集合
        for (String childCode : allSet) {
            // 两边都存在时检查数据是否发生变化
            if (thisKeySet.contains(childCode) && thatKeySet.contains(childCode)) {
                TranslateTreeNode<T> thisNode = thisChildrenCodeMap.get(childCode);
                TranslateTreeNode<T> thatNode = thatChildrenCodeMap.get(childCode);
                thisNode.setState(thisNode.equals(thatNode) ? "normal" : "modify", false);
                // 两边都存在的节点，则进行子节点比较
                thisNode.compareChildrenByCode(thatNode, codeFieldName);
            } else if (thisKeySet.contains(childCode)) {
                // 如果存在当前节点中，不在比较的节点中，意味着是
                TranslateTreeNode<T> thisNode = thisChildrenCodeMap.get(childCode);
                thisNode.setState("new", true);
            } else {
                // 如果在比较的节点中不再当前节点中，则意味着时删除
                // 删除的话将比较节点并入当前树，标记删除状态
                TranslateTreeNode<T> thatNode = thatChildrenCodeMap.get(childCode);
                thatNode.setState("delete", true);
                thisChildren.add(thatNode);
            }
        }
    }
}
