package com.mae.core.service;

/**
 * 加解密服务
 */
public interface IEncryptService {
    /**
     * 加密
     *
     * @param text
     * @return
     */
    String encrypt(String text) throws Exception;

    /**
     * 解密
     *
     * @param encryptedText
     * @return
     */
    String decrypt(String encryptedText) throws Exception;
}
