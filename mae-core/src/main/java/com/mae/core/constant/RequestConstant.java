package com.mae.core.constant;

/**
 * 请求常量
 */
public class RequestConstant {
    /**
     * 调用链追踪ID
     */
    public static final String KEY_TRACE_ID = "X-Mae-Trace-Id";


}
