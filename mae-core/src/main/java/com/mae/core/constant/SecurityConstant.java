package com.mae.core.constant;

public interface SecurityConstant {
    /**
     * 匿名的，未经过认证的
     */
    String ANONYMOUS = "anonymous";
}
