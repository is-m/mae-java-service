package com.mae.core;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReturnMessage {
    private int status;
    private String msg;

    public ReturnMessage(String msg) {
        this.msg = msg;
    }

    public ReturnMessage(int status, String msg) {
        this.msg = msg;
    }

    public static ReturnMessage ok(String message) {
        return new ReturnMessage(message);
    }

    public static ReturnMessage ok() {
        return ok("success");
    }

    public static ReturnMessage error(String message) {
        return new ReturnMessage(1, message);
    }
}
