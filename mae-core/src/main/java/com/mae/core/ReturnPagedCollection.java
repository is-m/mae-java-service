package com.mae.core;

import com.mae.core.utils.collection.CollectionUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ReturnPagedCollection<T> {
    private List<T> content;
    private ReturnPagedMeta meta;

    public static <T> ReturnPagedCollection<T> of(PagedResult<T> pagedResult) {
        ReturnPagedCollection<T> result = new ReturnPagedCollection<>();
        if(pagedResult != null) {
            result.setContent(pagedResult.getContent());
            result.setMeta(ReturnPagedMeta.ofPageVO(pagedResult.getPage()));
        }
        return result;
    }

    public static <T> ReturnPagedCollection<T> of(ReturnPagedMeta meta, List<T> content){
        return new ReturnPagedCollection<>(){{
            setMeta(meta);
            setContent(content);
        }};
    }

    public List<List<T>> partitionContent(int size){
        return CollectionUtils.partition(content, size);
    }
}
