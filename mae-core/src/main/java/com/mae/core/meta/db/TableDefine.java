package com.mae.core.meta.db;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 表
 */
@Getter
@Setter
public class TableDefine {
    /**
     * 表名称,例如 t_xx
     */
    private String name;

    /**
     * 表说明
     */
    private String comment;

    /**
     * 列定义
     */
    private List<ColumnDefine> columns;

    /**
     * 索引定义
     */
    private List<IndexDefine> indexes;
}
