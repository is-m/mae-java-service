package com.mae.core.meta.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ColumnDefine {
    /**
     * 列名称
     */
    private String name;

    /**
     * 列说明
     */
    private String comment;

    /**
     * 列类型 char, varchar, int, date, datetime,
     */
    private String type;

    private Integer length;

    /**
     * 保留小数位
     */
    private Integer scale;


}
