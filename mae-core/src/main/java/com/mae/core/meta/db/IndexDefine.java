package com.mae.core.meta.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class IndexDefine {
    /**
     * 主键
     */
    public static final String TYPE_PRIMARY = "PRIMARY KEY";
    /**
     * 唯一索引
     */
    public static final String TYPE_UNIQUE = "UNIQUE";
    /**
     *
     */
    public static final String TYPE_KEY = "KEY";
    public static final String TYPE_FULLTEXT = "FULLTEXT";

    private String name;
    private String type;
    private List<String> columnNames;

    public static IndexDefine primary(String column){
        IndexDefine indexDefine = new IndexDefine();
        indexDefine.setType(TYPE_PRIMARY);
        indexDefine.setName("PK");
        List<String> columnNames = new ArrayList<>();
        columnNames.add(column);
        indexDefine.setColumnNames(columnNames);
        return indexDefine;
    }
}
