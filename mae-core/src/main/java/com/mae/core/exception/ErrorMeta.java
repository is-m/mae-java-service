package com.mae.core.exception;

import java.util.HashMap;

public class ErrorMeta extends HashMap<String, Object> {

    public Object getSource() {
        return this.get("source");
    }

    public void setSource(Object source) {
        this.put("source", source);
    }
}
