package com.mae.core.exception;

import com.mae.core.utils.StringUtils;
import lombok.Getter;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

public class MaeException extends RuntimeException {
    @Getter
    private final List<Error> errors;

    public MaeException(String message) {
        // super(message);
        Error error = new Error(400, "Server Internal Exception", message, null);
        this.errors = Collections.singletonList(error);
    }

    public MaeException(String message, Object... args) {
        // super(message);
        if(args == null || args.length == 0){
            Error error = new Error(400, "Server Internal Exception", message, null);
            this.errors = Collections.singletonList(error);
        } else{
            String actualMessage;
            if(StringUtils.contains(message, "%")){
                actualMessage = String.format(message, args);
            } else if(StringUtils.contains(message, "{")){
                actualMessage = MessageFormat.format(message, args);
            } else {
                actualMessage = message;
            }
            Error error = new Error(400, "Server Internal Exception", actualMessage, null);
            this.errors = Collections.singletonList(error);
        }
    }

    public MaeException(String message, Throwable exception) {
        super(message, exception);
        Error error = new Error(400, "Server Internal Exception", message, null);
        this.errors = Collections.singletonList(error);
    }

    public MaeException(List<Error> errors) {
        // super(errors.stream().map(Error::getDetail).collect(Collectors.joining(";")));
        this.errors = errors;
    }

    public MaeException(int status, String message) {
        // super(message);
        Error error = new Error(status, "Server Internal Exception", message, null);
        this.errors = Collections.singletonList(error);
    }

    public int getTopStatus() {
        return errors.stream().map(Error::getStatus).sorted().findFirst().orElse(400);
    }
}
