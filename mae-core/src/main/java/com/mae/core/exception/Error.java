package com.mae.core.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {
    private int status;
    private String title;
    private String detail;
    private ErrorMeta meta;

    public Error(int status, String title, String detail, ErrorMeta meta) {
        this.status = status;
        this.title = title;
        this.detail = detail;
        this.meta = meta;
    }
}
