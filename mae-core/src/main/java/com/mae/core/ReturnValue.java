package com.mae.core;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class ReturnValue<T> {
    private T content;
    private ReturnMeta meta;

    public ReturnValue() {
        meta = new ReturnMeta();
    }

    public static <T> ReturnValue<T> of(T content) {
        ReturnValue<T> result = new ReturnValue<>();
        result.setContent(content);
        return result;
    }

    public boolean equalsContent(T equalsValue){
        return Objects.equals(content, equalsValue);
    }
}
