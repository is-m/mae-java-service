package com.mae.core.principle;

import java.util.Date;

/**
 * 可能会过期的
 */
public interface IExpire {
    /**
     * 过期时间
     *
     * @return null 为永不过期，或者具体过期日期
     */
    default Date getExpireDate() {
        return null;
    }
}
