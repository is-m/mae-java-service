package com.mae.core.principle;

/**
 *
 */
public interface IThrowable {
    /**
     * 是否抛出异常
     *
     * @return false 不抛出异常但会进行日志记录， true 抛出异常，抛出异常时会终止后续的任务执行（例如事件监听时如果抛出异常则会阻断后续的事件监听）
     */
    default boolean exception(){
        return false;
    }
}
