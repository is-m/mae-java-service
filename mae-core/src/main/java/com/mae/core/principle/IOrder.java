package com.mae.core.principle;

/**
 * 排序
 */
public interface IOrder {
    /**
     * 升序（小->大，旧->新）
     */
    String ASC = "acs";
    /**
     * 降序（大->小，新->旧）
     */
    String DESC = "desc";

    /**
     * 优先级
     *
     * @return
     */
    default int priority(){
        return 100;
    }


    default String order(){
        return ASC;
    }
}
