package com.mae.core.principle;

import com.mae.core.constant.SecurityConstant;

/**
 * 租户
 */
public interface ITenant {
    default String getTenantId() {
        return SecurityConstant.ANONYMOUS;
    }
}
