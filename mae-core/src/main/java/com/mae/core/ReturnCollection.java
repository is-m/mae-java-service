package com.mae.core;

import com.mae.core.utils.collection.CollectionUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ReturnCollection<T> {
    private List<T> content;
    private ReturnMeta meta;

    public ReturnCollection(List<T> content) {
        this.content = content;
        meta = new ReturnMeta();
        meta.set("totalElements", content == null ? 0 : content.size());
    }

    public static <T> ReturnCollection<T> of(List<T> content){
        return new ReturnCollection<>(content);
    }

    public List<List<T>> partitionContent(int size){
        return CollectionUtils.partition(content, size);
    }
}
