package com.mae.core;

import com.mae.core.exception.Error;
import lombok.Getter;

import java.util.Collections;
import java.util.List;


public class ReturnErrors {
    public static final ReturnErrors SINGLETON_INTERNAL_ERROR =
            new ReturnErrors(Collections.singletonList(new Error(500, "Server Interal Exception", "服务器内部异常", null)));

    @Getter
    private final List<Error> errors;

    public ReturnErrors(List<Error> errors) {
        this.errors = errors;
    }

}
