package com.mae.core;

import java.util.HashMap;

public class ReturnMeta extends HashMap<String, Object> {

    protected void set(String key, Object value) {
        super.put(key, value);
    }

    protected <T> T get(String key) {
        return (T) super.get(key);
    }
}
