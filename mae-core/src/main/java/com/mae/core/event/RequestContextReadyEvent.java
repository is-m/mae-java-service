package com.mae.core.event;

import com.mae.core.context.RequestContext;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RequestContextReadyEvent {
    private Object sender;

    private RequestContext requestContext;
}
