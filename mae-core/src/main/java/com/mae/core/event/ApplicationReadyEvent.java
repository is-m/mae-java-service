package com.mae.core.event;

import com.mae.core.context.ApplicationContext;
import com.mae.core.principle.IOrder;
import com.mae.core.principle.IThrowable;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用初始化完成事件
 */
@Getter
@Setter
public class ApplicationReadyEvent {

    private Object sender;

    private ApplicationContext applicationContext;

}
