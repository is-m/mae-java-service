package com.mae.core.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用停止事件
 */
@Getter
@Setter
@AllArgsConstructor
public class ApplicationStoppedEvent {
    private Object sender;
}
