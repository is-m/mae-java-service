package com.mae.core.event;

import com.mae.core.spi.Environment;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 应用启动前
 */
@Getter
@AllArgsConstructor
public class ApplicationBeforeStartEvent {
    /**
     * 触发者
     */
    private Object sender;

    /**
     * 应用环境
     */
    private Environment environment;
}
