package com.mae.core.entity;

import com.mae.core.context.RequestContext;
import com.mae.core.context.UserPrincipal;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public abstract class BaseEntity {
    /**
     * 数据标识
     */
    private String id;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 最后修改人
     */
    private String updateBy;
    /**
     * 最后修改时间
     */
    private Date updatedTime;

    public void initForCreate() {
        if (id == null || id.trim().length() == 0) {
            id = UUID.randomUUID().toString();
        }
        UserPrincipal user = RequestContext.getCurrent().getUser();
        Date now = new Date();
        this.createBy = user.getId();
        this.createdTime = now;
        this.updateBy = user.getId();
        this.updatedTime = now;
    }

    public void initForUpdate() {
        UserPrincipal user = RequestContext.getCurrent().getUser();
        this.updateBy = user.getId();
        this.updatedTime = new Date();
    }
}
