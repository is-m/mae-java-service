package com.mae.core.entity;

import com.mae.core.context.RequestContext;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class TenantEntity extends BaseEntity {
    private String tenantId;

    @Override
    public void initForCreate() {
        super.initForCreate();
        this.tenantId = RequestContext.getCurrent().getUser().getTenantId();
    }
}
