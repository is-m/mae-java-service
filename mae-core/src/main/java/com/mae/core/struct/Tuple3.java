package com.mae.core.struct;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * 3元组
 *
 * @param <K>
 * @param <T>
 * @param <V>
 */
public class Tuple3<K, T, V> extends Tuple {
    private final Set<Tuple3Entry<K, T, V>> data = new LinkedHashSet<>();

    public boolean add(K k, T t, V v) {
        return data.add(new Tuple3Entry<>(k, t, v));
    }

    public Tuple3Entry<K, T, V> getByK(K k) {
        for (Tuple3Entry<K, T, V> entry : data) {
            if (Objects.equals(entry.getK(), k)) {
                return entry;
            }
        }
        return null;
    }

    public Tuple3Entry<K, T, V> getByT(T t) {
        for (Tuple3Entry<K, T, V> entry : data) {
            if (Objects.equals(entry.getT(), t)) {
                return entry;
            }
        }
        return null;
    }

    public Tuple3Entry<K, T, V> getByV(V v) {
        for (Tuple3Entry<K, T, V> entry : data) {
            if (Objects.equals(entry.getV(), v)) {
                return entry;
            }
        }
        return null;
    }

    public List<Tuple3Entry<K, T, V>> entries() {
        return List.copyOf(this.data);
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Tuple3Entry<K, T, V> {
        private K k;
        private T t;
        private V v;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Tuple3Entry<?, ?, ?> entry = (Tuple3Entry<?, ?, ?>) o;
            return Objects.equals(k, entry.k) && Objects.equals(t, entry.t) && Objects.equals(v, entry.v);
        }

        @Override
        public int hashCode() {
            return Objects.hash(k, t, v);
        }
    }
}
