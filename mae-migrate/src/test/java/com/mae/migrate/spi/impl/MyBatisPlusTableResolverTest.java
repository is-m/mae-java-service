package com.mae.migrate.spi.impl;

import com.mae.core.meta.db.TableDefine;
import org.junit.jupiter.api.Test;

public class MyBatisPlusTableResolverTest {

    private MyBaitsPlusTableResolver myBaitsPlusTableResolver = new MyBaitsPlusTableResolver();
    private DefaultTableOperator defaultTableOperator = new DefaultTableOperator();

    @Test
    public void testResolve() {
        TableDefine tab = myBaitsPlusTableResolver.resolve(MyBatisEntityClass.class);
        String sql = defaultTableOperator.generateDdlCreateSql(tab);
        System.out.println("sql " + sql);
    }
}
