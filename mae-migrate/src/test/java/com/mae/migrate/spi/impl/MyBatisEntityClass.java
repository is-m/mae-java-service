package com.mae.migrate.spi.impl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("mybatis_test_table")
public class MyBatisEntityClass {
    @TableId(type=IdType.AUTO)
    //@TableField(value="id1")
    private String id1;

    private String name;

    @TableField("update_date")
    private Date updateDate;
}
