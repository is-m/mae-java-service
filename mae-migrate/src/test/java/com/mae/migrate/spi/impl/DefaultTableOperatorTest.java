package com.mae.migrate.spi.impl;

import com.mae.core.meta.db.ColumnDefine;
import com.mae.core.meta.db.IndexDefine;
import com.mae.core.meta.db.TableDefine;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
public class DefaultTableOperatorTest {

    private DefaultTableOperator tableOperator = new DefaultTableOperator();

    @Test
    public void testGenerateDdlCreateSql(){
        ColumnDefine idColumn = new ColumnDefine();
        idColumn.setName("id");
        idColumn.setType("VARCHAR");
        idColumn.setLength(64);
        idColumn.setComment("主键");

        ColumnDefine nameColumn = new ColumnDefine();
        nameColumn.setName("name");
        nameColumn.setType("VARCHAR");
        nameColumn.setLength(64);
        nameColumn.setComment("名称");

        ColumnDefine ageColumn = new ColumnDefine();
        ageColumn.setName("age");
        ageColumn.setType("INT");
        ageColumn.setLength(10);
        ageColumn.setComment("年龄");

        ColumnDefine birthdayColumn = new ColumnDefine();
        birthdayColumn.setName("birthday");
        birthdayColumn.setType("DATETIME");
        birthdayColumn.setComment("生日");

        IndexDefine idIndex = new IndexDefine();
        idIndex.setName("PK");
        idIndex.setType("primary key");
        idIndex.setColumnNames(Arrays.asList("id","name"));
        List<ColumnDefine> columns = Arrays.asList(idColumn,nameColumn,ageColumn,birthdayColumn);
        TableDefine tableDefine = new TableDefine();
        tableDefine.setName("test_table");
        tableDefine.setComment("this a test table");
        tableDefine.setColumns(columns);
        tableDefine.setIndexes(Arrays.asList(idIndex));


        String sql = tableOperator.generateDdlCreateSql(tableDefine);
        log.info("sql is {}", sql);
    }
}
