package com.mae.migrate.repository.impl;

import com.mae.core.utils.*;
import com.mae.migrate.config.MigrateProperties;
import com.mae.migrate.constant.MigrateConstant;
import com.mae.migrate.entity.SchemaHistory;
import com.mae.migrate.repository.ISchemaHistoryRepository;
import com.mae.migrate.utils.MigrationUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcInsertOperations;

import javax.sql.DataSource;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * 数据库版本
 */
@Slf4j
public class SchemaHistoryJdbcTemplateRepository implements ISchemaHistoryRepository {
    private MigrateProperties migrateProperties;
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsertOperations insertOperations;
    private final String BASE_SELECT;

    public SchemaHistoryJdbcTemplateRepository(MigrateProperties migrateProperties, DataSource dataSource) {
        this.migrateProperties = migrateProperties;
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        BASE_SELECT = String.format("SELECT * FROM %s WHERE 1=1 ", migrateProperties.getSchemaTableName());
    }

    @Override
    public void initSchemaHistoryTable() {
        String schemaTableName = migrateProperties.getSchemaTableName();
        if (!StringUtils.hasText(schemaTableName)) {
            log.warn("the mae.db.migrate.schema-table-name is empty, use default name [{}] ", MigrateConstant.DEFAULT_SCHEMA_HISTORY);
            schemaTableName = MigrateConstant.DEFAULT_SCHEMA_HISTORY;
        }
        try {
            String createTableSql = MigrationUtils.generateCreateTableStatement(SchemaHistory.class, schemaTableName);
            jdbcTemplate.execute(createTableSql);
            log.info("schema history is already created.");
        } catch (Exception e) {
            if (ExceptionUtils.getRootMessage(e).endsWith(" already exists")) {
                log.info("schema history is already exists.");
                // 检查数据库版本是否存在错误记录，如果存在错误则不进行迁移操作
                List<SchemaHistory> failureList = jdbcTemplate.query("select * from " + migrateProperties.getSchemaTableName() + " where status = 'error'",
                        new BeanPropertyRowMapper<>(SchemaHistory.class));
                if (!failureList.isEmpty()) {
                    log.warn("failure sql file size {}", failureList.size());
                    for (SchemaHistory item : failureList) {
                        log.warn("failure sql file {}", item.getSource());
                        log.warn("failure message {}", item.getMessage());
                    }
                    throw new IllegalStateException("The schema last processing was unsuccessful !!!");
                }
            } else {
                log.error("schema history table initialize failure", e);
                throw new RuntimeException("init schema history table fail", e);
            }
        }
    }

    @Override
    public void saveAll(List<SchemaHistory> schemaHistories) {
        log.info("writing database migration...");
        SimpleJdbcInsertOperations insertOperations = new SimpleJdbcInsert(jdbcTemplate);
        insertOperations.withTableName(migrateProperties.getSchemaTableName());
        int executeCount = 0;
        for (SchemaHistory item : schemaHistories) {
            String source = item.getSource();
            SchemaHistory existsHistory = selectOne(" and source = ? ", source);
            // 如果已经存在则检查状态和内容MD5是否有变化
            if (existsHistory != null) {
                // hash 编码不一致
                item.setHash(HashUtils.hash(item.getScriptContent(), HashAlgorithm.MD5));
                if (!Objects.equals(item.getHash(), existsHistory.getHash())) {
                    if (migrateProperties.isErrorTermination()) {
                        // 已经迁移的脚本不允许再更新，需要新的脚本来实现增量处理
                        throw new IllegalStateException("the source " + item.getSource() + " file does not allow updates.");
                    } else {
                        log.warn("the source {} modified, but file does not allow updates. ", item.getSource());
                    }
                } else {
                    log.debug("the source {} is unmodified.", item.getSource());
                }
                continue;
            }

            item.setId(UUID.randomUUID().toString());
            // 执行脚本并进行记录
            try {
                jdbcTemplate.execute(item.getScriptContent());
                item.setStatus("ok");
                insertOperations.execute(BeanUtils.toMap(item));
                executeCount++;
            } catch (Exception e) {
                item.setStatus("fail");
                item.setMessage(ExceptionUtils.getRootMessage(e));
                insertOperations.execute(BeanUtils.toMap(item));
                if (migrateProperties.isErrorTermination()) {
                    throw new IllegalStateException("the source " + item.getSource() + " file does not allow updates.");
                } else {
                    log.warn("the source {} contentScript execute fail", item.getSource(), e);
                }
            }
        }
    }

    private SchemaHistory selectOne(String conditionSql, Object... args) {
        List<SchemaHistory> schemaHistories = selectList(conditionSql, args);
        if (schemaHistories.isEmpty()) {
            return null;
        }
        if (schemaHistories.size() > 1) {
            log.warn("condition sql matched data length > 1");
        }
        return schemaHistories.get(0);
    }

    private List<SchemaHistory> selectList(String conditionSql, Object... args) {
        return jdbcTemplate.query(BASE_SELECT + conditionSql, new BeanPropertyRowMapper<>(SchemaHistory.class), args);
    }
}
