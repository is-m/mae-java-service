package com.mae.migrate.repository;

import com.mae.migrate.entity.SchemaHistory;

import java.util.List;

public interface ISchemaHistoryRepository {

    void initSchemaHistoryTable();

    void saveAll(List<SchemaHistory> schemaHistories);
}
