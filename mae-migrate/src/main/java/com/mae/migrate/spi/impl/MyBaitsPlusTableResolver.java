package com.mae.migrate.spi.impl;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.MybatisMapperBuilderAssistant;
import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.mae.core.annotations.orm.Index;
import com.mae.core.meta.db.ColumnDefine;
import com.mae.core.meta.db.IndexDefine;
import com.mae.core.meta.db.TableDefine;
import com.mae.core.utils.Assert;
import com.mae.core.utils.StringUtils;
import com.mae.migrate.spi.TableResolver;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Size;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.*;

@Component
@Slf4j
public class MyBaitsPlusTableResolver implements TableResolver {

    @Override
    public TableDefine resolve(Class<?> clz) {
        TableInfo tableInfo = TableInfoHelper.getTableInfo(clz);
        if (tableInfo == null) {
            log.error("mybatis plus table info not found by Class {} try initTableInfo", clz.getSimpleName());
            MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
            TableInfoHelper.initTableInfo(new MybatisMapperBuilderAssistant(mybatisConfiguration, ""), clz);
            tableInfo = TableInfoHelper.getTableInfo(clz);
        }
        List<TableFieldInfo> fieldList = tableInfo.getFieldList();
        List<ColumnDefine> columns = new ArrayList<>(fieldList.size());
        Map<String, IndexDefine> indexDefineMap = new HashMap<>();
        if(tableInfo.havePK()) {
            ColumnDefine keyColumn = new ColumnDefine();
            keyColumn.setName(tableInfo.getKeyColumn());
            keyColumn.setComment("主键");
            indexDefineMap.put("PK", IndexDefine.primary(tableInfo.getKeyColumn()));
            String typeName = tableInfo.getKeyType().getTypeName();
            switch (typeName){
                case "java.lang.String":
                    keyColumn.setType("VARCHAR");
                    keyColumn.setLength(256);
                    break;
                case "java.lang.Integer":
                case "java.lang.Long":
                    keyColumn.setType("BIGINT");
                    break;
                default:
                    throw new IllegalArgumentException("primary key type is not implement");
            }

            columns.add(keyColumn);
        } else {
            log.warn("the entity class not have primary key properties");
        }
        for (TableFieldInfo tableFieldInfo : fieldList) {
            String col = tableFieldInfo.getColumn();
            Field field = tableFieldInfo.getField();
            ColumnDefine column = new ColumnDefine();
            column.setName(col);
            this.fillJdbcType(column, tableFieldInfo);


            // 获取索引注解
            Index indexAnnotation = field.getAnnotation(Index.class);
            if (indexAnnotation != null) {
                String indexName = StringUtils.nvl(indexAnnotation.value(), "i_" + col);
                String indexType = indexAnnotation.type().toString();
                IndexDefine indexDefine = null;
                if (!indexDefineMap.containsKey(indexName)) {
                    indexDefine = new IndexDefine();
                    indexDefine.setName(indexName);
                    indexDefine.setType(indexType);
                    indexDefine.setColumnNames(new ArrayList<>());
                } else {
                    indexDefine = indexDefineMap.get(indexName);
                    Assert.isEquals(indexDefine.getType(), indexType, "实体类{0}索引{1}存在多种索引类型", clz.getSimpleName(), indexName);
                }
                indexDefine.getColumnNames().add(col);
            }
            columns.add(column);
        }

        TableDefine table = new TableDefine();
        table.setName(tableInfo.getTableName());
        table.setColumns(columns);
        table.setComment("这个一个测试表格");
        table.setIndexes(new ArrayList<>(indexDefineMap.values()));
        return table;
    }

    @Override
    public boolean canResolve(Class<?> clz) {
        return clz.getAnnotation(TableName.class) != null;
    }

    private void fillJdbcType(ColumnDefine columnDefine, TableFieldInfo fieldInfo) {
        String javaType = fieldInfo.getPropertyType().getTypeName();
        switch (javaType) {
            case "java.lang.String":
                int strLen = Optional.ofNullable(fieldInfo.getField().getAnnotation(Size.class)).map(Size::max).orElse(50);
                if (strLen <= 4000) {
                    columnDefine.setType("VARCHAR");
                    columnDefine.setLength(strLen);
                } else if (strLen <= 65535) {
                    columnDefine.setType("TEXT");
                } else {
                    columnDefine.setType("LONGTEXT");
                }
                break;
            case "java.util.Date":
                columnDefine.setType("DATETIME");
                break;
            case "java.lang.Integer":
                columnDefine.setType("INT");
                break;
            case "java.lang.Long":
                columnDefine.setType("BIGINT");
                break;
            case "java.lang.Double":
                long longMax = Optional.ofNullable(fieldInfo.getField().getAnnotation(Max.class)).map(Max::value).orElse(Long.MAX_VALUE);
                columnDefine.setType("DECIMAL");
                columnDefine.setLength(String.valueOf(longMax).length());
                // TODO 未实现小数识别，暂时保留3位小数
                columnDefine.setScale(3);
                break;
            case "java.lang.Boolean":
                columnDefine.setName("BIT");
                break;
            default:
                throw new IllegalArgumentException("cannot support java type "+javaType);
        }
    }
}
