package com.mae.migrate.spi.impl;

import com.mae.core.meta.db.ColumnDefine;
import com.mae.core.meta.db.IndexDefine;
import com.mae.core.meta.db.TableDefine;
import com.mae.migrate.spi.TableOperator;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.table.Index;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DefaultTableOperator implements TableOperator {
    @Override
    public String generateDdlCreateSql(TableDefine tableDefine) {
        Table tableTest = new Table(tableDefine.getName());

        List<ColumnDefinition> columnDefinitions = new ArrayList<>();
        for(ColumnDefine columnDefine : tableDefine.getColumns()){
            ColDataType colDataType = new ColDataType(columnDefine.getType(), Optional.ofNullable(columnDefine.getLength()).orElse(-1),-1);
            ColumnDefinition columnDefinition = new ColumnDefinition(columnDefine.getName(), colDataType);
            columnDefinitions.add(columnDefinition);
        }
        List<Index> indexes = new ArrayList<>();
        for(IndexDefine indexDefine : tableDefine.getIndexes()){
            Index index = new Index();
            index.withName(indexDefine.getName()).withColumnsNames(indexDefine.getColumnNames()).withType(indexDefine.getType());
            indexes.add(index);
        }
        CreateTable createTable = new CreateTable();
        createTable.withTable(tableTest).withColumnDefinitions(columnDefinitions).withIndexes(indexes);
        return createTable.toString();
    }

}
