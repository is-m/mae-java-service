package com.mae.migrate.spi;

import com.mae.core.meta.db.TableDefine;

/**
 *
 */
public interface TableResolver {

    /**
     * 处理
     *
     * @param clz
     * @return
     */
    TableDefine resolve(Class<?> clz);

    /**
     * 能否处理
     *
     * @param clz
     * @return
     */
    boolean canResolve(Class<?> clz);
}
