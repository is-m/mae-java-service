package com.mae.migrate.spi.impl;

import com.mae.core.entity.BaseEntity;
import com.mae.core.utils.ReflectUtils;
import com.mae.migrate.spi.EntityConditional;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * 默认识别实体类的方式，如果对象包含id字段，则认为是个实体类
 */
public class DefaultEntityConditional implements EntityConditional {

    /**
     * 识别是表的注解
     */
    private final List<String> TABLE_ANNOTATION_NAMES = Arrays.asList(
            "javax.persistence.Entity",
            "javax.persistence.Table",
            "com.baomidou.mybatisplus.annotation.TableName",
            "com.mae.core.annotations.Table");

    private final List<String> ENTITY_CLASS_NAMES = Arrays.asList("com.mae.core.entity.BaseEntity", "com.mae.core.entity.TenantEntity");

    @Override
    public boolean match(Class<?> clz) {
        Annotation[] annotations = clz.getAnnotations();
        for (Annotation annotation : annotations) {
            if (TABLE_ANNOTATION_NAMES.contains(annotation.getClass().getName())
            || TABLE_ANNOTATION_NAMES.contains(annotation.annotationType().getName())) {
                return true;
            }
        }
        if (!ENTITY_CLASS_NAMES.contains(clz.getName())) {
            while (clz.getSuperclass() != null) {
                if (ENTITY_CLASS_NAMES.contains(clz.getSuperclass().getName())) {
                    return true;
                }
                clz = clz.getSuperclass();
            }
        }
        return false;
    }
}
