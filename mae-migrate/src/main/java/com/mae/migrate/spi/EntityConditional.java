package com.mae.migrate.spi;

import java.lang.reflect.Field;

public interface EntityConditional {

    /**
     * 是否
     * @param clz
     * @return
     */
    boolean match(Class<?> clz);

}
