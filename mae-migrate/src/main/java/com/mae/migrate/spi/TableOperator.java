package com.mae.migrate.spi;

import com.mae.core.meta.db.TableDefine;

public interface TableOperator {

    String generateDdlCreateSql(TableDefine tableDefine);

}
