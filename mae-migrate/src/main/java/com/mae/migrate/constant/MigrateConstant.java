package com.mae.migrate.constant;

/**
 * 配置常量
 */
public class MigrateConstant {
    /**
     * 配置前缀
     */
    public static final String CONFIG_PREFIX = "mae.db.migrate";
    /**
     * 默认档案表
     */
    public static String DEFAULT_SCHEMA_HISTORY = "mae_schema_history";
}
