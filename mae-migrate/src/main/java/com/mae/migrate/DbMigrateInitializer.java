package com.mae.migrate;

import com.mae.migrate.config.MigrateProperties;
import com.mae.migrate.service.IEntityClassSqlHandler;
import com.mae.migrate.utils.MigrationUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.sql.DataSource;

/**
 * 数据迁移初始化器
 */
@Slf4j
public class DbMigrateInitializer implements InitializingBean {

    private final DataSource dataSource;

    private MigrateProperties migrateProperties;

    private IEntityClassSqlHandler entityClassSqlHandler;

    public DbMigrateInitializer(DataSource dataSource, MigrateProperties migrateProperties,IEntityClassSqlHandler entityClassSqlHandler) {
        this.dataSource = dataSource;
        this.migrateProperties = migrateProperties;
        this.entityClassSqlHandler = entityClassSqlHandler;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("db migration starting.");
        MigrationUtils.migrate(dataSource, migrateProperties, entityClassSqlHandler);
    }
}
