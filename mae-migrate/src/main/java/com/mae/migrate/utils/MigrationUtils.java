package com.mae.migrate.utils;

import com.mae.core.utils.ReflectUtils;
import com.mae.core.utils.StringUtils;
import com.mae.migrate.config.MigrateProperties;
import com.mae.migrate.service.IEntityClassSqlHandler;
import com.mae.migrate.service.IMigrateService;
import com.mae.migrate.service.ISchemaHistoryService;
import com.mae.migrate.service.impl.EntityMigrateService;
import com.mae.migrate.service.impl.SchemaHistoryService;
import com.mae.migrate.service.impl.SqlMigrateService;
import jakarta.validation.constraints.Size;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * 迁移工具类
 */
@Slf4j
public class MigrationUtils {
    /**
     * 迁移
     *
     * @param dataSource        数据源
     * @param migrateProperties 迁移配置
     */
    public static void migrate(DataSource dataSource, MigrateProperties migrateProperties, IEntityClassSqlHandler entityClassSqlHandler) {
        if (!migrateProperties.isEnabled()) {
            log.info("db migration is disabled. use [mae.db.migrate.enable=true] to enable");
            return;
        }

        String types = migrateProperties.getTypes();
        ISchemaHistoryService schemaHistoryService = new SchemaHistoryService(migrateProperties, dataSource);
        List<IMigrateService> migrateServices = new ArrayList<>();
        if (types.contains("sql")) {
            migrateServices.add(new SqlMigrateService(migrateProperties, schemaHistoryService));
        }

        if (types.contains("entity")) {
            migrateServices.add(new EntityMigrateService(migrateProperties, schemaHistoryService, entityClassSqlHandler));
        }

        for (IMigrateService migrateService : migrateServices) {
            migrateService.init();
        }
    }

    public static String getTableName(Class<?> clz){
        // FIXME: jpa or other table annotation
        return clz.getSimpleName();
    }

    /**
     * 根据类生成建表语句 DDL
     *
     * @param clz 实体类
     * @param name 表名
     * @return 建表的sql
     */
    public static String generateCreateTableStatement(Class<?> clz, String name) {
        String tableName = StringUtils.hasText(name) ? name : clz.getSimpleName(); // 使用类名作为表名
        StringBuilder createTableStatement = new StringBuilder();

        createTableStatement.append("CREATE TABLE ").append(tableName).append(" (");

        Field[] fields = ReflectUtils.getAllFields(clz); // 获取类的字段
        // 字段排序
        List<Field> sortedFields = sortFields(fields);
        for (Field field : sortedFields) {
            String fieldName = field.getName();//field.getName().replaceAll("([A-Z])", "_$1").toLowerCase(); // 字段名
            Class<?> fieldType = field.getType(); // 字段类型

            createTableStatement.append("`").append(fieldName).append("`"); // 添加字段名
            createTableStatement.append(" ");

            // 根据字段类型转换为SQL类型
            String sqlType = fieldType.getSimpleName();
            Size sizeAnnotation = field.getAnnotation(Size.class);
            switch (sqlType) {
                case "int":
                    sqlType = "INTEGER";
                    break;
                case "String":
                    int length = sizeAnnotation == null ? 50 : sizeAnnotation.max();
                    if (length <= 4000) {
                        sqlType = "VARCHAR(" + length + ")";
                    } else if (length <= 65535) {
                        sqlType = "TEXT";
                    } else {
                        sqlType = "MEDIUMTEXT"; // 16,777,215字节（约16MB）
                        // LONGTEXT类型的存储容量为4,294,967,295字节（约4GB）
                    }
                    break;
                default:
                    break;
            }
            createTableStatement.append(sqlType); // 添加字段类型
            createTableStatement.append(",");
        }

        // 移除最后一个多余的逗号
        createTableStatement.deleteCharAt(createTableStatement.length() - 1);
        createTableStatement.append(");");
        return createTableStatement.toString();
    }

    private static List<Field> sortFields(Field[] fields) {
        List<Field> top = new ArrayList<>();
        List<Field> middle = new ArrayList<>();
        List<Field> last = new ArrayList<>();
        for (Field field : fields) {
            String fieldName = field.getName();
            switch (fieldName) {
                case "id" -> top.add(field);
                case "tenant_id", "status", "version", "sort", "deleted", "createBy", "createdTime", "updateBy", "updatedTime" -> last.add(field);
                default -> middle.add(field);
            }
        }
        List<Field> resultList = new ArrayList<>(fields.length);
        resultList.addAll(top);
        resultList.addAll(middle);
        resultList.addAll(last);
        return resultList;
    }

}
