package com.mae.migrate.utils;

import com.mae.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ResourceUtils {

    /**
     * 加载类
     *
     * @param packages
     * @return
     */
    public static List<Class<?>> loadClasses(List<String> packages) {
        List<Class<?>> classes = new ArrayList<>();
        List<String> classpathPatterns = toClasspathPattern2(packages);
        PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver = new PathMatchingResourcePatternResolver();
        CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        for (String classpathPattern : classpathPatterns) {
            try {
                Resource[] resources = pathMatchingResourcePatternResolver.getResources(classpathPattern);
                for (Resource resource : resources) {
                    if (resource.isReadable()) {
                        MetadataReader metadataReader = factory.getMetadataReader(resource);
                        String className = metadataReader.getClassMetadata().getClassName();
                        Class<?> clz = contextClassLoader.loadClass(className);
                        classes.add(clz);
                    }
                }
                return classes;
            } catch (ClassNotFoundException | IOException e) {
                throw new RuntimeException("get migrate sql file resources has error", e);
            }
        }
        return Collections.emptyList();
    }

    public static List<String> toClasspathPattern2(List<String> list) {
        return list.stream().map(item -> toClasspathPattern(item) + "/**/*.class").filter(StringUtils::hasText).toList();
    }


    public static List<String> toClasspathPattern(List<String> list) {
        return list.stream().map(ResourceUtils::toClasspathPattern).filter(StringUtils::hasText).toList();
    }

    private static String toClasspathPattern(String value) {
        if (StringUtils.isEmpty(value) || value.startsWith("classpath:") || value.startsWith("classpath*:")) {
            return value;
        }
        return "classpath*:" + value.replaceAll("[.\\\\]", "/");
    }


}
