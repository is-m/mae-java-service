package com.mae.migrate.entity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import jakarta.validation.groups.Default;
import lombok.Getter;
import lombok.Setter;

/**
 * 数据库脚本执行记录
 */
@Getter
@Setter
public class SchemaHistory {
    @Size(max = 64)
    private String id;

    /**
     * 名称
     */
    @Size(max = 255)
    private String name;

    /**
     * 版本
     */
    @Size(max = 50)
    private String version;

    /**
     * 执行的脚本
     */
    @Size(max = Integer.MAX_VALUE)
    private String scriptContent;

    /**
     * 脚本的哈希
     */
    @Size(max = 64)
    private String hash;

    /**
     * 来源
     * migrate 迁移脚本，
     * 实际存放值为：migrate:/xxx/xxx/db/migrate/mysql/V1.0.0.1_init.sql
     * entity 实体类
     * 实际存放值为：entity:com.xx.xx.UserInfo
     */
    @Size(max = 500)
    private String source;

    /**
     * 状态，ok 成功，fail 失败
     */
    @Size(max = 20)
    private String status;

    /**
     * 消息
     */
    @Size(max = 4000)
    private String message;
}
