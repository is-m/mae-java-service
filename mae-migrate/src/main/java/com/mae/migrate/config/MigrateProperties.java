package com.mae.migrate.config;

import com.mae.core.utils.StringUtils;
import com.mae.migrate.constant.MigrateConstant;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 数据迁移配置
 * 如果同时存在数据脚本和实体类迁移的情况，优先执行脚本，后执行实体类
 */
@Getter
@Setter
@ConfigurationProperties(prefix = MigrateConstant.CONFIG_PREFIX)
public class MigrateProperties {
    /**
     * 是否启用迁移
     * mae.db.migrate.enabled
     */
    private boolean enabled = true;

    /**
     * 数据库方言
     */
    private String dialect = "mysql";

    /**
     * 支持迁移类型
     */
    private String types = "sql,entity";

    /**
     * 实体类所在包路径，不填写时默认全量扫描，多个逗号分隔
     * mae.db.migrate.base-packages
     */
    private String packages;

    /**
     * 实体类识别接口实现类
     * com.mae.migrate.spi.EntityConditional
     */
    private String entityConditional = "com.mae.migrate.spi.impl.DefaultEntityConditional";

    /**
     * sql迁移路径，多个逗号分隔
     */
    private String locations = "classpath*:db/migration/";

    /**
     * 表名称
     */
    private String schemaTableName = MigrateConstant.DEFAULT_SCHEMA_HISTORY;

    /**
     * 发现异常时终止程序允许
     */
    private boolean errorTermination = true;

    public String getTypes() {
        return types == null ? "" : types;
    }

    public String[] getResolvedLocations() {
        if (StringUtils.isEmpty(locations)) {
            return null;
        }
        String[] split = locations.split(",");
        String[] result = new String[split.length];
        for (int i = 0; i < split.length; i++) {
            String prefix = split[i];
            if (prefix.endsWith("/")) {
                result[i] = prefix + dialect + "/*.sql";
            } else {
                result[i] = prefix + "/" + dialect + "/*.sql";
            }
        }
        return result;
    }
}
