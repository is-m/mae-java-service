package com.mae.migrate.config;

import com.mae.migrate.DbMigrateInitializer;
import com.mae.migrate.service.IEntityClassSqlHandler;
import com.mae.migrate.service.impl.EntityClassSqlHandler;
import com.mae.migrate.spi.TableOperator;
import com.mae.migrate.spi.impl.DefaultTableOperator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Slf4j
@Configuration
@Import({MigrateProperties.class, DbMigrateInitializer.class})
public class DbMigrateAutoConfiguration {

    public DbMigrateAutoConfiguration(){
      log.info("component db migrate init");
    }

    @ConditionalOnMissingBean
    @Bean
    public TableOperator tableOperator(){
        return new DefaultTableOperator();
    }

    @ConditionalOnMissingBean
    @Bean
    public IEntityClassSqlHandler entityClassSqlHandler(TableOperator tableOperator){
        return new EntityClassSqlHandler(tableOperator);
    }
}
