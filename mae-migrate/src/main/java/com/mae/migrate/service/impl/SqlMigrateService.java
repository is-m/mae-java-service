package com.mae.migrate.service.impl;

import com.mae.core.utils.ClassUtils;
import com.mae.core.utils.HashAlgorithm;
import com.mae.core.utils.HashUtils;
import com.mae.core.utils.Mae;
import com.mae.migrate.config.MigrateProperties;
import com.mae.migrate.entity.SchemaHistory;
import com.mae.migrate.service.IMigrateService;
import com.mae.migrate.service.ISchemaHistoryService;
import com.mae.migrate.spi.EntityConditional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class SqlMigrateService implements IMigrateService {
    private MigrateProperties migrateProperties;
    private ISchemaHistoryService schemaHistoryService;


    public SqlMigrateService(MigrateProperties migrateProperties, ISchemaHistoryService schemaHistoryService) {
        this.migrateProperties = migrateProperties;
        this.schemaHistoryService = schemaHistoryService;
    }

    @Override
    public void init() {
        log.info("migration sql");
        // 找到所有系统内置脚本
        // 获取配置的路径
        String[] locations = migrateProperties.getResolvedLocations();
        if (locations == null) {
            log.warn("ignore sql migrate of empty locations");
            return;
        }

        log.info("sql migrate locations {}", locations);
        List<String> classpathItems = new ArrayList<>();
        List<String> filesystemItems = new ArrayList<>();
        for (String item : locations) {
            if (item.startsWith("file:")) {
                filesystemItems.add(item.substring(5));
            } else {
                // 没有标识符时按类路径匹配
                classpathItems.add(item);
            }
        }
        List<Resource> sqlResources = new ArrayList<>();
        PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver = new PathMatchingResourcePatternResolver();
        for (String classpathPattern : classpathItems) {
            try {
                Resource[] resources = pathMatchingResourcePatternResolver.getResources(classpathPattern);
                for (Resource resource : resources) {
                    String filename = resource.getFilename();
                    if (filename != null && filename.endsWith(".sql")) {
                        sqlResources.add(resource);
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException("get migrate sql file resources has error", e);
            }
        }

        if (!sqlResources.isEmpty()) {
            List<SchemaHistory> schemaHistories = new ArrayList<>(sqlResources.size());
            for (Resource resource : sqlResources) {
                log.info("migrate sql file {}", resource.getFilename());
                SchemaHistory schemaHistory = new SchemaHistory();
                try {
                    // 根据文件构造迁移归档对象
                    String contentAsString = resource.getContentAsString(StandardCharsets.UTF_8);
                    schemaHistory.setHash(HashUtils.hash(contentAsString, HashAlgorithm.MD5));
                    schemaHistory.setScriptContent(contentAsString);
                    schemaHistory.setSource("migrate:" + resource.getURI().getPath() + resource.getFilename());
                } catch (Exception e) {
                    throw new RuntimeException("migrate sql file has error", e);
                }
                schemaHistory.setName(resource.getFilename());
                schemaHistories.add(schemaHistory);
            }
            schemaHistoryService.saveAll(schemaHistories);
        } else {
            log.info("classpath sql file is empty");
        }
    }
}
