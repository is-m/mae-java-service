package com.mae.migrate.service.impl;

import com.mae.core.spi.Environment;
import com.mae.core.utils.Assert;
import com.mae.core.utils.ExceptionUtils;
import com.mae.core.utils.StringUtils;
import com.mae.migrate.config.MigrateProperties;
import com.mae.migrate.constant.MigrateConstant;
import com.mae.migrate.entity.SchemaHistory;
import com.mae.migrate.repository.ISchemaHistoryRepository;
import com.mae.migrate.repository.impl.SchemaHistoryJdbcTemplateRepository;
import com.mae.migrate.service.ISchemaHistoryService;
import com.mae.migrate.utils.MigrationUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcInsertOperations;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class SchemaHistoryService implements ISchemaHistoryService {
    private MigrateProperties migrateProperties;
    private ISchemaHistoryRepository schemaHistoryRepository;

    public SchemaHistoryService(MigrateProperties migrateProperties, DataSource dataSource) {
        this.migrateProperties = migrateProperties;
        this.schemaHistoryRepository = new SchemaHistoryJdbcTemplateRepository(migrateProperties, dataSource);
        this.schemaHistoryRepository.initSchemaHistoryTable();
    }

    @Override
    public void saveAll(List<SchemaHistory> schemaHistories) {
        if (CollectionUtils.isEmpty(schemaHistories)) {
            log.warn("empty schema history save");
            return;
        }

        schemaHistoryRepository.saveAll(schemaHistories);
    }

    @Override
    public SchemaHistory getBySource(String source) {
        Map<String, Object> params = new HashMap<>();
        params.put("source", source);
        String sql = "select * from " + migrateProperties.getSchemaTableName() + " where source = :source";
        return null;
    }

}
