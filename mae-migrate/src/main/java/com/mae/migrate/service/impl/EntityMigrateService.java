package com.mae.migrate.service.impl;

import com.mae.core.utils.Assert;
import com.mae.core.utils.ClassUtils;
import com.mae.core.utils.MessageUtils;
import com.mae.migrate.config.MigrateProperties;
import com.mae.migrate.constant.MigrateConstant;
import com.mae.migrate.entity.SchemaHistory;
import com.mae.migrate.service.IEntityClassSqlHandler;
import com.mae.migrate.service.IMigrateService;
import com.mae.migrate.service.ISchemaHistoryService;
import com.mae.migrate.spi.EntityConditional;
import com.mae.migrate.utils.MigrationUtils;
import com.mae.migrate.utils.ResourceUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Slf4j
public class EntityMigrateService implements IMigrateService {
    private MigrateProperties migrateProperties;
    private ISchemaHistoryService schemaHistoryService;
    private EntityConditional entityConditional;
    private IEntityClassSqlHandler entityClassSqlHandler;

    public EntityMigrateService(MigrateProperties migrateProperties, ISchemaHistoryService schemaHistoryService, IEntityClassSqlHandler entityClassSqlHandler) {
        this.migrateProperties = migrateProperties;
        this.schemaHistoryService = schemaHistoryService;
        this.entityConditional = ClassUtils.newInstance(migrateProperties.getEntityConditional());
        this.entityClassSqlHandler = entityClassSqlHandler;
    }

    @Override
    public void init() {
        log.info("migration entity");
        String basePackages = migrateProperties.getPackages();
        Assert.hasText(basePackages, MessageUtils.getAppConfigEmpty(MigrateConstant.CONFIG_PREFIX + ".packages"));
        List<String> packages = Arrays.asList(basePackages.split(","));
        log.info("load entity class for packages {}", packages);
        List<Class<?>> clzAll = ResourceUtils.loadClasses(packages);
        List<SchemaHistory> schemaHistories = clzAll.stream().map(entityClass -> {
            try {
                String sql = entityClassSqlHandler.generateDDLSql(entityClass);
                if(sql == null){
                    log.debug("class {} not handle ", entityClass.getSimpleName());
                    return null;
                }
                log.info("migrate entity class {}", entityClass);
                SchemaHistory schemaHistory = new SchemaHistory();
                schemaHistory.setScriptContent(sql);
                schemaHistory.setSource("entity:" + entityClass.getName());
                return schemaHistory;
            }catch (Exception e){
                log.warn("the class {} is not a entity class cause {}", entityClass.getSimpleName(),e.getMessage());
                return null;
            }
        }).filter(Objects::nonNull).toList();
        log.info("{} entity class founded", schemaHistories.size());
        schemaHistoryService.saveAll(schemaHistories);
    }

}
