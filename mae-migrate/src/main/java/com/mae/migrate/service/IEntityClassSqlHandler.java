package com.mae.migrate.service;

/**
 * 实体类脚本处理器
 */
public interface IEntityClassSqlHandler {
    /**
     * 生成 DDL SQL
     * @return DDL
     */
    String generateDDLSql(Class<?> clz);
}
