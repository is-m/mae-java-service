package com.mae.migrate.service.impl;

import com.mae.core.meta.db.TableDefine;
import com.mae.migrate.service.IEntityClassSqlHandler;
import com.mae.migrate.spi.TableOperator;
import com.mae.migrate.spi.TableResolver;
import com.mae.migrate.spi.impl.MyBaitsPlusTableResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
public class EntityClassSqlHandler implements IEntityClassSqlHandler {
    @Autowired
    private Map<String, TableResolver> tableResolverMap;
    private TableOperator tableOperator;

    public EntityClassSqlHandler(TableOperator tableOperator){
        this.tableOperator = tableOperator;
    }

    @Override
    public String generateDDLSql(Class<?> clz) {
        for(TableResolver tableResolver : tableResolverMap.values()){
            if(tableResolver.canResolve(clz)){
                log.debug("{} table resolver handle clz {}", tableResolver.getClass().getSimpleName(), clz.getSimpleName());
                TableDefine resolve = tableResolver.resolve(clz);
                return tableOperator.generateDdlCreateSql(resolve);
            }
        }
        return null;
    }
}
