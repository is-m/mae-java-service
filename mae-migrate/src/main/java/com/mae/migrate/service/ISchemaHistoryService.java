package com.mae.migrate.service;

import com.mae.migrate.entity.SchemaHistory;

import java.util.List;

/**
 * 数据
 */
public interface ISchemaHistoryService {

    void saveAll(List<SchemaHistory> schemaHistories);

    SchemaHistory getBySource(String source);
}
