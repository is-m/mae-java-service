package ${packageName}.service;

import ${packageName}.${modelName};
import ${packageName}.mapper.${mapperName};
import java.util.List;

public interface I${serviceName}{

    public List<${modelName}> getAll${modelName}s();
}