<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${packageName}.dao.${daoName}">

    <sql id="baseSelect">
        select
        <#list columns as column>
            t.`${column.columnName}` as ${column.propertyName?uncap_first} <#if x_has_next>,</#if>
        </#list>
        from ${tableName} t
    </sql>

    <resultMap id="BaseResultMap" type="${packageName}.${modelName}">
        <#list columns as column>
            <<#if column.primary??>id<#else>result</#if> column="${column.columnName}" property="${column.propertyName?uncap_first}" jdbcType="<#if column.type='INT'>INTEGER<#elseif column.type='DATETIME'>TIMESTAMP<#elseif column.type='TEXT'>VARCHAR<#else>${column.type}</#if>" />
        </#list>
    </resultMap>

    <select id="getAll${modelName}s" resultMap="BaseResultMap">
        select * from ${tableName}
    </select>

    <insert id="insert${modelName}">
        insert into ${tableName}(
        <#list columns as column>
        `${column.columnName}`<#if x_has_next>, </#if>
        </#list>
        )
        value (
        <#list columns as column>
            #{${column.propertyName?uncap_first},jdbcType=${column.mybatisJdbcType}}<#if x_has_next>, </#if>
        </#list>
        )
    </insert>

    <update id="update${modelName}">
        update ${tableName}
        <set>
        <#list columns as column>
            <#if column.columnName != 'ID'>
            <if test="${column.propertyName?uncap_first} != null">
            t.`${column.columnName}` as ${column.propertyName?uncap_first} <#if x_has_next>,</if>
            </if>
            </#if>
        </#list>
        </set>
        where id=#{id}
    </update>

    <delete id="deleteById">
        delete from ${tableName} where id = #{id}
    </delete>

    <select id="findOne" resultType="${packageName}.${modelName}">
        <include refid="baseSelect" />
        where t.`id`=#{id}
    </select>

    <select id="find${modelName}PageList" resultType="${packageName}.${modelName}">
        <include refid="baseSelect" />
    </select>
</mapper>