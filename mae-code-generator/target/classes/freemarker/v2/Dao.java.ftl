package ${packageName}.dao;

import com.mae.core.PagedResult;
import ${packageName}.${modelName};
import ${packageName}.${modelName}PageQuery;
import java.util.List;

/**
 * ${modelName} data access object
 *
 * @author mae-framework
 * @since 2022-08-08
 */
public interface ${daoName}{
    /**
     *
     */
    int create${modelName}(${modelName} ${modelName?uncap_first});

    /**
     * delete by id
     *
     * @param id id
     */
    int deleteById(String id);

    /**
     *
     */
    int update${modelName}(${modelName} ${modelName?uncap_first});

    /**
     * find all list
     */
    List<${modelName}> findAll${modelName}s();

    /**
     * find paged list
     *
     * @param query pageQuery
     * @return PageList
     */
    PagedResult<${modelName}> find${modelName}PageList(${modelName}PageQuery query);

}