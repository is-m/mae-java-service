package com.mae.generator.app.service;

import com.mae.core.ReturnMessage;
import com.mae.generator.api.service.IConnectionService;
import com.mae.generator.model.Db;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class ConnectionService implements IConnectionService {
    @Override
    public ReturnMessage connect(Db db) {
        return null;
    }
}
