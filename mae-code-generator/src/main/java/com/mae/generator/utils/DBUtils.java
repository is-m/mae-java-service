package com.mae.generator.utils;

import com.mae.core.exception.MaeException;
import com.mae.generator.model.Db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {
    private static Connection connection;

    public static Connection getConnection() {
        return connection;
    }

    public static Connection initDb(Db db) {
        if (connection == null) {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                connection = DriverManager.getConnection(db.getUrl(), db.getUsername(), db.getPassword());
            } catch (Exception error) {
                throw new MaeException("数据库连接失败", error);
            }
        }
        return connection;
    }
}
