package com.mae.generator.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TableClass {
    private String tableName;  //表名 ，以下是生成的名字
    private String modelName;
    private String serviceName;
    private String mapperName;
    private String daoName;
    private String controllerName;
    private String packageName;
    private List<ColumnClass> columns; // 字段
}
