package com.mae.generator.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ColumnClass {
    private String propertyName; //对应java属性的名字
    private String columnName;  //数据库中的名字
    private String type;        //字段类型
    private String remark;      //备注
    private boolean isPrimary;  //字段是不是一个主键
    private String javaType;
    private String mybaitsJdbcType;
}
