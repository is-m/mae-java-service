package com.mae.generator.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(name = "Db", description = "数据库连接")
public class Db {
    @Schema(name = "username", description = "用户名", example = "root")
    private String username;
    @Schema(name = "password", description = "密码", example = "root")
    private String password;
    @Schema(name = "url", description = "连接串", example = "jdbc:mysql://localhost:3306/test")
    private String url;
}
