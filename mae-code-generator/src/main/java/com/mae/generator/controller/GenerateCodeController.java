package com.mae.generator.controller;

import cn.hutool.core.date.DatePattern;
import com.mae.generator.model.TableClass;
import com.mae.generator.service.DatabaseService;
import com.mae.generator.service.GenerateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
public class GenerateCodeController {
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private GenerateCodeService generateCodeService;

    @PostMapping("/api/generateCode")
    public ResponseEntity<InputStreamResource> generateCode(@RequestBody List<TableClass> tableClassList) throws IOException {
        if (tableClassList == null || tableClassList.isEmpty()) {
            tableClassList = databaseService.getTables("org.example");
        }
        File codeZipFile = generateCodeService.generateCode(tableClassList);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        String fileName = "generateCode" + DatePattern.PURE_DATETIME_FORMAT.format(new Date()) + ".zip";
        headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        FileSystemResource fileSystemResource = new FileSystemResource(codeZipFile);
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(fileSystemResource.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(new InputStreamResource(fileSystemResource.getInputStream()));
    }
}
