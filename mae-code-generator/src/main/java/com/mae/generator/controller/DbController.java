package com.mae.generator.controller;

import cn.hutool.core.util.StrUtil;
import com.mae.core.ReturnCollection;
import com.mae.core.ReturnMessage;
import com.mae.core.exception.MaeException;
import com.mae.generator.model.Db;
import com.mae.generator.model.TableClass;
import com.mae.generator.service.DatabaseService;
import com.mae.generator.utils.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class DbController {
    @Autowired
    private DatabaseService databaseService;

    @PostMapping("/api/connect")
    public ReturnMessage connect(@RequestBody Db db) {
        Connection con = DBUtils.initDb(db);
        return ReturnMessage.ok("数据库连接成功");
    }

    @PostMapping("/api/config")
    public ReturnCollection<TableClass> config(@RequestBody Map<String, String> map) {
        String packageName = map.get("packageName");
        List<TableClass> tables = databaseService.getTables(packageName);
        return new ReturnCollection<>(tables);
    }
}