package com.mae.generator.service;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ZipUtil;
import com.mae.core.exception.MaeException;
import com.mae.generator.model.ColumnClass;
import com.mae.generator.model.TableClass;
import com.mae.generator.utils.DBUtils;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
@Service
public class GenerateCodeService {
    private static final Map<String, Pair<String, String>> typeMap;
    private static final Pair<String, String> PAIR_STRING = Pair.of("String", "VARCHAR");
    private static final Pair<String, String> PAIR_DATE = Pair.of("Date", "TIMESTAMP");
    private static final Pair<String, String> PAIR_INTEGER = Pair.of("Integer", "NUMERIC");
    private static final Pair<String, String> PAIR_LONG = Pair.of("Long", "NUMERIC");
    private static final Pair<String, String> PAIR_DOUBLE = Pair.of("Double", "NUMERIC");
    private static final Pair<String, String> PAIR_BOOLEAN = Pair.of("Boolean", "BIT");

    static {
        typeMap = new HashMap<>();
        Stream.of("VARCHAR,TEXT,CHAR".split(",")).forEach(item -> typeMap.put(item, PAIR_STRING));
        Stream.of("DATETIME".split(",")).forEach(item -> typeMap.put(item, PAIR_DATE));
        Stream.of("INT".split(",")).forEach(item -> typeMap.put(item, PAIR_INTEGER));
        Stream.of("BIGINT".split(",")).forEach(item -> typeMap.put(item, PAIR_LONG));
        Stream.of("DOUBLE".split(",")).forEach(item -> typeMap.put(item, PAIR_DOUBLE));
        Stream.of("BIT".split(",")).forEach(item -> typeMap.put(item, PAIR_BOOLEAN));
    }

    Configuration cfg = null;

    {
        cfg = new Configuration(Configuration.VERSION_2_3_30);
        cfg.setTemplateLoader(new ClassTemplateLoader(GenerateCodeService.class, "/freemarker"));
        cfg.setDefaultEncoding("UTF-8");
    }

    public File generateCode(List<TableClass> tableClassList) {
        // 创建一个临时文件
        File tempZipFile = FileUtil.createTempFile();
        tempZipFile.deleteOnExit();
        ZipOutputStream zipOutputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(tempZipFile);
            zipOutputStream = ZipUtil.getZipOutputStream(fileOutputStream, Charset.defaultCharset());

            Template modelTemplate = cfg.getTemplate("Model.java.ftl");
            Template mapperJavaTemplate = cfg.getTemplate("Mapper.java.ftl");
            Template mapperXmlTemplate = cfg.getTemplate("Mapper.xml.ftl");
            Template serviceTemplate = cfg.getTemplate("Service.java.ftl");
            Template controllerTemplate = cfg.getTemplate("Controller.java.ftl");
            Connection connection = DBUtils.getConnection();
            DatabaseMetaData metaData = connection.getMetaData();
            for (TableClass tableClass : tableClassList) {
                ResultSet columns = metaData.getColumns(connection.getCatalog(), null, tableClass.getTableName(), null);
                ResultSet primaryKeys = metaData.getPrimaryKeys(connection.getCatalog(), null, tableClass.getTableName());
                List<ColumnClass> columnClassList = new ArrayList<>();
                while (columns.next()) {
                    String column_name = columns.getString("COLUMN_NAME");
                    String type_name = columns.getString("TYPE_NAME");
                    String remarks = columns.getString("REMARKS");
                    ColumnClass columnClass = new ColumnClass();
                    columnClass.setRemark(remarks);
                    columnClass.setColumnName(column_name);
                    columnClass.setType(type_name);
                    initJavaAndMybatisJdbcType(columnClass);
                    columnClass.setPropertyName(StrUtil.lowerFirst(StrUtil.toCamelCase(column_name.toLowerCase(Locale.ROOT))));
                    primaryKeys.first();
                    while (primaryKeys.next()) {
                        String pkName = primaryKeys.getString("COLUMN_NAME");
                        if (column_name.equals(pkName)) {
                            columnClass.setPrimary(true);
                        }
                    }
                    columnClassList.add(columnClass);
                }
                tableClass.setColumns(columnClassList);
                String path = tableClass.getPackageName().replace(".", "/");
                String pathJavaSrc = "src/main/java/" + path;
                String pathResourceSrc = "src/main/resources/" + path;

                generate(modelTemplate, tableClass, pathJavaSrc + "/model/", zipOutputStream);
                generate(mapperJavaTemplate, tableClass, pathJavaSrc + "/mapper/", zipOutputStream);
                generate(mapperXmlTemplate, tableClass, pathResourceSrc + "/mapper/", zipOutputStream);
                generate(serviceTemplate, tableClass, pathJavaSrc + "/service/", zipOutputStream);
                generate(controllerTemplate, tableClass, pathJavaSrc + "/controller/", zipOutputStream);
            }
            zipOutputStream.finish();
            return tempZipFile;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
            if (zipOutputStream != null) {
                try {
                    zipOutputStream.close();
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        }
        throw new MaeException("代码生成失败");
    }

    private void initJavaAndMybatisJdbcType(ColumnClass columnClass) {
        String type = columnClass.getType().toUpperCase(Locale.ROOT);
        if (!typeMap.containsKey(type)) {
            throw new IllegalArgumentException("column not support for type" + type);
        }

        Pair<String, String> typePair = typeMap.get(type);
        columnClass.setJavaType(typePair.getLeft());
        columnClass.setMybaitsJdbcType(typePair.getRight());
    }

    private void generate(Template template, TableClass tableClass, String path, ZipOutputStream zipOutputStream) throws IOException, TemplateException {
        String fileName = path + tableClass.getModelName() + template.getName().replace(".ftl", "").replace("Model", "");
        log.debug("put code generate zip file " + fileName);
        zipOutputStream.putNextEntry(new ZipEntry(fileName));
        OutputStreamWriter out = new OutputStreamWriter(zipOutputStream);
        template.process(tableClass, out);
        zipOutputStream.closeEntry();
    }
}

