package com.mae.generator.service;

import cn.hutool.core.util.StrUtil;
import com.mae.core.exception.MaeException;
import com.mae.generator.model.TableClass;
import com.mae.generator.utils.DBUtils;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class DatabaseService {

    public List<TableClass> getTables(String packageName) {
        try {
            Connection connection = DBUtils.getConnection();
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet tables = metaData.getTables(connection.getCatalog(), null, null, null);
            List<TableClass> tableClassList = new ArrayList<>();
            while (tables.next()) {
                TableClass tableClass = new TableClass();
                tableClass.setPackageName(packageName);
                String table_name = tables.getString("TABLE_NAME");
                String modelName = StrUtil.upperFirst(StrUtil.toCamelCase(table_name));
                tableClass.setTableName(table_name);
                tableClass.setModelName(modelName);
                tableClass.setControllerName(modelName + "Controller");
                tableClass.setMapperName(modelName + "Mapper");
                tableClass.setDaoName(modelName + "Dao");
                tableClass.setServiceName(modelName + "Service");
                tableClassList.add(tableClass);
            }
            return tableClassList;
        } catch (Exception e) {
            throw new MaeException("读取数据库失败", e);
        }
    }
}
