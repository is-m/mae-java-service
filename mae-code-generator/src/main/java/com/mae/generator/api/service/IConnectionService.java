package com.mae.generator.api.service;

import com.mae.core.ReturnMessage;
import com.mae.generator.model.Db;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "UserControllerApi", description = "用户的增删改查")
public interface IConnectionService {

    @Operation(summary = "添加用户", description = "根据姓名添加用户并返回")
    ReturnMessage connect(Db db);
}
