package ${packageName};

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ${modelName} {
<#if columns??>
  <#list columns as column>

    /**
     * ${column.remark}
     */
    private ${column.javaType} ${column.propertyName?uncap_first};
  </#list>
</#if>
}