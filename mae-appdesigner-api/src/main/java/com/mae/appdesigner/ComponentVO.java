package com.mae.appdesigner;

import java.util.List;

public class ComponentVO {
    /**
     * 类型
     */
    private String type;

    /**
     * 名称
     */
    private String name;

    /**
     * 组件图标
     */
    private String icon;

    /**
     * 组件属性
     */
    private List<ComponentAttributeVO> attributes;

    /**
     * 组件事件
     */
    private List<ComponentEventVO> events;
}
