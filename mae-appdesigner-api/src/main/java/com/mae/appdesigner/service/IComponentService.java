package com.mae.appdesigner.service;

import com.mae.appdesigner.ComponentVO;

/**
 * 属性服务
 */
public interface IComponentService {
    /**
     * 新建属性
     *
     * @return
     */
    ComponentVO createComponent(ComponentVO componentVO);

    /**
     * 修改属性
     *
     * @param componentVO
     * @return
     */
    ComponentVO updateComponent(ComponentVO componentVO);

    /**
     * 删除属性
     *
     * @param id
     * @return
     */
    int deleteComponent(String id);
}
