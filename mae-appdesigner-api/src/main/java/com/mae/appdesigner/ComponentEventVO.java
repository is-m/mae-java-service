package com.mae.appdesigner;

public class ComponentEventVO {
    /**
     * 事件名称
     */
    private String name;

    /**
     * 事件说明
     */
    private String description;
}
