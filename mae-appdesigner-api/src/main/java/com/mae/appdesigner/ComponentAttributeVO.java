package com.mae.appdesigner;

public class ComponentAttributeVO {
    /**
     * 属性名称
     */
    private String name;

    /**
     * 属性说明
     */
    private String description;

    /**
     * 属性值类型
     */
    private String valueType;
}
