package com.mae.rpc.utils;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.stream.Stream;

/**
 * HttpClient java 11 引入的
 */
@Slf4j
public class HttpUtils {
    private static ExecutorService es = new ThreadPoolExecutor(5, 200, 60L, TimeUnit.SECONDS, new ArrayBlockingQueue<>(200), new CustomizableThreadFactory("RPC-Request"));

    /**
     * get 请求 （30秒超时）
     *
     * @param url 请求地址
     * @return 响应
     */
    public static HttpResponse<String> get(String url) {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(url)).timeout(Duration.ofSeconds(30)).build();
        try {
            log.debug("get " + url);
            return httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            throw new RuntimeException("请求初始失败", e);
        }
    }

    /**
     * 多地址请求, 取任意成功返回
     *
     * @return
     */
    public static HttpResponse<String> anyGet(List<String> urls) {
        log.info("get any {}", urls);
        HttpClient httpClient = HttpClient.newHttpClient();

        List<CompletableFuture<HttpResponse<String>>> completableFutures = urls.stream().map(url -> {
            HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(url)).timeout(Duration.ofSeconds(10)).build();
            return httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString());
        }).toList();
        try {
            return (HttpResponse<String>) CompletableFuture.anyOf(completableFutures.toArray(new CompletableFuture[0])).get(11, TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new RuntimeException("批量请求失败", e);
        }
    }

    public static String post(String url, Object data) {
        HttpRequest.BodyPublisher bodyPublisher = HttpRequest.BodyPublishers.ofString(JSON.toJSONString(data));
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(url)).header("content-type","application/json").method("POST", bodyPublisher).timeout(Duration.ofSeconds(30)).build();

        try {
            log.debug("post " + url);
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (Objects.equals(200, response.statusCode())) {
                return response.body();
            }
            throw new IllegalStateException("post url " + url + " " + response.statusCode() + " " + response.body());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("请求失败 POST " + url, e);
        }
    }

    /*
    private static CompletableFuture<String> sendAsyncGetRequest(HttpClient httpClient, String url) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();

        return httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body);
    }

    public static void main(String[] args) {
        HttpClient httpClient = HttpClient.newHttpClient();
            String url = "https://api.example.com/data"; // 替换为你要请求的URL

            CompletableFuture<String> responseFuture = sendAsyncGetRequest(httpClient, url);

            // 在响应到达之前执行其他操作
            responseFuture.thenAccept(response -> {
                System.out.println("Response: " + response);
            }).exceptionally(error -> {
                System.err.println("Error: " + error.getMessage());
                return null;
            });

            // 阻塞主线程，直到异步请求完成
            responseFuture.join();
     }
    */
}
