package com.mae.rpc.annotation;

import java.lang.annotation.*;


@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Rpc {
    /**
     * 微服务的名称，同一个注册中心中使用
     *
     * @return
     */
    String service() default "";

    /**
     * 服务访问端点，不同注册中心或者不同的公司组织
     *
     * @return
     */
    String endpoint() default "";
}
