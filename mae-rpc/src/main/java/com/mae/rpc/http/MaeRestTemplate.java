package com.mae.rpc.http;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class MaeRestTemplate {
    @Autowired
    private Environment environment;

    public Object get(String restName, Object param, Class<?> returnType) {
        RpcRestInfo rpcRestInfo = environment.getProperty("rest." + restName, RpcRestInfo.class);

        return null;
    }

    @Getter
    @Setter
    public class RpcRestInfo {
        private String url;
        private String method;
    }

}
