package com.mae.logging.service;

import com.alibaba.fastjson2.JSON;
import com.mae.logging.SystemErrorLogVO;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Named;

@Slf4j
@Named
public class SystemErrorLogService implements ISystemErrorLogService {

    @Override
    public void createSystemErrorLog(SystemErrorLogVO systemErrorLogVO) {
        log.error("todo write db ==> {}", JSON.toJSONString(systemErrorLogVO));
    }
}
