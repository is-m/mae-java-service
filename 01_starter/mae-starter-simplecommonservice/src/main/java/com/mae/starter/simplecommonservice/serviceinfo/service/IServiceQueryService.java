package com.mae.starter.simplecommonservice.serviceinfo.service;

import com.mae.core.ReturnPagedCollection;
import com.mae.core.ReturnValue;
import com.mae.starter.simplecommonservice.serviceinfo.constant.ServiceInfoConstant;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInstance;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoCountQuery;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoSingletonQuery;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoPageQuery;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Optional;

/**
 *  服务信息查询
 */
@RequestMapping(ServiceInfoConstant.REST_BASE_PATH)
public interface IServiceQueryService {
    /**
     * 服务计数
     *
     * @param countQuery
     * @return
     */
    ReturnValue<Long> countServiceInfo(ServiceInfoCountQuery countQuery);

    /**
     * 单个服务实例查询
     *
      * @param singletonQuery
     * @return
     */
    @RequestMapping(value = "/{serviceId}", method = RequestMethod.GET)
    ReturnValue<ServiceInfo> getOne(ServiceInfoSingletonQuery singletonQuery);

    /**
     * 分页查找
     *
     * @param pageQuery
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    ReturnPagedCollection<ServiceInfo> getPageList(ServiceInfoPageQuery pageQuery);

    /**
     * 获取全部的服务ID
     * @return
     */
    List<String> getAllServices();

    List<ServiceInstance> findAllInstances();
}
