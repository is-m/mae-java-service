package com.mae.starter.simplecommonservice.serviceinfo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@TableName("tserviceinstance")
public class ServiceInstance {
    @TableId(value = "id", type = IdType.AUTO)
    private String id;
    @TableField(value="service_id")
    private String serviceId;
    private String host;
    private String type;
    private Integer port;
    private String endpoint;
    private String status;
    private String env;
    private String metadata;
    private Date createTime;
    private Date modifyTime;
    // 最后上报时间
    private Date lastUp_time;
}
