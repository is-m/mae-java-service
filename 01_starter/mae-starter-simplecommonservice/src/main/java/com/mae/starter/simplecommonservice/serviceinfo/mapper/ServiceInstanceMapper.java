package com.mae.starter.simplecommonservice.serviceinfo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInstance;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ServiceInstanceMapper extends BaseMapper<ServiceInstance> {


}
