package com.mae.starter.simplecommonservice.serviceinfo.query;

import com.mae.core.PageQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceInfoPageQuery extends PageQuery {
    // 是否包含服务实例
    private boolean includeInstance;
    // 服务实例所属环境
    private String instanceEnv;
}
