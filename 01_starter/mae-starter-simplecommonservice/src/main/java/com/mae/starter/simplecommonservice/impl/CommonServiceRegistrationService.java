package com.mae.starter.simplecommonservice.impl;

import com.mae.cloud.discovery.simpleclient.spi.IRegistrationService;
import com.mae.starter.simplecommonservice.serviceinfo.command.ServiceInstanceDownCommand;
import com.mae.starter.simplecommonservice.serviceinfo.command.UpServiceInstanceCommand;
import com.mae.starter.simplecommonservice.serviceinfo.service.impl.ServiceCommandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CommonServiceRegistrationService implements IRegistrationService {
    @Autowired
    private ServiceCommandService serviceCommandService;

    @Override
    public void register(org.springframework.cloud.client.ServiceInstance serviceInstance) {
        log.info("register service instance {}", serviceInstance);
        UpServiceInstanceCommand upServiceInstanceCommand = new UpServiceInstanceCommand();
        upServiceInstanceCommand.setServiceId(serviceInstance.getServiceId());
        upServiceInstanceCommand.setInstanceId(serviceInstance.getInstanceId());
        upServiceInstanceCommand.setEndpoint(serviceInstance.getUri().toString());
        upServiceInstanceCommand.setEnv(serviceInstance.getMetadata().get("env"));
        serviceCommandService.upServiceInstance(upServiceInstanceCommand);
    }

    @Override
    public void deregister(org.springframework.cloud.client.ServiceInstance serviceInstance) {
        log.info("deregister service instance {}", serviceInstance);
        ServiceInstanceDownCommand serviceInstanceDownCommand = new ServiceInstanceDownCommand();
        serviceInstanceDownCommand.setInstanceId(serviceInstance.getInstanceId());
        serviceCommandService.downServiceInstance(serviceInstanceDownCommand);
    }
}
