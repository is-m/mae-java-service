package com.mae.starter.simplecommonservice.serviceinfo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ServiceInfoMapper extends BaseMapper<ServiceInfo> {
}
