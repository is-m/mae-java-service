package com.mae.starter.simplecommonservice.config;

import com.mae.cloud.discovery.simpleserver.config.EnableDiscoverySimpleServer;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("com.mae")
//@MapperScan("com.mae.starter.simplecommonservice.serviceinfo.mapper")
@EnableDiscoverySimpleServer
@Configuration
@Slf4j
public class SimpleCommonServiceAutoConfig {

    public SimpleCommonServiceAutoConfig() {
        log.info("simple common service init.");
    }
}
