package com.mae.starter.simplecommonservice.serviceinfo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@TableName("tserviceinfo")
public class ServiceInfo {
    private String id;
    private String name;
    private String description;
    /**
     * 类型 公共服务 common, 业务服务 business
     */
    private String type;

    /**
     * 公共服务的实例，如果当前服务本身就是公共服务时，该属性不返回
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @TableField(exist = false)
    private List<ServiceInstance> commons;

    /**
     * 服务本身的实例
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @TableField(exist = false)
    private List<ServiceInstance> instances;
}
