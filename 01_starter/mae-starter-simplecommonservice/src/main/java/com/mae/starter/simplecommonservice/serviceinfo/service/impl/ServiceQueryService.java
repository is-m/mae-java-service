package com.mae.starter.simplecommonservice.serviceinfo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mae.core.ReturnPagedCollection;
import com.mae.core.ReturnValue;
import com.mae.core.utils.StringUtils;
import com.mae.orm.mybatisplus.util.PageUtils;
import com.mae.starter.simplecommonservice.serviceinfo.constant.ServiceInfoConstant;
import com.mae.starter.simplecommonservice.serviceinfo.mapper.ServiceInfoMapper;
import com.mae.starter.simplecommonservice.serviceinfo.mapper.ServiceInstanceMapper;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoCountQuery;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoSingletonQuery;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoPageQuery;
import com.mae.starter.simplecommonservice.serviceinfo.service.IServiceQueryService;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInfo;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@RestController
public class ServiceQueryService implements IServiceQueryService {
    @Autowired
    private ServiceInfoMapper serviceInfoMapper;
    @Autowired
    private ServiceInstanceMapper serviceInstanceMapper;

    @Override
    public ReturnValue<Long> countServiceInfo(ServiceInfoCountQuery countQuery) {
        LambdaQueryWrapper<ServiceInfo> serviceInfoLambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.hasText(countQuery.getId())) {
            serviceInfoLambdaQueryWrapper.eq(ServiceInfo::getId, countQuery.getId());
        }
        Long count = serviceInfoMapper.selectCount(serviceInfoLambdaQueryWrapper);
        return ReturnValue.of(count);
    }

    @Override
    public ReturnValue<ServiceInfo> getOne(ServiceInfoSingletonQuery singletonQuery) {
        ServiceInfo fullServiceInfo = getFullServiceInfo(singletonQuery);
        if (singletonQuery.isIncludeInstance()) {
            String env = singletonQuery.getInstanceEnv();
            if (StringUtils.hasText(env)) {
                log.info("filter service instance by environment {}", env);
                fullServiceInfo.setCommons(filterInstancesByEnv(fullServiceInfo.getCommons(), env));
                fullServiceInfo.setInstances(filterInstancesByEnv(fullServiceInfo.getInstances(), env));
            } else {
                log.info("return service instance all environment");
            }
        } else {
            log.info("not return service instances");
            fullServiceInfo.setCommons(null);
            fullServiceInfo.setInstances(null);
        }
        return ReturnValue.of(fullServiceInfo);
    }

    @Override
    @RequestMapping(value="", method = RequestMethod.GET)
    public ReturnPagedCollection<ServiceInfo> getPageList(ServiceInfoPageQuery pageQuery) {
        IPage<ServiceInfo> page = PageUtils.toPage(pageQuery);
        LambdaQueryWrapper<ServiceInfo> serviceInfoLambdaQueryWrapper = new LambdaQueryWrapper<>();
        List<ServiceInfo> records = serviceInfoMapper.selectList(page, serviceInfoLambdaQueryWrapper);
        ReturnPagedCollection<ServiceInfo> serviceInfoReturnPagedCollection = PageUtils.toPagedCollection(page, records);
        if (pageQuery.isIncludeInstance()) {
            List<List<ServiceInfo>> partition = serviceInfoReturnPagedCollection.partitionContent(50);
            for (List<ServiceInfo> part : partition) {
                List<String> serviceInfoIdList = part.stream().map(ServiceInfo::getId).toList();
                LambdaQueryWrapper<ServiceInstance> serviceInstanceLambdaQueryWrapper = new LambdaQueryWrapper<>();
                serviceInstanceLambdaQueryWrapper.in(ServiceInstance::getServiceId, serviceInfoIdList);
                if (StringUtils.hasText(pageQuery.getInstanceEnv())) {
                    serviceInstanceLambdaQueryWrapper.eq(ServiceInstance::getEnv, pageQuery.getInstanceEnv());
                }
                List<ServiceInstance> serviceInstances = serviceInstanceMapper.selectList(serviceInstanceLambdaQueryWrapper);
                Map<String, List<ServiceInstance>> serviceInfoInstanceMap = serviceInstances.stream().collect(Collectors.groupingBy(ServiceInstance::getServiceId));
                part.forEach(item -> {
                    List<ServiceInstance> instances = serviceInfoInstanceMap.get(item.getId());
                    item.setInstances(instances);
                });
            }

        }
        return serviceInfoReturnPagedCollection;
    }

    @Override
    public List<String> getAllServices() {
        LambdaQueryWrapper<ServiceInfo> serviceInfoLambdaQueryWrapper = new LambdaQueryWrapper<>();
        serviceInfoLambdaQueryWrapper.select(ServiceInfo::getId);
        return serviceInfoMapper.selectObjs(serviceInfoLambdaQueryWrapper);
    }

    @Override
    public List<ServiceInstance> findAllInstances() {
        LambdaQueryWrapper<ServiceInstance> serviceInstanceLambdaQueryWrapper = new LambdaQueryWrapper<>();
        return serviceInstanceMapper.selectList(serviceInstanceLambdaQueryWrapper);
    }

    /**
     * 根据环境过滤服务实例
     *
     * @param instances 待过滤的实例列表
     * @param env       环境信息
     * @return 环境匹配的实例列表
     */
    private List<ServiceInstance> filterInstancesByEnv(List<ServiceInstance> instances, String env) {
        if (instances == null) {
            return null;
        }
        return instances.stream().filter(item -> Objects.equals(env, item.getEnv())).toList();
    }

    // TODO 这里加根据服务ID添加缓存
    private ServiceInfo getFullServiceInfo(ServiceInfoSingletonQuery singletonQuery) {
        LambdaQueryWrapper<ServiceInfo> serviceInfoLambdaQueryWrapper = new LambdaQueryWrapper<>();
        String serviceId = singletonQuery.getId();
        serviceInfoLambdaQueryWrapper.eq(ServiceInfo::getId, serviceId);
        ServiceInfo serviceInfo = serviceInfoMapper.selectById(serviceId);
        if (serviceInfo != null && singletonQuery.isIncludeInstance()) {
            // 如果不是公共服务，则加载多加载一次公共服务实例
            if (ServiceInfoConstant.SERVICE_COMMON.equals(serviceId)) {
                LambdaQueryWrapper<ServiceInstance> instanceLambdaQueryWrapper = new LambdaQueryWrapper<>();
                instanceLambdaQueryWrapper.eq(ServiceInstance::getServiceId, serviceId);
                List<ServiceInstance> serviceInstances = serviceInstanceMapper.selectList(instanceLambdaQueryWrapper);
                serviceInfo.setInstances(serviceInstances);
            } else {
                LambdaQueryWrapper<ServiceInstance> commonInstanceLambdaQueryWrapper = new LambdaQueryWrapper<>();
                commonInstanceLambdaQueryWrapper.in(ServiceInstance::getServiceId, Arrays.asList(ServiceInfoConstant.SERVICE_COMMON, serviceId));
                List<ServiceInstance> serviceInstances = serviceInstanceMapper.selectList(commonInstanceLambdaQueryWrapper);
                Map<String, List<ServiceInstance>> instancesMap = serviceInstances.stream().collect(Collectors.groupingBy(ServiceInstance::getServiceId));
                serviceInfo.setCommons(instancesMap.get(ServiceInfoConstant.SERVICE_COMMON));
                serviceInfo.setInstances(instancesMap.get(serviceId));
            }

        }
        return serviceInfo;
    }

}
