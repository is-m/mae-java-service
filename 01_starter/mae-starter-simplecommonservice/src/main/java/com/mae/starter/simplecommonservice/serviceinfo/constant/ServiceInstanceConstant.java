package com.mae.starter.simplecommonservice.serviceinfo.constant;

/**
 * 服务实例常量
 */
public class ServiceInstanceConstant {
    /**
     * 存活
     */
    public static final String STATUS_UP = "UP";
    /**
     * 停用
     */
    public static final String STATUS_DOWN = "DOWN";

    /**
     * 公共服务
     */
    public static final String TYPE_COMMON = "common";

    /**
     * 业务服务
     */
    public static final String TYPE_BUSINESS = "business";

    /**
     * 服务实例元数据:环境:默认环境
     */
    public static final String ENV_DEFAULT = "default";
}
