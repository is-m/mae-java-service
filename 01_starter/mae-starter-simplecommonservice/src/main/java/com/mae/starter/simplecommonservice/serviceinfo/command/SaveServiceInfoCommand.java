package com.mae.starter.simplecommonservice.serviceinfo.command;

import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInstance;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import java.util.List;

@Getter
@Setter
@ToString
public class SaveServiceInfoCommand {
    @NotBlank
    private String id;
    @NotBlank
    private String name;
    @Length(max=2000)
    private String description;

    @Valid
    private List<ServiceInstance> instances;

    // 是否在保存返回时包含实例，默认不包含
    private boolean returnInstances;
}
