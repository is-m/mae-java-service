package com.mae.starter.simplecommonservice.serviceinfo.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceInfoCountQuery {
    private String id;

    public static ServiceInfoCountQuery only(String id) {
        return new ServiceInfoCountQuery(){{
            setId(id);
        }};
    }
}
