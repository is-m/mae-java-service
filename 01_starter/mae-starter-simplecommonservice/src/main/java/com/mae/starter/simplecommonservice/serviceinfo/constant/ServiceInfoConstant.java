package com.mae.starter.simplecommonservice.serviceinfo.constant;

/**
 * 服务信息常量
 */
public class ServiceInfoConstant {
    /**
     * REST 接口基路径
     */
    public static final String REST_BASE_PATH = "/common/service-info";

    /**
     * 公共服务
     */
    public static final String SERVICE_COMMON = "common-service";
}
