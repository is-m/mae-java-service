package com.mae.starter.simplecommonservice.serviceinfo.command;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.bind.annotation.PathVariable;

@Getter
@Setter
@ToString
public class ServiceInstanceDownCommand {
    private String serviceId;
    /**
     * 实例ID
     */
    @NotBlank
    private String instanceId;
}
