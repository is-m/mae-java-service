package com.mae.starter.simplecommonservice.serviceinfo.command;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
public class UpServiceInstanceCommand {
    private String instanceId;
    private String serviceId;
    private String endpoint;
    private String env;
    private Map<String, String> metadata;
}
