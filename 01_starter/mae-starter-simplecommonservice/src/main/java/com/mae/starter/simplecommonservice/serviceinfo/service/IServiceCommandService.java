package com.mae.starter.simplecommonservice.serviceinfo.service;

import com.mae.core.ReturnMessage;
import com.mae.core.ReturnValue;
import com.mae.starter.simplecommonservice.serviceinfo.command.SaveServiceInfoCommand;
import com.mae.starter.simplecommonservice.serviceinfo.command.ServiceInstanceDownCommand;
import com.mae.starter.simplecommonservice.serviceinfo.command.UpServiceInstanceCommand;
import com.mae.starter.simplecommonservice.serviceinfo.constant.ServiceInfoConstant;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInfo;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 服务命令
 */
@Valid
@RequestMapping(ServiceInfoConstant.REST_BASE_PATH)
public interface IServiceCommandService {
    /**
     * 保存服务
     *
     * @param saveServiceInfoCommand
     * @return
     */
    @RequestMapping(value = "/{serviceId}/save", method = RequestMethod.POST)
    ReturnValue<ServiceInfo> saveServiceInfo(@RequestBody @NotNull SaveServiceInfoCommand saveServiceInfoCommand);

    /**
     * 上报服务实例
     *
     * @param upServiceInstanceCommand
     * @return
     */
    @RequestMapping(value = "/{serviceId}/instance/{instanceId}/up", method = RequestMethod.POST)
    ReturnValue<ServiceInfo> upServiceInstance(@RequestBody @PathVariable @NotNull UpServiceInstanceCommand upServiceInstanceCommand);

    /**
     * 下线服务实例
     *
     * @param serviceInstanceDownCommand
     * @return
     */
    @RequestMapping(value = "/{serviceId}/instance/{instanceId}/down", method = RequestMethod.POST)
    ReturnMessage downServiceInstance(@PathVariable @NotNull ServiceInstanceDownCommand serviceInstanceDownCommand);
}
