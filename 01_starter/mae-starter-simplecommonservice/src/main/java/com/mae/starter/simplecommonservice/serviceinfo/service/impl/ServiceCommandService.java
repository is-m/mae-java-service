package com.mae.starter.simplecommonservice.serviceinfo.service.impl;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.mae.core.ReturnMessage;
import com.mae.core.ReturnValue;
import com.mae.core.utils.Assert;
import com.mae.core.utils.StringUtils;
import com.mae.starter.simplecommonservice.serviceinfo.command.SaveServiceInfoCommand;
import com.mae.starter.simplecommonservice.serviceinfo.command.ServiceInstanceDownCommand;
import com.mae.starter.simplecommonservice.serviceinfo.command.UpServiceInstanceCommand;
import com.mae.starter.simplecommonservice.serviceinfo.constant.ServiceInstanceConstant;
import com.mae.starter.simplecommonservice.serviceinfo.mapper.ServiceInfoMapper;
import com.mae.starter.simplecommonservice.serviceinfo.mapper.ServiceInstanceMapper;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoCountQuery;
import com.mae.starter.simplecommonservice.serviceinfo.query.ServiceInfoSingletonQuery;
import com.mae.starter.simplecommonservice.serviceinfo.service.IServiceCommandService;
import com.mae.starter.simplecommonservice.serviceinfo.service.IServiceQueryService;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInstance;
import com.mae.starter.simplecommonservice.serviceinfo.entity.ServiceInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.BatchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RestController
public class ServiceCommandService implements IServiceCommandService {
    @Autowired
    private ServiceInfoMapper serviceInfoMapper;
    @Autowired
    private ServiceInstanceMapper serviceInstanceMapper;
    @Autowired
    private IServiceQueryService serviceQueryService;

    @Override
    public ReturnValue<ServiceInfo> saveServiceInfo(SaveServiceInfoCommand saveServiceInfoCommand) {
        log.info("saveServiceInfo {}", saveServiceInfoCommand);
        // 检查数据是否存在
        String serviceId = saveServiceInfoCommand.getId();

        ServiceInfo serviceInfo = new ServiceInfo();
        serviceInfo.setId(serviceId);
        serviceInfo.setName(saveServiceInfoCommand.getName());
        serviceInfo.setDescription(saveServiceInfoCommand.getDescription());

        // 存在的话就更新
        if (existServiceInfo(serviceId)) {
            serviceInfoMapper.updateById(serviceInfo);
        } else { // 不存在时插入
            serviceInfoMapper.insert(serviceInfo);
        }
        // 合并服务实例
        this.mergeServiceInstances(serviceId, saveServiceInfoCommand.getInstances());
        if (saveServiceInfoCommand.isReturnInstances()) {
            return serviceQueryService.getOne(ServiceInfoSingletonQuery.full(serviceId, null));
        }
        return ReturnValue.of(serviceInfo);
    }

    @Override
    public ReturnValue<ServiceInfo> upServiceInstance(UpServiceInstanceCommand upServiceInstanceCommand) {
        log.info("upServiceInstance {}", upServiceInstanceCommand);
        // 检查是否存在服务信息
        String serviceId = upServiceInstanceCommand.getServiceId();
        URI uri = URI.create(upServiceInstanceCommand.getEndpoint());
        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setServiceId(serviceId);
        serviceInstance.setPort(uri.getPort());
        serviceInstance.setHost(uri.getHost());
        serviceInstance.setEndpoint(upServiceInstanceCommand.getEndpoint());
        serviceInstance.setStatus(ServiceInstanceConstant.STATUS_UP);
        serviceInstance.setEnv(upServiceInstanceCommand.getEnv());
        serviceInstance.setMetadata(JSON.toJSONString(upServiceInstanceCommand.getMetadata()));
        if(!existServiceInfo(serviceId)){
            // 不存在服务信息时先注册服务
            SaveServiceInfoCommand saveServiceInfoCommand = new SaveServiceInfoCommand();
            saveServiceInfoCommand.setId(serviceId);
            saveServiceInfoCommand.setName(serviceId);
            saveServiceInfoCommand.setDescription("auto discovery");
            saveServiceInfoCommand.setInstances(Collections.singletonList(serviceInstance));
            saveServiceInfoCommand.setReturnInstances(true);
            return saveServiceInfo(saveServiceInfoCommand);
        } else {
            // 给服务服务实例添加服务信息
            mergeServiceInstances(serviceId, Collections.singletonList(serviceInstance));
            return serviceQueryService.getOne(ServiceInfoSingletonQuery.full(serviceId, null));
        }
    }

    @Override
    public ReturnMessage downServiceInstance(ServiceInstanceDownCommand serviceInstanceDownCommand) {
        log.info("downServiceInstance {}", serviceInstanceDownCommand);
        String instanceId = serviceInstanceDownCommand.getInstanceId();
        // TODO 先查找，看调用服务的来源是否可信，业务服务的认证只能注销服务本身
        LambdaUpdateWrapper<ServiceInstance> serviceInstanceLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        serviceInstanceLambdaUpdateWrapper.set(ServiceInstance::getStatus, ServiceInstanceConstant.STATUS_DOWN);
        serviceInstanceLambdaUpdateWrapper.eq(ServiceInstance::getId, instanceId);
        int downCount = serviceInstanceMapper.update(serviceInstanceLambdaUpdateWrapper);
        if(Objects.equals(downCount,1)){
            log.info("service instance {} down finish", serviceInstanceDownCommand.getInstanceId());
            return ReturnMessage.ok();
        }
        log.warn("service instance {} down fail of count {}", serviceInstanceDownCommand.getInstanceId(), downCount);
        return ReturnMessage.error("service instance down count " + downCount);
    }

    /**
     * 合并服务实例
     *
     * @param serviceId 服务ID
     * @param instances 实例列表
     */
    private void mergeServiceInstances(String serviceId, List<ServiceInstance> instances) {
        if (!StringUtils.hasText(serviceId) || CollectionUtils.isEmpty(instances)) {
            log.debug("service {} merge instances is empty", serviceId);
            return;
        }
        log.info("service {} merge {} instances", serviceId, instances.size());
        for (ServiceInstance serviceInstance : instances) {
            serviceInstance.setServiceId(serviceId);
            if (!StringUtils.hasText(serviceInstance.getId())) {
                serviceInstance.setId(serviceId + ":" + serviceInstance.getEnv() + ":" + serviceInstance.getEndpoint());
            }
        }
        List<BatchResult> batchResults = serviceInstanceMapper.insertOrUpdate(instances);
        if(log.isInfoEnabled()) {
            log.info("batch insertOrUpdate result {}", batchResults.toString());
        }
    }

    /**
     * 是否存在服务信息
     *
     * @param serviceId 服务ID
     * @return true 存在服务， false 不存在服务
     */
    private boolean existServiceInfo(String serviceId){
        ServiceInfoCountQuery only = ServiceInfoCountQuery.only(serviceId);
        ReturnValue<Long> countReturnValue = serviceQueryService.countServiceInfo(only);
        Assert.isFalse(countReturnValue.getContent() > 1L, "数据异常服务ID{0}存在相同的多条记录",serviceId);
        return Objects.equals(countReturnValue.getContent(), 1L);
    }
}
