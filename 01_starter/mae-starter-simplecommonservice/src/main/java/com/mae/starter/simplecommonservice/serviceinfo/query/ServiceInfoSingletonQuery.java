package com.mae.starter.simplecommonservice.serviceinfo.query;

import com.mae.core.utils.Assert;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceInfoSingletonQuery {
    // 服务ID
    @NotBlank
    private String id;
    // 是否包含服务实例
    private boolean includeInstance;
    // 服务实例所属环境
    private String instanceEnv;

    /**
     * 只返回基本信息
     *
     * @param id 服务ID
     * @return
     */
    public static ServiceInfoSingletonQuery only(String id){
        Assert.hasText(id, "服务ID不能为空");
        return new ServiceInfoSingletonQuery() {{
            setId(id);
        }};
    }

    /**
     * 返回全部信息（包含实例）
     *
     * @param id 服务ID
     * @param instanceEnv 实例所属环境
     * @return
     */
    public static ServiceInfoSingletonQuery full(String id, String instanceEnv){
        Assert.hasText(id, "服务ID不能为空");
        return new ServiceInfoSingletonQuery() {{
            setId(id);
            setIncludeInstance(true);
            setInstanceEnv(instanceEnv);
        }};
    }
}
