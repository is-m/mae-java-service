package com.mae.commonservice.application.service;

import com.mae.commonservice.application.dto.ApplicationRegistrationDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/application")
public interface IApplicationService {

    @PostMapping("/reg")
    void register(ApplicationRegistrationDTO registrationDTO);

}
