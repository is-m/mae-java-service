package com.mae.commonservice.application.service.impl;

import com.mae.commonservice.application.dto.ApplicationRegistrationDTO;
import com.mae.commonservice.application.service.IApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ApplicationService implements IApplicationService {

    @Override
    public void register(ApplicationRegistrationDTO registrationDTO) {
        String name = registrationDTO.getName();
        log.info("reg application {}");
    }
}
