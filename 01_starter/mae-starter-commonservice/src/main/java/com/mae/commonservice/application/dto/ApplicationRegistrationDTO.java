package com.mae.commonservice.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationRegistrationDTO {
    /**
     * 服务名称
     */
    private String name;
}
